package net.atdhe.structs;

public interface IActivityLuncher
{
	void Launch();

	void LaunchWithKey();
}
