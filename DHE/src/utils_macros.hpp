#ifndef _UTILS_MACROS_HPP
#define _UTILS_MACROS_HPP

#ifdef _MSC_VER
extern "C"
{
#include <Windows.h>
#include <strsafe.h>
#pragma warning(disable : 4996) // hide sprintf warning
}
unsigned get_mmtime (void);
float msToSec(const unsigned time_ms);
unsigned CalcElapsetTime(const unsigned time_ms);
void PrintWinSysError(LPTSTR lpszFunction, const bool forceStdOut = false);
#else
#include <sys/time.h>
#include <time.h>
double usecToSec(const double time_usec);
double CalcElapsetTime(const timeval t1);	
const timeval StarTimeCount(void);
timeval get_mmtime (void);
#endif

#include <cstdlib>
#include <cstdarg>
#include <cstdio>
#include <cstring>

#include <string>
#include <vector>

static unsigned const GB_IN_BYTES = 1073741824;
static unsigned const MB_IN_BYTES = 1048576;
static unsigned const KB_IN_BYTES = 1024;
static int const UTIL_MACROS_MAX_LENGHT_PATH = 4096;

extern bool utils_macros_log_to_file;
extern char utils_macros_log_path[UTIL_MACROS_MAX_LENGHT_PATH];
extern unsigned utils_macros_log_max_fize;
extern FILE* utils_macros_fd_log;


std::string GetCurrentDateTime(void);
void fd_log_handle(void);
void fb_log_close(void);

#define NOT_USED(x) ((void)(x))
#define NUM_ARGS(TYPE, ...)  (sizeof((TYPE[]){__VA_ARGS__})/sizeof(TYPE))

#define PPTR_(...)\
	printf(__VA_ARGS__);

#define PTR_(...)\
	{fd_log_handle();\
	if (utils_macros_log_to_file)\
	{\
		fprintf(utils_macros_fd_log, __VA_ARGS__);\
		fflush (utils_macros_fd_log);\
	}\
	else\
		printf(__VA_ARGS__);}

#define PTR_WIN_TICKS(TYPE, ...)\
	{float ss = msToSec(CalcElapsetTime(get_mmtime() ));\
	PTR_("[%20f][%5s] %s\n", ss, TYPE, __VA_ARGS__)}

#define PTR_UNX_TICKS(TYPE, ...)\
	{float ss = usecToSec(CalcElapsetTime(get_mmtime() ));\
    PTR_("[%20f][%5s] %s\n", ss, TYPE, __VA_ARGS__)}

#define PTR_WIN_TIMEDATE(TYPE, ...)\
	{PTR_("[%20s][%5s] %s\n", GetCurrentDateTime().c_str(), TYPE, __VA_ARGS__)}

#define PTR_UNX_TIMEDATE(TYPE, ...)\
	{PTR_("[%20s][%5s] %s\n", GetCurrentDateTime().c_str(), TYPE, __VA_ARGS__)}

// Only stdout
#define PPTR_WIN_TICKS(TYPE, ...)\
	{float ss = msToSec(CalcElapsetTime(get_mmtime() ));\
	PPTR_("[%20f][%5s] %s\n", ss, TYPE, __VA_ARGS__)}

#define PPTR_UNX_TICKS(TYPE, ...)\
	{float ss = usecToSec(CalcElapsetTime(get_mmtime() ));\
    PPTR_("[%20f][%5s] %s\n", ss, TYPE, __VA_ARGS__)}

#define PPTR_WIN_TIMEDATE(TYPE, ...)\
	{PPTR_("[%20s][%5s] %s\n", GetCurrentDateTime().c_str(), TYPE, __VA_ARGS__)}

#define PPTR_UNX_TIMEDATE(TYPE, ...)\
	{PPTR_("[%20s][%5s] %s\n", GetCurrentDateTime().c_str(), TYPE, __VA_ARGS__)}



inline bool file_exist(const char* const path)
{
    FILE* f;
    if ( (f = fopen(path, "rb")) == NULL)
        return false;
    else
    {
        fclose(f);
        return true;
    }
}

/*
 * REPORTING MACROS
 */

#ifdef _MSC_VER
#define INFO_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PTR_WIN_TIMEDATE("INFO", buffer)}

#define ERR_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PTR_WIN_TIMEDATE("ERROR", buffer)}

#define INFO(...)\
	PTR_WIN_TIMEDATE("INFO", __VA_ARGS__)

#define ERR(...)\
	PTR_WIN_TIMEDATE("ERROR", __VA_ARGS__)


#define PINFO_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PPTR_WIN_TIMEDATE("INFO", buffer)}

#define PERR_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PPTR_WIN_TIMEDATE("ERROR", buffer)}

#define PINFO(...)\
	PPTR_WIN_TIMEDATE("INFO", __VA_ARGS__)

#define PERR(...)\
	PPTR_WIN_TIMEDATE("ERROR", __VA_ARGS__)


class convCharToWChar
{
private:
	wchar_t* wc_ptr;
public:
	convCharToWChar(const char* const text) : wc_ptr(NULL)
	{
		size_t newsize = strlen(text) + 20;
		wc_ptr = new wchar_t[newsize];
		if (NULL == wc_ptr) throw std::runtime_error("Out of memory");
		size_t convertedChars = 0;
		mbstowcs_s(&convertedChars, wc_ptr, newsize, text, _TRUNCATE);
		if ( convertedChars == 0 ) throw std::runtime_error("Conversion error");
	}

	~convCharToWChar(void)
	{
		if (NULL != wc_ptr)
			delete[] wc_ptr;
	}

	const wchar_t* const ValueWCHAR(void)
	{
		return wc_ptr;
	}

	LPCTSTR ValueLPCTSTR(void)
	{
		return wc_ptr;
	}

};

class convWCharToChar
{
private:
	char* c_ptr;
public:
	convWCharToChar(const wchar_t* const text) : c_ptr(NULL)		
	{
		size_t newsize = wcslen(text) + 20;
		newsize *= 2;
		c_ptr = new char[newsize];
		if (NULL == c_ptr) throw std::runtime_error("Out of memory");
		size_t convertedChars = 0;
		wcstombs_s( &convertedChars, c_ptr, newsize, text, _TRUNCATE);
		if ( convertedChars == 0 ) throw std::runtime_error("Conversion error");
	}

	const char* const value(void)
	{
		return c_ptr;
	}
};

#else
#define INFO_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PTR_UNX_TIMEDATE("INFO", buffer)}

#define ERR_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PTR_UNX_TIMEDATE("ERROR", buffer)}

#define INFO(...)\
	PTR_UNX_TIMEDATE("INFO", __VA_ARGS__)

#define ERR(...)\
	PTR_UNX_TIMEDATE("ERROR", __VA_ARGS__)


#define PINFO_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PPTR_UNX_TIMEDATE("INFO", buffer)}

#define PERR_T(TEMPLATE, ...)\
	{char buffer[4096];\
	memset(buffer, 0, 4096);\
	sprintf(buffer, TEMPLATE, __VA_ARGS__);\
	PPTR_UNX_TIMEDATE("ERROR", buffer)}

#define PINFO(...)\
	PPTR_UNX_TIMEDATE("INFO", __VA_ARGS__)

#define PERR(...)\
	PPTR_UNX_TIMEDATE("ERROR", __VA_ARGS__)

#endif

/*
 * STL LOADER MACROS
 */
template <typename T>
inline void lcv_(std::vector<T>& v, size_t numOfArgs, ...)
{
    va_list args;
    va_start(args, numOfArgs);
    char* s = NULL;
    for(size_t i = 0; i < numOfArgs; ++i)
    {
        s = va_arg(args, char*);
        v.push_back(T(s));
    }
    va_end(args);
}

template <typename T>
void liv_(std::vector<T>& v, size_t numOfArgs, ...)
{
    va_list args;
    va_start(args, numOfArgs);
    T num;
    for(size_t i = 0; i < numOfArgs; ++i)
    {
        num = va_arg(args, T);
        v.push_back(num);
    }
    va_end(args);
}

/*
 * Load std::vector<[char*|std::string|LIKE CHAR]>
 */
#define lcv(TYPE, VECTOR, ...) lcv_<TYPE>(VECTOR, NUM_ARGS(TYPE, __VA_ARGS__), __VA_ARGS__)

/*
 * Load std::vector<[int|unsigned|float|double]>
 */
#define liv(TYPE, VECTOR, ...) liv_<TYPE>(VECTOR, NUM_ARGS(TYPE, __VA_ARGS__), __VA_ARGS__)

#endif // _UTILS_MACROS_HPP



