package ddbb;

public class ArgResponse
{
	public enum ArgType {STRING, FLOAT, INTEGER, BOOL}
	private ArgType arg_type;
	public String key;

	
	public ArgResponse(String key, ArgType argtype)
	{
		this.key = key;
		this.arg_type = argtype;
	}
	
	public ArgType arg_type()
	{
		return this.arg_type;
	}
}
