package net.whatswith.structs;

public class Return
{
	public enum types {HOST_DOWN, INTERNET_DOWN, TERMINATE_THREAD, BANNED_IP};
	public boolean status = false;
	public String title = new String();
	public String message = new String();	
	public String arg1 = new String();
	public types type;
	
	
	public Return(){}
	
	public Return(boolean Status_, String Message_, String Title_)
	{
		status = Status_;
		title = Title_;
		message = Message_;
	}
	
	public Return(boolean Status_, String Message_, String Title_, types type_)
	{
		status = Status_;
		title = Title_;
		message = Message_;
		type = type_;
	}
	
	public Return(boolean Status_, String Message_, String Title_, String Arg1_)
	{
		status = Status_;
		title = Title_;
		message = Message_;
		arg1 = Arg1_;
	}	
	
	public Return(boolean Status_)
	{
		status = Status_;
	}
	
	public void Set(boolean Status_, String Message_, String Title_)
	{
		status = Status_;
		title = Title_;
		message = Message_;
	}
	
	public void Set(boolean Status_, String Message_, String Title_, String Arg1_)
	{
		status = Status_;
		title = Title_;
		message = Message_;
		arg1 = Arg1_;
	}	
	
	

}
