package ddbb;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 *  Creamos todos los comandos con los que nos podemos comunicar con la bbdd
 *  FORMATO: Query -- REQUEST
 *  $1, $2...$n Indica los parametros de cada comando contra la base de datos el orden es 
 *  muy importante ya que se usa como correpondencia contra el comando
 *  Para un parametro simple usamos(entre comilla simple): '$1'
 *  Para un array usamos(sin comilla simple): ARRAY[$1] 
 *  FORMATO: Query -- RESPONSE
 *  Estructura
 *  {
 *   "RESULT":(TRUE|FALSE)|(CAMPO1%d, CAMPO2%s ... CAMPOn%s)|ARRAY[CAMPO1%s, CAMPO2%s ... CAMPOn%s]
 *   "ERROR":"Discripcion del error"
 *  }
 *  Usamos una lista ordenada con los nombre de los parametros a retornar, el orden es muy
 *  imoportante ya que existe correspondecia de izq a der con la tabla retornada por la base de datos
 *  Para un parametro simple usamos: RESULT:CAMPO1%s, CAMPO2%b ... CAMPOn%s
 *  Para un array usamos: RESULT:ARRAY[CAMPO1%s, CAMPO2%s ... CAMPOn%s]
 *  Para un resultado simple booleano: RESULT:%b
 *  Para indicar el tipo usamos la notacion printf sindo valores permitidos:
 *  %s : string, %d: integer, %f: float, %b: boolean
 *  
 */
public class Query
{
	private String cmd;
	private String acction;
	private String query;
	private ArrayList<ArgRequest> RequestArgs;
	private ArrayList<ArgResponse> ResponseArgs;
	private JsonNode jsonRequest;
	final static String pattern_check_list = "((\\w+%[d|s|f|b],)*\\w+%[d|s|f|b])|(%[d|s|f|b])";
	final static String pattern_check_init = "(^(RESULT:ARRAY|RESULT))";

	private enum ResponseType
	{
		ONLY_VALUE, SINGLE_LINE_STRUCT, ARRAY_LINE_STRUCT
	};

	private ResponseType response_type;
	private String sql;
	private String errorDescription;

	public Query(String query, ArrayList<ArgRequest> RequestArgs)
	{
		this.query = query;
		this.RequestArgs.addAll(RequestArgs);
	}

	public Query(String Request, String Response, String ErrorDescription) throws Exception
	{
		this.errorDescription = ErrorDescription;
		
		this.RequestArgs = procRequest(Request);
		this.ResponseArgs = procResponse(Response);
	}
	
	public void SetJsonResquest(JsonNode jsonRequest) throws Exception
	{
		if (jsonRequest == null)
			throw new Exception("Json request can't be null");
		if (jsonRequest.size() == 0)
			throw new Exception("Json request can't be empty");
		this.jsonRequest = jsonRequest;
	}
	
	public String GetRequestArgValue(String key)
	{
		 return jsonRequest.path(key).asText();
	}

	private void procResponseValidation(String Response) throws Exception
	{
		Matcher matcher_init = Pattern.compile(pattern_check_init).matcher(Response);
		if (!matcher_init.find())
			throw new Exception("Todas las cadenas deben empezar con RESULT:ARRAY|RESULT, esta es: " + Response);
				
		Matcher matcher_list = Pattern.compile(pattern_check_list).matcher(Response);
		if (matcher_list.groupCount() == 0)
			throw new Exception("La sintaxis es: NOMBRE_CAMPO[%d|%s|%f|%b]" + Response);		
	}
	
	private ArrayList<ArgResponse> procResponse(String Response) throws Exception
	{
		// RESULT:%b
		// RESULT:ARRAY[CAMPO1%s, CAMPO2%b, CAMPO3%f]
		// RESULT:CAMPO1%s, CAMPO2%b, CAMPO3%f
		procResponseValidation(Response);
		ArrayList<ArgResponse> l_ResponseArgs = new ArrayList<ArgResponse>();
		if (Response.indexOf("RESULT:%b") != -1)
		{
			l_ResponseArgs.add(new ArgResponse("RESULT", ArgResponse.ArgType.BOOL));
			this.response_type = ResponseType.ONLY_VALUE;
		} else if (Response.indexOf("RESULT:ARRAY") != -1)
		{
			String strip = Response.substring(Response.indexOf("RESULT:ARRAY[") + 1, Response.length() - 1);
			this.response_type = ResponseType.ARRAY_LINE_STRUCT;
			l_ResponseArgs = procStrips(strip.split(","));
		} else
		{
			String strip = Response.substring(Response.indexOf("RESULT:") + 1, Response.length());
			this.response_type = ResponseType.SINGLE_LINE_STRUCT;
			l_ResponseArgs = procStrips(strip.split(","));
		}
		return l_ResponseArgs;
	}

	private ArrayList<ArgResponse> procStrips(String strips[])
	{
		ArrayList<ArgResponse> l_ResponseArgs = new ArrayList<ArgResponse>();
		for (String arg : strips)
		{
			String type = arg.substring(arg.lastIndexOf("%"), arg.length());
			String key = arg.substring(0, arg.indexOf("%"));
			if (type.compareTo("%d") == 0)
			{
				l_ResponseArgs.add(new ArgResponse(key, ArgResponse.ArgType.INTEGER));
			} else if (type.compareTo("%f") == 0)
			{
				l_ResponseArgs.add(new ArgResponse(key, ArgResponse.ArgType.FLOAT));
			} else if (type.compareTo("%b") == 0)
			{
				l_ResponseArgs.add(new ArgResponse(key, ArgResponse.ArgType.BOOL));
			} else if (type.compareTo("%s") == 0)
			{
				l_ResponseArgs.add(new ArgResponse(key, ArgResponse.ArgType.STRING));
			}
		}
		return l_ResponseArgs;
	}

	private ArrayList<ArgRequest> procRequest(String Request)
	{
		// "SELECT usersquestion_add('$1', ARRAY[$2])"
		ArrayList<ArgRequest> args = new ArrayList<ArgRequest>();
		String strip = Request.substring(Request.indexOf("(") + 1);
		strip = strip.substring(0, strip.length() - 1);
		String strips[] = strip.split(",");
		for (String arg : strips)
		{
			String key = null;
			ArgRequest.ArgType argtype = null;
			if (arg.substring(0).compareTo("'") == 0)
			{
				key = arg.replace("'", "");
				argtype = ArgRequest.ArgType.SIMPLE;
			} else if (arg.contains("ARRAY"))
			{
				key = arg.substring(arg.indexOf("ARRAY[") + 1, arg.length() - 1);
				argtype = ArgRequest.ArgType.MULTI;
			}
			args.add(new ArgRequest(key, argtype));
		}
		return args;
	}

	public String action()
	{
		return this.acction;
	}

	public String cmd()
	{
		return this.cmd();
	}

	public void SetCmdAction(String cmd, String action)
	{
		this.cmd = cmd;
		this.acction = action;
	}

	public String GenerateResponse(ResultSet queryResult) throws SQLException, JsonProcessingException
	{
		Map<String, Object> root = new HashMap<String, Object>();
		if (queryResult == null)
		{
			root.put("RESULT", false);
			root.put("ERROR", this.errorDescription);
		} else
		{
			ArrayList<Map<String, Object>> array_line_struct;
			Map<String, Object> single_line_struct;
			Object only_value;
			if (this.response_type == ResponseType.ARRAY_LINE_STRUCT)
			{
				array_line_struct = new ArrayList<Map<String, Object>>();
				while (queryResult.next())
				{
					Map<String, Object> value = new HashMap<String, Object>();
					int index = 0;
					for (ArgResponse argument : this.ResponseArgs)
					{
						if (argument.arg_type() == ArgResponse.ArgType.STRING)
							value.put(argument.key, queryResult.getString(index));
						else if (argument.arg_type() == ArgResponse.ArgType.BOOL)
							value.put(argument.key, queryResult.getBoolean(index));
						else if (argument.arg_type() == ArgResponse.ArgType.FLOAT)
							value.put(argument.key, queryResult.getFloat(index));
						else if (argument.arg_type() == ArgResponse.ArgType.INTEGER)
							value.put(argument.key, queryResult.getInt(index));
						index++;
					}
					array_line_struct.add(value);
				}
				root.put("RESULT", array_line_struct);
			} else if (this.response_type == ResponseType.SINGLE_LINE_STRUCT)
			{
				queryResult.next();
				single_line_struct = new HashMap<String, Object>();
				int index = 0;
				for (ArgResponse argument : this.ResponseArgs)
				{
					if (argument.arg_type() == ArgResponse.ArgType.STRING)
						single_line_struct.put(argument.key, queryResult.getString(index));
					else if (argument.arg_type() == ArgResponse.ArgType.BOOL)
						single_line_struct.put(argument.key, queryResult.getBoolean(index));
					else if (argument.arg_type() == ArgResponse.ArgType.FLOAT)
						single_line_struct.put(argument.key, queryResult.getFloat(index));
					else if (argument.arg_type() == ArgResponse.ArgType.INTEGER)
						single_line_struct.put(argument.key, queryResult.getInt(index));
					index++;
				}
				root.put("RESULT", single_line_struct);
			} else if (this.response_type == ResponseType.ONLY_VALUE)
			{
				queryResult.next();
				only_value = new Object();
				ArgResponse argument = this.ResponseArgs.get(0);
				if (argument.arg_type() == ArgResponse.ArgType.STRING)
					only_value = queryResult.getString(0);
				else if (argument.arg_type() == ArgResponse.ArgType.BOOL)
					only_value = queryResult.getBoolean(0);
				else if (argument.arg_type() == ArgResponse.ArgType.FLOAT)
					only_value =  queryResult.getFloat(0);
				else if (argument.arg_type() == ArgResponse.ArgType.INTEGER)
					only_value =  queryResult.getInt(0);
				root.put("RESULT", only_value);
			}

		}

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(root);
	}

	public String GenerateRequest() throws Exception
	{
		if (jsonRequest == null)
			throw new Exception("jsonRequest is NULL, Have you called the method 'SetJsonResquest()'");
		sql = this.query;
		int index = 0;
		for (ArgRequest argument : this.RequestArgs)
		{
			if (argument.arg_type() == ArgRequest.ArgType.SIMPLE)
			{
				sql.replace("$" + String.valueOf(index), jsonRequest.path(argument.key()).asText());
			} else if (argument.arg_type() == ArgRequest.ArgType.MULTI)
			{
				StringBuilder line = new StringBuilder();
				JsonNode jsonArray = jsonRequest.path(argument.key());
				for (int i = 0; i < jsonArray.size(); i++)
					line.append("'" + jsonArray.get(i).asText() + "',");

				sql.replace("$" + String.valueOf(index), line.toString().substring(0, line.toString().lastIndexOf(",")));
			}
			index++;
		}
		return sql;
	}
}
