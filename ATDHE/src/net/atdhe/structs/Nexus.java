package net.atdhe.structs;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.ArrayUtils;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.widget.TextView;
import net.atdhe.R;
import net.atdhe.json.RequestBuilder;
import net.atdhe.operations.Commands;
import net.atdhe.screens.menus.Debug_Settings;
import net.atdhe.tools.Serial;
import net.atdhe.structs.Contacts_db;
import net.atdhe.tools.Phone;

public class Nexus
{
	public Container Storage;
	public boolean Close = false;
	public net.atdhe.tools.Serial<Container> Serial = null;
	private final String key = "atdhe";
	public Activity MainActivity;
	public TelephonyManager tm;
	public String SimCountryIso;
	private long TimeOutInSec = 10;

	// ------------------------------------------------------------
	public int NumMaxOfContacts = 30;
	public int NumMaxOfProposals = 5;
	public int NumMaxOfQuestionByContact = 5;
	final long DELAY_IN_MILLIS = 5;
	// ------------------------------------------------------------

	public final int CRAFTING = 0, MAKING_DECISION = 1, RESULT = 2;
	public final int CRAFTING_DB = 1, MAKING_DECISION_DB = 2, RESULT_DB = 3;
	private Timer TIMER_COMPLETE_CRAFTING, TIMER_MAKING_DECISION, TIMER_CHECK_PHASE_FINISH, TIMER_CRAFTING;
	private TextView zLog;
	private Handler mHandler = new Handler();
	public ArrayList<Contacts_db> Contacts;
	private SimpleDateFormat MTimeFormat = new SimpleDateFormat("hh:mm:ss.SSS", Locale.getDefault());

	public Nexus(Activity act)
	{
		MainActivity = act;
		this.tm = (TelephonyManager) act.getSystemService(Context.TELEPHONY_SERVICE);
		SimCountryIso = tm.getSimCountryIso().toUpperCase(Locale.ENGLISH);
		Serial = new Serial<Container>(act, key);
		if (null == (Storage = Serial.Load()))
			Storage = new Container();
		zLog = (TextView) MainActivity.findViewById(R.id.layo_mainact_tv_log);
		Contacts = new ArrayList<Contacts_db>();
	}

	public void InitApp()
	{
		CheckConnections(Storage.host);
	}

	private void CheckConnections(final String host)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			private Return ret;

			@Override
			protected void onPreExecute()
			{
				zLog.setText("Comprobando las conexiones..\n");
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = new Return(true, "OK", "Checking net");
				if (Storage.desactv_conn == false)
				{
					if (CheckInternetUP() == false)
						ret = new Return(false, "Internet is Down", "Check Connections");
					if (CheckHostIsUP(Storage.host, TimeUnit.SECONDS.toMillis(TimeOutInSec)) == false)
						ret = new Return(false, "The Server is Down, please try later", "Check Host is Up");
				}

				if (Storage.desactv_conn == true)
					LogApp("Comprobacion de conexion desactivada por usuario");

				if (ret.status == false)
				{
					LogApp("Error : " + ret.message);
				} else
				{
					LogApp("Creando " + NumMaxOfContacts + " usuarios en el telefono");
					InitTasks();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
			}
		};
		task.execute((Void[]) null);
	}

	private void InitTasks()
	{
		LogApp("Borrando libreta de contactos...");
		DeleteAllContact();
		LogApp("Creando contactos nuevos");
		CreateNContacts(NumMaxOfContacts, Contacts);
		// UNITARY_TEST(Contacts);
		CREATE_USERS(Contacts);
	}

	private Timer TASK_CHECK_PHASE_FINISH(int delayInSeconds, int periodInSeconds)
	{
		int delayInMill = (int) TimeUnit.SECONDS.toMillis(delayInSeconds);
		int periodInMill = (int) TimeUnit.SECONDS.toMillis(periodInSeconds);

		Timer mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new TimerTask() {
			public void run()
			{
				CheckPhaseFinishAndFinishIt(CRAFTING);
			}
		}, delayInMill, periodInMill);
		return mTimer;
	}

	private Timer TASK_CRAFTING(int delayInSeconds, int periodInSeconds)
	{
		int delayInMill = (int) TimeUnit.SECONDS.toMillis(delayInSeconds);
		int periodInMill = (int) TimeUnit.SECONDS.toMillis(periodInSeconds);

		Timer mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new TimerTask() {
			public void run()
			{
				CRAFTING(Contacts);
			}
		}, delayInMill, periodInMill);
		return mTimer;
	}

	private Timer TASK_MAKING_DECISION(int delayInSeconds, int periodInSeconds)
	{
		int delayInMill = (int) TimeUnit.SECONDS.toMillis(delayInSeconds);
		int periodInMill = (int) TimeUnit.SECONDS.toMillis(periodInSeconds);

		Timer mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new TimerTask() {
			public void run()
			{
				MAKING_DECISION(Contacts);
			}
		}, delayInMill, periodInMill);
		return mTimer;
	}

	private Timer TASK_COMPLETING_CRAFTING(int delayInSeconds, int periodInSeconds)
	{
		int delayInMill = (int) TimeUnit.SECONDS.toMillis(delayInSeconds);
		int periodInMill = (int) TimeUnit.SECONDS.toMillis(periodInSeconds);

		Timer mTimer = new Timer();
		mTimer.scheduleAtFixedRate(new TimerTask() {
			public void run()
			{
				COMPLETING_CRAFTING(Contacts);
			}
		}, delayInMill, periodInMill);
		return mTimer;
	}

	private void LogApp(final String text)
	{
		zLog.post(new Runnable() {
			public void run()
			{
				zLog.append(text + "\n");
			}
		});

	}

	private void LogAppT(final String text)
	{
		Log.i("WHATSWITH-TESTER", text);
	}

	private void UNITARY_TEST(final ArrayList<Contacts_db> Contacts)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute()
			{
				LogApp("TEST UNITARIO");
				Contacts_db cc = Contacts.get(0);
				SparseArray<Question_db> Questions = Commands.QuestionsSync_AndClose(Storage.login(cc));
				String tt = "k�lk�lkl�";
				tt = tt.toLowerCase();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{

				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{

			}
		};
		task.execute((Void[]) null);
	}

	private void CREATE_USERS(final ArrayList<Contacts_db> Contacts)
	{

		Return ret = new Return();

		LogApp("Creado usuarios en la base de datos");

		LogApp("Se van a crear : " + Contacts.size() + " usuarios.");
		for (Contacts_db cc : Contacts)
		{
			ret = Commands.FirstSession_AndClose(Storage.login(cc));
			if (ret.status == false)
				break;
			else
				LogAppT("CREATE_USERS: Creado usuario : " + cc.name + " con telefono n� : " + cc.phone);
		}

		if (ret.status == false)
		{
			LogAppT("CREATE_USERS ERROR: " + ret.message);
		} else
		{
			LogApp("Se lanza el proceso de CRAFING..");			
			CRAFTING(Contacts);
			
			/*
			LogApp("Se lanza el proceso de COMPLETE_CRAFTING..");
			while (CheckPhaseFinishAndFinishIt(CRAFTING_DB) == false)
			{
				try
				{
					Thread.sleep(50);
					COMPLETING_CRAFTING(Contacts);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
			LogApp("Se lanza el proceso de MAKING_DECISION..");			
			while (CheckPhaseFinishAndFinishIt(MAKING_DECISION_DB) == false)
			{
				try
				{
					Thread.sleep(50);
					MAKING_DECISION(Contacts);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}*/

			// TIMER_CRAFTING = TASK_CRAFTING(2, 4);
			// TIMER_COMPLETE_CRAFTING = TASK_COMPLETING_CRAFTING(2, 4);
			// LogApp("Se lanza el proceso de CHECK_PHASE_FINISH..");
			// TIMER_CHECK_PHASE_FINISH = TASK_CHECK_PHASE_FINISH(2, 4);
			// TIMER_MAKING_DECISION = TASK_MAKING_DECISION(1, 2);
		}
	}

	private boolean CheckPhaseFinishAndFinishIt(int phase)
	{
		Return ret = new Return();
		if (Commands.debug_are_there_questions_in_phase(Storage.login(Contacts.get(0)), phase, ret) == false)
		{
			if (phase == CRAFTING_DB)
			{
				LogApp("COMPLETING_CRAFTING: TERMINADO");
			}
			if (phase == MAKING_DECISION_DB)
			{
				LogApp("COMPLETING_CRAFTING: TERMINADO");
				LogApp("RESULT: TERMINADO");
			}
			
			return true;
		} else
		{
			return false;
		}
	}
	
	private void CRAFTING(final ArrayList<Contacts_db> Contacts)
	{

		String Question = "Question-";
		int NumOfQuestions = 0;

		for (Contacts_db cc : Contacts)
		{
			try
			{
				Thread.sleep(DELAY_IN_MILLIS);
			} catch (Exception e)
			{
			}
			NumOfQuestions = Commands.GenerateRandInt(1, NumMaxOfQuestionByContact);
			LogAppT("CRAFTING : Se van a crear : " + NumOfQuestions + " questiones para el usuario : " + cc.name
					+ " con tel. n� : " + cc.phone);
			for (int i = 0; i < NumOfQuestions; i++)
				BuildQuestion(Question + String.valueOf(i), cc);

		}

	}

	private void XX_MAKING_DECISION(final ArrayList<Contacts_db> Contacts)
	{
		ArrayList<Questions_by_Phase> questions_by_phase;
		ArrayList<Questions_by_UserPhase> questions_by_Userphase;
		SparseArray<Phases_db> Phases;
		SparseArray<Question_db> Questions;
		int complete = 0;

		for (Contacts_db cc : Contacts)
		{
			try
			{
				Thread.sleep(DELAY_IN_MILLIS);
			} catch (Exception e)
			{
			}
			Phases = Commands.PhasesUpdate_AndClose(Storage.login(cc));
			Questions = Commands.QuestionsSync_AndClose(Storage.login(cc));
			if (null != Phases && null != Questions)
			{
				questions_by_phase = Commands.GetQuestionsByPhaseForBoxes(Questions, Phases);
				if (questions_by_phase.get(MAKING_DECISION).new_ones > 0)
				{
					Questions = Commands.QuestionsSyncByPhase_AndClose(Storage.login(cc), questions_by_phase.get(CRAFTING).phase_id);
					questions_by_Userphase = Commands.GetQuestionsByPhaseForList(questions_by_phase.get(CRAFTING).phase_id, Questions);
					for (Questions_by_UserPhase qp : questions_by_Userphase)
					{
						SetProposalsOrder(cc, qp.question_id);
					}
				} else if (questions_by_phase.get(CRAFTING).new_ones == 0)
					complete++;
			}
		}
	}
	
	private void MAKING_DECISION(final ArrayList<Contacts_db> Contacts)
	{

		ArrayList<Questions_by_Phase> questions_by_phase;
		ArrayList<Questions_by_UserPhase> questions_by_Userphase;
		SparseArray<Phases_db> Phases;
		SparseArray<Question_db> Questions;

		for (Contacts_db cc : Contacts)
		{
			try
			{
				Thread.sleep(DELAY_IN_MILLIS);
			} catch (Exception e)
			{
			}
			Phases = Commands.PhasesUpdate_AndClose(Storage.login(cc));
			Questions = Commands.QuestionsSync_AndClose(Storage.login(cc));
			if (null != Phases && null != Questions)
			{
				if (Questions.size() > 0 && Phases.size() > 0)
				{
					questions_by_phase = Commands.GetQuestionsByPhaseForBoxes(Questions, Phases);
					if (questions_by_phase.get(MAKING_DECISION).new_ones > 0)
					{
						Questions = Commands.QuestionsSyncByPhase_AndClose(Storage.login(cc), questions_by_phase.get(MAKING_DECISION).phase_id);
						if (null == Questions)
							continue;
						questions_by_Userphase = Commands.GetQuestionsByPhaseForList(questions_by_phase.get(MAKING_DECISION).phase_id, Questions);
						if (null == questions_by_Userphase)
							continue;

						for (Questions_by_UserPhase qp : questions_by_Userphase)
						{
							if (qp.is_phase_accepted == false)
								SetProposalsOrder(cc, qp.question_id);								
							else
								LogAppT("MAKING_DECISION: Quesion : " + qp.question_id + " ya aceptada por el usuario : " + cc.phone);
						}
					}
				}
			} else
				LogAppT("MAKING_DECISION: No hay ninguna question en espera para el usuario : " + cc.phone);
		}

	}


	private void COMPLETING_CRAFTING(final ArrayList<Contacts_db> Contacts)
	{

		ArrayList<Questions_by_Phase> questions_by_phase;
		ArrayList<Questions_by_UserPhase> questions_by_Userphase;
		SparseArray<Phases_db> Phases;
		SparseArray<Question_db> Questions;

		for (Contacts_db cc : Contacts)
		{
			try
			{
				Thread.sleep(DELAY_IN_MILLIS);
			} catch (Exception e)
			{
			}
			Phases = Commands.PhasesUpdate_AndClose(Storage.login(cc));
			Questions = Commands.QuestionsSync_AndClose(Storage.login(cc));
			if (null != Phases && null != Questions)
			{
				if (Questions.size() > 0 && Phases.size() > 0)
				{
					questions_by_phase = Commands.GetQuestionsByPhaseForBoxes(Questions, Phases);
					if (questions_by_phase.get(CRAFTING).new_ones > 0)
					{
						Questions = Commands.QuestionsSyncByPhase_AndClose(Storage.login(cc), questions_by_phase.get(CRAFTING).phase_id);
						if (null == Questions)
							continue;
						questions_by_Userphase = Commands.GetQuestionsByPhaseForList(questions_by_phase.get(CRAFTING).phase_id, Questions);
						if (null == questions_by_Userphase)
							continue;

						for (Questions_by_UserPhase qp : questions_by_Userphase)
						{
							if (qp.is_phase_accepted == false)
								AddProposals(cc, qp.question_id);
							else
								LogAppT("COMPLETING_CRAFTING: Quesion : " + qp.question_id + " ya aceptada por el usuario : " + cc.phone);
						}
					}
				}
			} else
				LogAppT("COMPLETING_CRAFTING: No hay ninguna question en espera para el usuario : " + cc.phone);
		}

	}

	private int[] ReOrderProposals(SparseArray<Proposals_db> proposals)
	{
		ArrayList<Integer> proposals_reorder = new ArrayList<Integer>();
		for (int i = 0; i < proposals.size(); i++)
			proposals_reorder.add(proposals.keyAt(i));
		Collections.shuffle(proposals_reorder);

		return ArrayUtils.toPrimitive(proposals_reorder.toArray(new Integer[proposals_reorder.size()]));
	}

	private void SetProposalsOrder(final Contacts_db cc, final int Question_id)
	{
		Return ret = new Return();
		SparseArray<Proposals_db> proposals;

		proposals = Commands.ProposalsSyncByQuestion_AndClose(Storage.login(cc), Question_id);
		if (null != proposals)
			ret = Commands.AddProposalsPreferenceOrder_AndClose(Storage.login(cc), Question_id, ReOrderProposals(proposals));

		if (ret.status)
		{
			Commands.QuestionAccept_AndClose(Storage.login(cc), Question_id);
			LogAppT("MAKING_DECISION: Question: " + Question_id + " ACEPTADA por el usuario : " + cc.phone);
		}
	}

	private void AddProposals(final Contacts_db cc, final int Question_id)
	{
		Return retA = new Return(false), retB = new Return(false);
		int NumOfProposals = 0;
		int NumOfContactsToAdd = 0;
		ArrayList<String> proposals = new ArrayList<String>();
		ArrayList<String> users_ids = new ArrayList<String>();
		ArrayList<String> Users_of_question;

		NumOfProposals = Commands.GenerateRandInt(0, NumMaxOfProposals);
		NumOfContactsToAdd = Commands.GenerateRandInt(0, 1);

		// PROPOSALS
		for (int i = 0; i < NumOfProposals; i++)
			proposals.add("Proposal-" + cc.phone + "-" + String.valueOf(i));

		// Se obtienen los usuario de la propuesta
		Return ret = new Return();
		Users_of_question = Commands.GetUsersOfQuestion(Storage.login(cc), Question_id, ret);
		if (null == Users_of_question)
		{
			LogAppT("COMPLETING_CRAFTING: No parece hbcer usuarios para la question : " + Question_id);
			return;
		}

		if (Users_of_question.size() == 0)
		{
			LogAppT("COMPLETING_CRAFTING: No parece haber usuarios para la question : " + Question_id);
			return;
		}
		// USERS TO ADD
		for (int i = 0; i < NumOfContactsToAdd; i++)
		{

			int ii = Commands.GenerateRandInt(0, Contacts.size() - 1);
			Contacts_db lcc;
			lcc = Contacts.get(ii);
			boolean dx = Users_of_question.contains(lcc.phone);
			if (lcc.phone.compareTo(cc.phone) != 0 && dx == false && users_ids.contains(lcc.phone) == false)
			{
				users_ids.add(lcc.phone);
			}
		}

		// ADD PROPOSALS
		if (proposals.size() > 0)
		{
			retA = Commands.AddProposals_AndClose(Storage.login(cc), Question_id, proposals.toArray(new String[proposals.size()]));
			if (users_ids.size() > 0)
				retB = Commands
						.Question_add_users_AndClose(Storage.login(cc), Question_id, users_ids.toArray(new String[users_ids.size()]));
			else
				retB = new Return(true, "Success", "Add new User to Question");

			if (retA.status && retB.status)
			{
				LogAppT("COMPLETING_CRAFTING: A�ADIDAS " + proposals.size() + " propuestas con : " + users_ids.size()
						+ " usuarios a la question : " + Question_id);
				Commands.QuestionAccept_AndClose(Storage.login(cc), Question_id);
				LogAppT("COMPLETING_CRAFTING: Question: " + Question_id + " ACEPTADA por el usuario : " + cc.phone);
			}
		}

	}

	private void BuildQuestion(final String Question, final Contacts_db cc)
	{

		int NumOfProposals = 0;
		int NumOfContactsToAdd = 0;
		ArrayList<String> proposals = new ArrayList<String>();
		ArrayList<String> users_ids = new ArrayList<String>();
		Return ret = new Return();
		Integer question_id = 0;
		SimpleDateFormat InputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.getDefault());

		NumOfProposals = Commands.GenerateRandInt(1, NumMaxOfProposals);
		NumOfContactsToAdd = Commands.GenerateRandInt(1, Contacts.size() - 1);

		// PROPOSALS
		for (int i = 0; i < NumOfProposals; i++)
			proposals.add("Proposal-" + String.valueOf(i));

		// USERS TO ADD
		int i = 0;
		while (i < NumOfContactsToAdd)
		{
			int ii = Commands.GenerateRandInt(0, Contacts.size() - 1);
			Contacts_db lcc = Contacts.get(ii);
			if (lcc.phone.compareTo(cc.phone) != 0 && users_ids.contains(lcc.phone) == false)
			{
				users_ids.add(lcc.phone);
				i++;
			}
		}
		users_ids.add(cc.phone);

		// CREATION
		ret = Commands.QuestionOpen_AndClose(Storage.login(cc), Question, InputFormat.format(new Date()), 3,
				proposals.toArray(new String[proposals.size()]), users_ids.toArray(new String[users_ids.size()]), question_id);
		LogAppT("CRAFTING: Creada la question : " + Question + " pasa el usuario : " + cc.phone);
		LogAppT("CRAFTING: Y a�adidas a la question : " + Question + " , " + NumOfProposals + " propouestas con " + NumOfContactsToAdd
				+ " usuarios");

		if (ret.status)
			LogAppT("CRAFTING: Question Creada : " + Question);
		else
			LogAppT("CRAFTING: Question : " + Question + " error en creacion.");

	}

	public void CreateNContacts(int numOfContacts, ArrayList<Contacts_db> contacts)
	{
		String user = "User";
		int phone = 600000001;
		String password = "clave.0001";
		for (int i = 0; i < numOfContacts; i++)
		{
			Contacts_db cc = new Contacts_db(user + String.valueOf(i), String.valueOf(phone + i), password);
			cc.phone = net.atdhe.tools.Phone.FormatNumber(cc.phone, "ES");
			Commands.CreateContact(cc.name, cc.phone, MainActivity);
			contacts.add(cc);
		}
	}

	public void DeleteAllContact()
	{
		ContentResolver cr = MainActivity.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		while (cur.moveToNext())
		{
			try
			{
				String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
				Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
				cr.delete(uri, null, null);
			} catch (Exception e)
			{
				System.out.println(e.getStackTrace());
			}
		}
	}

	public void DeleteNContacts(ArrayList<Contacts_db> Contacts)
	{
		for (Contacts_db cc : Contacts)
			Commands.DeleteContact(cc.name, cc.phone, MainActivity);
	}

	public void DownApp()
	{
		for (Contacts_db cc : Contacts)
		{
			Commands.DeleteContact(cc.name, cc.phone, MainActivity);
		}
	}

	public void DebugMenu(Activity act, MenuItem item)
	{
		switch (item.getItemId())
		{

		case R.id.action_debug_settings:
			Intent intent = new Intent(act, Debug_Settings.class);
			act.startActivity(intent);
			break;
		}
	}

	/*
	 * public Login login() { return Storage.login(); }
	 */

	public void MenuSelector(Activity act, MenuItem item)
	{
		Intent intent = null;
		switch (item.getItemId())
		{

		case R.id.action_debug_settings:
			intent = new Intent(act, Debug_Settings.class);
			act.startActivity(intent);
			break;
		}
	}

	public boolean CheckInternetUP()
	{
		try
		{
			ConnectivityManager conMgr = (ConnectivityManager) MainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (conMgr != null)
			{
				NetworkInfo i = conMgr.getActiveNetworkInfo();
				if (i != null)
				{
					if (!i.isConnected())
						return false;
					if (!i.isAvailable())
						return false;
					return true;
				} else
					return false;

			} else
				return false;
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public boolean CheckHostIsUP(final String Host, final long TimeOutInMillis)
	{

		if (Host.isEmpty())
			return false;
		InetAddress CheckIP = null;
		try
		{
			CheckIP = InetAddress.getByName(Host);
			return CheckIP.isReachable((int) TimeOutInMillis);
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}

	}
}
