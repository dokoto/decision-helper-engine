package net.atdhe.structs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import android.graphics.Bitmap;
 
public class Contacts_db
{
	public static enum FilterType
	{
		BY_NAME, BY_LOCK;
		public static FilterType at(int index)
		{
			for (FilterType row : FilterType.values())
			{
				if (index == 0)
					return row;
				else
					index--;
			}
			return null;
		}
	};

	public String name;
	public String phone;
	public String password;
	public Bitmap photo;
	public boolean has_photo;
	public Date sign_date;
	public boolean is_selected;
	public boolean is_hidden;
	public boolean is_locked;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.getDefault());

	public Contacts_db(String Name, String Phone, Bitmap Photo, boolean Has_photo)
	{
		this.name = Name;
		this.phone = Phone;
		this.photo = Photo;
		this.has_photo = Has_photo;
		this.is_hidden = true;
	}
	
	public Contacts_db(String Name, String Phone, String Password)
	{
		this.name = Name;
		this.phone = Phone;
		this.password = Password;
		this.photo = null;
		this.has_photo = false;
		this.is_hidden = false;
	}

	public Contacts_db(String Name, String Sign_date, boolean Is_hidden)
	{
		try
		{
			this.name = Name;
			this.sign_date = dateFormat.parse(Sign_date);
			this.is_hidden = Is_hidden;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Contacts_db(Contacts_db obj)
	{
		this.name = obj.name;
		this.phone = obj.phone;
		this.photo = obj.photo;
		this.has_photo = obj.has_photo;
		this.sign_date = obj.sign_date;
		this.is_selected = obj.is_selected;
		this.is_hidden = obj.is_hidden;
		this.is_locked = obj.is_locked;
	}

	public void upGradeWithDb(String Phone, String Sign_date)
	{
		try
		{
			this.phone = Phone;
			this.is_hidden = false;
			this.sign_date = dateFormat.parse(Sign_date);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void upGradeWithDb(String Phone, Date Sign_date)
	{
		this.phone = Phone;
		this.is_hidden = false;
		this.sign_date = Sign_date;
	}

	public boolean compareTo(Contacts_db obj)
	{
		if (this.name.compareTo(obj.name) != 0)
			return false;
		if (this.phone.compareTo(obj.phone) != 0)
			return false;
		if (this.has_photo == obj.has_photo)
			return false;
		if (this.sign_date.compareTo(obj.sign_date) != 0)
			return false;
		if (this.is_selected == obj.is_selected)
			return false;
		if (this.is_hidden == obj.is_hidden)
			return false;
		if (this.is_locked == obj.is_locked)
			return false;

		return true;
	}
}
