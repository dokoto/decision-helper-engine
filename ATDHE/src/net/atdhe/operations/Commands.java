package net.atdhe.operations;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import net.atdhe.Main;
import java.util.Map.Entry;
import net.atdhe.json.RequestBuilder;
import net.atdhe.json.ResponseBuilder;
import net.atdhe.ssl.SSLConnSever;
import net.atdhe.structs.Contacts_db;
import net.atdhe.structs.IActivityLuncher;
import net.atdhe.structs.Login;
import net.atdhe.structs.Phases_db;
import net.atdhe.structs.Proposals_db;
import net.atdhe.structs.Question_db;
import net.atdhe.structs.Questions_by_Phase;
import net.atdhe.structs.Questions_by_UserPhase;
import net.atdhe.structs.Questions_rev_com;
import net.atdhe.structs.Return;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.widget.Toast;

public class Commands
{
	public static void Async_Question_Accept(final Login login, final Activity act, final IActivityLuncher func,
			final boolean finish_at_end, final int question_id)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			private Return ret;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = QuestionAccept_AndClose(login, question_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
				{
					//Toast.makeText(act.getApplicationContext(), "Preference Order save successful", Toast.LENGTH_SHORT).show();
					if (finish_at_end)
						act.finish();
				}
			}
		};
		task.execute((Void[]) null);
	}

	public static void Async_Open_Question(final Login login, final Activity act, final IActivityLuncher func, final String question,
			final String exp_date, final int delay_days, final String[] proposals, final String[] users_ids, final boolean finish_at_end)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			private Return ret = new Return();
			private Integer question_id;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = QuestionOpen_AndClose(login, question, exp_date, delay_days, proposals, users_ids, question_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
				} else
				{
					//Toast.makeText(act.getApplicationContext(), "New question created success", Toast.LENGTH_SHORT).show();
					if (func != null)
						func.Launch();
				}
				if (finish_at_end)
					act.finish();
			}

		};
		task.execute((Void[]) null);
	}

	public static int GenerateRandInt(int min, int max)
	{
		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static void Async_Commit_Update_Contacts(final Login login, final Activity act, final IActivityLuncher func)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			private Return ret = new Return();

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				Main.Nexus.Storage.contacts = getContactsFromDevice(act);
				ret = ContactsUpdate_AndClose(login, ContactsToArray(Main.Nexus.Storage.contacts));

				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					//Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
				} else
				{					
					//Toast.makeText(act.getApplicationContext(), "Upgrade Contacts success", Toast.LENGTH_SHORT).show();
					Main.Nexus.Serial.Save(Main.Nexus.Storage);
					if (func != null)
						func.LaunchWithKey();
				}
			}

		};
		task.execute((Void[]) null);
	}

	public static Return UserExist_AndClose(final Login login)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			String query_user_exist = RequestBuilder.user_exist(login.user);
			StringBuilder response = new StringBuilder();
			SendServer(conn, query_user_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_exist(response.toString(), error) == false)
				return new Return(false, error.toString(), "User Exist");
			else
				return new Return(true, "User : " + login.user + " exist", "User Exist");

		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		} finally
		{
			Close(conn, login);
		}
	}

	public static Return FirstSession_AndClose(final Login login)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			String query_user_exist = RequestBuilder.user_exist(login.user);
			StringBuilder response = new StringBuilder();
			SendServer(conn, query_user_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_exist(response.toString(), error) == false)
			{
				if (CreateUser(conn, login.user, login.password).status == true)
				{
					if (AuthUser(conn, login.user, login.password).status == true)
					{
						return new Return(true, "A new user : " + login.user + " has been successful Created and Authenticated",
								"Add and Auth new User", login.password);
					}
				}
				return new Return(false);
			} else
				return AuthUser(conn, login.user, login.password);

		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		} finally
		{
			Close(conn, login);
		}
	}

	public static void Close(SSLConnSever conn, Login login)
	{
		UnPlug_(conn, login);
		CloseSeverConn(conn);
	}

	/*
	 * Metodo que conecta y autoriza contra el servidor si es necesario)
	 */
	private static Return ConnectAndAuth(SSLConnSever conn, final Login login)
	{
		return ConnectAndAuth(conn, login.host, login.port, login.user, login.password);
	}

	public static Return PasswordChange_AndClose(final Login login, final String password_new)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return PasswordChange_(conn, login.user, login.password, password_new);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}

	}

	public static Return QuestionOpen_AndClose(final Login login, final String question, final String exp_date, final int delay_days,
			final String[] proposals, final String[] users_ids, Integer question_id)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{

			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return QuestionOpen_(conn, login.user, question, exp_date, delay_days, proposals, users_ids, question_id);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}

	public static SparseArray<Phases_db> PhasesUpdate_AndClose(final Login login)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return PhasesUpdate_(conn);
			else
				return null;
		} finally
		{
			Close(conn, login);
		}
	}

	public static Return ContactsUpdate_AndClose(final Login login, String[] users_ids)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return ContactsUpdate_(conn, users_ids);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}

	public static Return AddProposals_AndClose(final Login login, final int question_id, final String[] proposals)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return AddProposals_(conn, login.user, question_id, proposals);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}
	
	
	public static ArrayList<String> GetUsersOfQuestion(final Login login, final int question_id, Return ret)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return GetUsersOfQuestion_(conn, question_id, ret);
			else
			{
				ret.Set(false);
				return null;
			}
		} finally
		{
			Close(conn, login);
		}
	}	

	private static Return AddProposals_(SSLConnSever conn, final String user_id, final int question_id, String[] proposals)
	{
		try
		{
			String proposals_add = RequestBuilder.proposals_add(user_id, question_id, proposals);
			StringBuilder response = new StringBuilder();
			SendServer(conn, proposals_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.proposals_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Add Proposals");
			else
				return new Return(true, "Success", "Add Proposals");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(),
					"Update All Contacts");
		}
	}

	public static Return AddProposalsPreferenceOrder_AndClose(final Login login, final int question_id, final int[] proposals_ids)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{

			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return AddProposalsPreferenceOrder_(conn, login.user, question_id, proposals_ids);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}

	}

	public static Return AddProposalsPreferenceOrder_(SSLConnSever conn, final String user_id, final int question_id,
			final int[] proposals_ids)
	{
		try
		{
			String userproposals_add = RequestBuilder.userproposals_add(user_id, question_id, proposals_ids);
			StringBuilder response = new StringBuilder();
			SendServer(conn, userproposals_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.userproposals_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Save Preference Proposals Order");
			else
				return new Return(true, "Success", "Save Preference Proposals Ordern");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(),
					"Save Preference Proposals Order");
		}

	}

	public static Return Question_add_users_AndClose(final Login login, final int question_id, final String[] users_ids)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return Question_add_users_(conn, question_id, users_ids);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}

	private static Return Question_add_users_(SSLConnSever conn, final int question_id, String[] users_ids)
	{
		try
		{
			String question_add_users = RequestBuilder.question_add_users(question_id, users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(conn, question_add_users, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_add_users(response.toString(), error) == false)
				return new Return(false, error.toString(), "Add new User to Question");
			else
				return new Return(true, "Success", "Add new User to Question");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(),
					"Add new User to Question");
		}
	}

	public static Return QuestionAccept_AndClose(final Login login, final int question_id)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return QuestionAccept_(conn, login.user, question_id);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}
	
	// MEJORAS-CORRECCIONES 
	public static boolean debug_are_there_questions_in_phase(final Login login, final int sort_index, Return ret)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return debug_are_there_questions_in_phase_(conn, sort_index, ret);
			else
			{
				ret.Set(false);
				return false;
			}
		} finally
		{
			Close(conn, login);
		}
	}	
	
	private static ArrayList<String> GetUsersOfQuestion_(SSLConnSever conn, final int question_id, Return ret)
	{
		try
		{
			String sync_get_users_of_question = RequestBuilder.sync_get_users_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_users_of_question, response);
			StringBuilder error = new StringBuilder();
			ArrayList<String> users_ids = new ArrayList<String>();
			if (ResponseBuilder.sync_get_users_of_question(response.toString(), users_ids, error) == true)
				ret.Set(true, "Success", "Sync All Proposals");
			else
				ret.Set(false, "Error", error.toString());

			return users_ids;
		} catch (Exception e)
		{
			ret.Set(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Proposals");
			return null;
		}
	}
	
	
	private static boolean debug_are_there_questions_in_phase_(SSLConnSever conn, final int sort_index, Return ret)
	{
		try
		{
			String debug_are_there_questions_in_phase = RequestBuilder.debug_are_there_questions_in_phase(sort_index);
			StringBuilder response = new StringBuilder();
			SendServer(conn, debug_are_there_questions_in_phase, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.debug_are_there_questions_in_phase(response.toString(), error) == false)
			{
				ret.Set(false, error.toString(), "Are There question in phase");
				return false;
			}
			else
			{
				ret.Set(true, error.toString(), "Are There question in phase");
				return true;
			}
		} catch (Exception e)
		{
			ret.Set(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Accept");
			return false;
		}
	}
	
	private static Return QuestionAccept_(SSLConnSever conn, final String user_id, final int question_id)
	{
		try
		{
			String question_accept = RequestBuilder.question_accept(user_id, question_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, question_accept, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_accept(response.toString(), error) == false)
				return new Return(false, error.toString(), "Question Accept");
			else
				return new Return(true, "Success", "Question Accept");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Accept");
		}
	}

	public static ArrayList<Questions_by_UserPhase> GetQuestionsByPhaseForList(final int Phase_id, SparseArray<Question_db> questions)
	{
		ArrayList<Questions_by_UserPhase> questionsByPU = new ArrayList<Questions_by_UserPhase>();
		for (int i = 0; i < questions.size(); ++i)
		{
			int key = questions.keyAt(i);
			if (-1 != key)
			{
				Question_db q = questions.get(key);
				if (q.phase_id == Phase_id)
				{
					questionsByPU.add(new Questions_by_UserPhase(key, q));
				}
			}
		}
		return questionsByPU;
	}

	public static ArrayList<Questions_by_Phase> GetQuestionsByPhaseForBoxes(SparseArray<Question_db> questions,
			SparseArray<Phases_db> phases)
	{
		ArrayList<Questions_by_Phase> questionsByP = new ArrayList<Questions_by_Phase>();
		SparseIntArray maps = new SparseIntArray();

		for (int i = 0; i < phases.size(); ++i)
		{
			int key = phases.keyAt(i);
			questionsByP.add(new Questions_by_Phase(key, phases.get(key).phase));
			maps.put(key, i);
		}
		for (int i = 0; i < questions.size(); i++)
		{
			Question_db q = questions.valueAt(i);
			if (null == q)
				continue;
			int pos = maps.get(q.phase_id);
			if (-1 == pos)
				continue;
			questionsByP.get(pos).tot_ones++;
			if (q.is_phase_accepted == false)
				questionsByP.get(pos).new_ones++;
		}
		return questionsByP;
	}

	public static SparseArray<Question_db> QuestionsSync_AndClose(final Login login)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == false)
				return null;
			else
				return QuestionsSync_(conn, login.user);
		} finally
		{
			Close(conn, login);
		}
	}

	public static Return Get_top_proposals_of_question_AndClose(final Login login, final int question_id, int[] proposal_id)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{

			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == false)
				return Get_top_proposals_of_question_(conn, question_id, proposal_id);
			else
				return new Return(false);
		} finally
		{
			Close(conn, login);
		}
	}

	public static SparseArray<Proposals_db> ProposalsSyncByQuestion_AndClose(final Login login, final int question_id)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == false)
				return null;
			else
				return ProposalsSyncByQuestion_(conn, question_id);
		} finally
		{
			Close(conn, login);
		}
	}

	public static SparseArray<Question_db> QuestionsSyncByPhase_AndClose(final Login login, final int phase_id)
	{
		SSLConnSever conn = OpenServerConn(login.host, login.port);
		try
		{
			if (ConnectAndAuth(conn, login.host, login.port, login.user, login.password).status == true)
				return QuestionsSyncByPhase_(conn, login.user, phase_id);
			else
				return null;
		} finally
		{
			Close(conn, login);
		}
	}

	private static Return ConnectAndAuth(SSLConnSever conn, final String hostname, final int port, final String user_id,
			final String password)
	{
		try
		{
			if (isConnected(conn).status == false)
			{
				if (OpenServerConn(hostname, port) == null)
					return new Return(false);
				else
					return AuthUser(conn, user_id, password);
			} else
			{
				if (isAuth(conn) == false)
					return AuthUser(conn, user_id, password);
				else
					return new Return(true, "User Authentication success", "User Auth", password);
			}
		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		}
	}

	private static Return ContactsUpdate_(SSLConnSever conn, String[] users_ids)
	{
		try
		{
			String user_array_exist = RequestBuilder.user_array_exist(users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(conn, user_array_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_array_exist(response.toString(), Main.Nexus.Storage.contacts, error) == false)
				return new Return(false, error.toString(), "Update All Contacts");
			else
				return new Return(true, "Success", "Update All Contacts");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(),
					"Update All Contacts");
		}
	}

	private static SparseArray<Phases_db> PhasesUpdate_(SSLConnSever conn)
	{
		SparseArray<Phases_db> Phases = new SparseArray<Phases_db>();
		try
		{
			String sync_get_phase_sort = RequestBuilder.sync_get_phase_sort();
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_phase_sort, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.sync_get_phase_sort(response.toString(), Phases, error) == false)
				return null;
			else
				return Phases;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	
	public static HashMap<String, Contacts_db> getContactsFromDevice(Activity act)
	{
		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
				ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE,
				ContactsContract.CommonDataKinds.Phone.PHOTO_ID };
		Cursor people = act.getContentResolver().query(uri, projection, null, null, null);
		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
		int indexPhoto = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID);
		int indexNumberType = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);

		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();

		String phone = new String();
		try
		{
			people.moveToFirst();
			do
			{
				if (people.getInt(indexNumberType) == Phone.TYPE_MOBILE)
				{
					Bitmap BitPhoto = queryContactImage(people.getInt(indexPhoto), act);
					phone = (Main.Nexus.Storage.crazy_mode == false) ? net.atdhe.tools.Phone.FormatNumber(people.getString(indexNumber),
							Main.Nexus.SimCountryIso) : net.atdhe.tools.Phone.FormatNumber(people.getString(indexNumber), "ES");

					if (Main.Nexus.Storage.phoneNumber().compareTo(phone) != 0)
					{
						String name = people.getString(indexName);
						ContactsInfo.put(phone, new Contacts_db(name, phone, BitPhoto, (BitPhoto == null) ? false : true));
					}
				}
			} while (people.moveToNext());

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return ContactsInfo;
	}

	public static String[] ContactsToArray(HashMap<String, Contacts_db> c)
	{
		ArrayList<String> ContactsInfo = new ArrayList<String>();
		Iterator<Entry<String, Contacts_db>> it = c.entrySet().iterator();
		while (it.hasNext())
			ContactsInfo.add(it.next().getValue().phone);

		return ContactsInfo.toArray(new String[ContactsInfo.size()]);
	}

	public static String[] ContactsToArrayOnlySelected(HashMap<String, Contacts_db> c, boolean add_device_phone)
	{
		ArrayList<String> ContactsInfo = new ArrayList<String>();
		Iterator<Entry<String, Contacts_db>> it = c.entrySet().iterator();
		while (it.hasNext())
		{
			Contacts_db row = it.next().getValue();
			if (row.is_selected)
				ContactsInfo.add(row.phone);
		}
		if (add_device_phone == true)
			ContactsInfo.add(Main.Nexus.Storage.phoneNumber());

		return ContactsInfo.toArray(new String[ContactsInfo.size()]);
	}

	public static class CMP_Contact_db_Name implements Comparator<Contacts_db>
	{
		@Override
		public int compare(Contacts_db object1, Contacts_db object2)
		{
			return object1.name.compareToIgnoreCase(object2.name);
		}
	}

	public static class CMP_Contact_db_lock implements Comparator<Contacts_db>
	{
		@Override
		public int compare(Contacts_db object1, Contacts_db object2)
		{
			return (object1.is_locked && object2.is_locked) ? 0 : -1;
		}
	}

	public static boolean DeleteContact(final String UserName, final String PhoneNumber, final Activity Act)
	{
		Uri contactUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(PhoneNumber));
		Cursor cur = Act.getContentResolver().query(contactUri, null, null, null, null);
		try
		{
			if (cur.moveToFirst())
			{
				do
				{
					if (cur.getString(cur.getColumnIndex(PhoneLookup.DISPLAY_NAME)).equalsIgnoreCase(UserName))
					{
						String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
						Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
						Act.getContentResolver().delete(uri, null, null);
						return true;
					}

				} while (cur.moveToNext());
			}

		} catch (Exception e)
		{
			System.out.println(e.getStackTrace());
		}
		return false;
	}

	public static void CreateContact(final String UserName, final String PhoneNumber, final Activity Act)
	{

		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
				.build());

		// ------------------------------------------------------ Name
		if (UserName != null)
		{
			ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
					.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, UserName).build());
		}

		// ------------------------------------------------------ Mobile Number
		if (PhoneNumber != null)
		{
			ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
					.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
					.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, PhoneNumber)
					.withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
		}

		/*
		 * //------------------------------------------------------ Home Numbers
		 * if (HomeNumber != null) {
		 * ops.add(ContentProviderOperation.newInsert(ContactsContract
		 * .Data.CONTENT_URI)
		 * .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		 * .withValue(ContactsContract.Data.MIMETYPE,
		 * ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
		 * .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, HomeNumber)
		 * .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
		 * ContactsContract.CommonDataKinds.Phone.TYPE_HOME) .build()); }
		 * 
		 * //------------------------------------------------------ Work Numbers
		 * if (WorkNumber != null) {
		 * ops.add(ContentProviderOperation.newInsert(ContactsContract
		 * .Data.CONTENT_URI)
		 * .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		 * .withValue(ContactsContract.Data.MIMETYPE,
		 * ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
		 * .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, WorkNumber)
		 * .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
		 * ContactsContract.CommonDataKinds.Phone.TYPE_WORK) .build()); }
		 * 
		 * //------------------------------------------------------ Email if
		 * (emailID != null) {
		 * ops.add(ContentProviderOperation.newInsert(ContactsContract
		 * .Data.CONTENT_URI)
		 * .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		 * .withValue(ContactsContract.Data.MIMETYPE,
		 * ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
		 * .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
		 * .withValue(ContactsContract.CommonDataKinds.Email.TYPE,
		 * ContactsContract.CommonDataKinds.Email.TYPE_WORK) .build()); }
		 * 
		 * //------------------------------------------------------ Organization
		 * if (!company.equals("") && !jobTitle.equals("")) {
		 * ops.add(ContentProviderOperation
		 * .newInsert(ContactsContract.Data.CONTENT_URI)
		 * .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		 * .withValue(ContactsContract.Data.MIMETYPE,
		 * ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
		 * .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY,
		 * company)
		 * .withValue(ContactsContract.CommonDataKinds.Organization.TYPE,
		 * ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
		 * .withValue(ContactsContract.CommonDataKinds.Organization.TITLE,
		 * jobTitle)
		 * .withValue(ContactsContract.CommonDataKinds.Organization.TYPE,
		 * ContactsContract.CommonDataKinds.Organization.TYPE_WORK) .build()); }
		 */

		// Asking the Contact provider to create a new contact
		try
		{
			Act.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (Exception e)
		{
			e.printStackTrace();
			//Toast.makeText(Act, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	private static SparseArray<Question_db> QuestionsSyncByPhase_(SSLConnSever conn, final String user_id, final int phase_id)
	{
		try
		{
			String sync_get_questions_by_user_phase = RequestBuilder.sync_get_questions_by_user_phase(user_id, phase_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_questions_by_user_phase, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Questions_by_UserPhase> questionsReciv = new ArrayList<Questions_by_UserPhase>();
			SparseArray<Question_db> questions = new SparseArray<Question_db>();
			if (ResponseBuilder.sync_get_questions_by_user_phase(response.toString(), questionsReciv, error) == true)
			{				
				for (Questions_by_UserPhase row : questionsReciv)			
						questions.put(row.question_id, new Question_db(row));				
				return questions;
			} else return null;

		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Return Get_top_proposals_of_question_(SSLConnSever conn, final int question_id, int[] proposal_id)
	{
		try
		{
			String sync_get_top_proposals_of_question = RequestBuilder.sync_get_top_proposals_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_top_proposals_of_question, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.sync_get_top_proposals_of_question(response.toString(), proposal_id, error) == true)
				return new Return(true, "Success", "Get Results");
			else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Get Results");
		}
	}

	private static SparseArray<Proposals_db> ProposalsSyncByQuestion_(SSLConnSever conn, final int question_id)
	{
		SparseArray<Proposals_db> RetProposals = new SparseArray<Proposals_db>();
		try
		{
			String sync_get_sort_proposals_of_question = RequestBuilder.sync_get_sort_proposals_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_sort_proposals_of_question, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Proposals_db> proposals = new ArrayList<Proposals_db>();
			if (ResponseBuilder.sync_get_sort_proposals_of_question(response.toString(), proposals, error) == true)
			{
				for (Proposals_db row : proposals)
				{
					RetProposals.put(row.proposal_id, new Proposals_db(row));
				}
				return RetProposals;

			} else
				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static SparseArray<Question_db> QuestionsSync_(SSLConnSever conn, final String user_id)
	{
		SparseArray<Question_db> questions_db = new SparseArray<Question_db>();
		try
		{
			String sync_get_questions_revs_of_user = RequestBuilder.sync_get_questions_revs_of_user(user_id);
			StringBuilder response = new StringBuilder();
			SendServer(conn, sync_get_questions_revs_of_user, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Questions_rev_com> questions = new ArrayList<Questions_rev_com>();
			if (ResponseBuilder.sync_get_questions_revs_of_user(response.toString(), questions, error) == true)
			{
				for (Questions_rev_com row : questions)
				{
					questions_db.put(row.question_id, new Question_db(row));
				}
				return questions_db;
			} else
				return null;

		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private static Return PasswordChange_(SSLConnSever conn, final String user_id, final String password_old, final String password_new)
	{
		try
		{
			String password_change = RequestBuilder.password_change(user_id, password_old, password_new);
			StringBuilder response = new StringBuilder();
			SendServer(conn, password_change, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.password_change(response.toString(), error) == false)
				return new Return(false, error.toString(), "Password Change");
			else
				return new Return(true, user_id + " password success modified.", "New User");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Password Change");
		}
	}

	private static Return QuestionOpen_(SSLConnSever conn, final String user_id, final String question, final String exp_date,
			final int delay_days, final String[] proposals, final String[] users_ids, Integer question_id)
	{
		try
		{
			String question_open = RequestBuilder.question_open(user_id, question, exp_date, delay_days, proposals, users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(conn, question_open, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_open(response.toString(), question_id, error) == false)
				return new Return(false, error.toString(), "Quesion Open");
			else
				return new Return(true, user_id + "Question Created success.", "Question Open");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Open");
		}
	}

	private static Return CreateUser(SSLConnSever conn, final String user_id, final String password)
	{
		try
		{
			String query_user_add = RequestBuilder.user_add(user_id, password);
			StringBuilder response = new StringBuilder();
			SendServer(conn, query_user_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "New User");
			else
				return new Return(true, user_id + " successful Created.", "New User");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "New User");
		}
	}

	private static Return AuthUser(SSLConnSever Conn, final String user_id, final String password)
	{
		try
		{
			String query_user_password_exist = RequestBuilder.user_password_exist(user_id, password);
			StringBuilder response = new StringBuilder();
			SendServer(Conn, query_user_password_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Auth User");
			else
				return new Return(true, user_id + " successful Authenticated", "Auth User", password);

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "New User");
		}
	}

	private static boolean CloseSeverConn(SSLConnSever Conn)
	{
		// MEJORA A LLEVAR
		if (Conn == null) return true;			
		if (Conn.close())
		{
			return true;
		} else
			return false;
	}

	private static Return UnPlug_(SSLConnSever Conn, Login login)
	{
		try
		{
			StringBuilder response = new StringBuilder();
			Conn.CloseSession(response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.session_close(response.toString(), error) == false)
				return new Return(false, error.toString(), "Close Session");
			else
				return new Return(true, "Success", "Close Session");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Close Sessio");
		}
	}

	private static SSLConnSever OpenServerConn(final String hostname, final int port)
	{
		SSLConnSever Conn = new SSLConnSever();
		if (Conn.open(hostname, port).status == true)
			return Conn;
		else
			return null;
	}

	private static boolean isAuth(SSLConnSever Conn)
	{
		if (Conn == null)
			return false;
		return Conn.IsAuth();
	}

	private static Return isConnected(SSLConnSever Conn)
	{
		if (Conn == null)
			return new Return(false, "Sock is Null", "Is Connected");
		return Conn.IsConnected();
	}

	private static boolean SendServer(SSLConnSever Conn, String request, StringBuilder response)
	{
		if (Conn == null)
			return false;
		if (Conn.send(request, response) == true)
		{
			return true;
		} else
			return false;
	}

	private static Bitmap queryContactImage(int imageDataRow, Activity act)
	{
		Cursor c = act.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
				new String[] { ContactsContract.CommonDataKinds.Photo.PHOTO }, ContactsContract.Data._ID + "=?",
				new String[] { Integer.toString(imageDataRow) }, null);
		byte[] imageBytes = null;
		if (c != null)
		{
			if (c.moveToFirst())
			{
				imageBytes = c.getBlob(0);
			}
			c.close();
		}

		if (imageBytes != null)
		{
			return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		} else
		{
			return null;
		}
	}

}