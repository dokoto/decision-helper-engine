#ifndef _DB_CONN_HPP_
#define _DB_CONN_HPP_

#include <cstring>
#include <vector>
#include <string>
#include "libpq-fe.h"

#include "utils_proc.hpp"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif
namespace dhe
{
    bool init_db_conn(const std::string& db_host, const std::string& db_name, const std::string& db_user, const std::string& db_password);
	PGconn* openPgDB(const std::string& db_host, const std::string& db_name, const std::string& db_user, const std::string& db_password);
	void closePgDB(void);
	std::string dbDataToString(const std::string& host, const std::string& name, const std::string& user, const std::string& password);

	bool user_add(const std::string& user_id, const std::string& passwd, std::string& error);
	bool user_password_exist(const std::string& user_id, const std::string& passwd, std::string& error);
	bool user_exist(const std::string& user_id, std::string& error);
	bool user_array_exist(const std::vector<std::string>& users_ids, std::vector<user_array_exist_t>& result, std::string& error);	
    bool password_change(const std::string& user_id, const std::string& passwd_old, const std::string& passwd_new, std::string& error);
	bool user_lock(const std::string& user_id, std::string& error);
	bool user_unlock(const std::string& user_id, std::string& error);

	bool debug_are_there_questions_in_phase(const int sort_index, std::string& error);
	bool question_accept(const std::string& user_id, const unsigned question_id, std::string& error);
	bool question_deprecated(const unsigned question_id, std::string& error);
	bool question_switch_fase(const unsigned question_id, std::string& error);
	int question_get_last_rev(const unsigned question_id, std::string& error);
	bool question_is_accepted(const unsigned question_id, std::string& error);
	int question_open(const std::string& user_id, const std::string& question, const std::string& exp_date, 
		const int delay_days_between_phases, const std::vector<std::string>& proposals,
                          const std::vector<std::string>& usersquestion, std::string& error);
	bool question_add_users(const unsigned question_id,  const std::vector<std::string>& users_ids, std::string& error);

	int proposals_add(const std::string& user_id, const unsigned question_id,  const std::vector<std::string>& proposals, std::string& error);
	bool userproposals_add(const std::string& user_id, const unsigned question_id, const std::vector<unsigned> proposals_ids, std::string& error);
	
	void sync_get_questions_revs_of_user(const std::string& user_id, std::vector<sync_get_questions_revs_of_user_t>& result, std::string& error);
	void sync_get_questions_by_user_phase(const std::string& user_id, const unsigned phase_id, std::vector<sync_get_questions_by_user_phase_t>& result, std::string& error);
	void sync_get_sort_proposals_of_question(const unsigned question_id, std::vector<sync_get_sort_proposals_of_question_t>& result, std::string& error);
	void sync_get_top_proposals_of_question(const unsigned question_id,  std::vector<calc_better_proposal_t>& result, std::string& error);
	void sync_get_questions_sort(const std::vector<unsigned> questions_ids, 
		std::vector<sync_get_questions_sort_t>& result, std::string& error);
	void sync_get_phase_sort(std::vector<sync_get_phase_sort_t>& result, std::string& error);
	void sync_get_users_of_question(const int question_id, std::vector<std::string>& result, std::string& error);



	bool delete_all_tables(std::string& error);
	

}

#endif // _DB_CONN_HPP_


