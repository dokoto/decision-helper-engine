package net.whatswith.screens.contacts;

import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Contacts_db;
import net.whatswith.structs.Contacts_db.FilterType;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Contacts extends ListActivity
{

	private ContactsAdapter adapter;
	private Contacts_db.FilterType filter = null;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		try
		{				
			Bundle b = getIntent().getExtras();
			int index = b.getInt("Filter");
			filter = Contacts_db.FilterType.at(index);
			if (null == filter)
			{
				Toast.makeText(this.getApplicationContext(), "Filter of Contacts has failed", Toast.LENGTH_LONG).show();
				this.finish();
			}
			else if (filter == FilterType.BY_NAME)			
				adapter = new ContactsAdapter(this, 
						Commands.sortAndFilterContacts(Main.Nexus.Storage.contacts, new Commands.CMP_Contact_db_Name() ));
			else if (filter == FilterType.BY_LOCK)
				adapter = new ContactsAdapter(this, 
						Commands.sortAndFilterContacts(Main.Nexus.Storage.contacts, new Commands.CMP_Contact_db_lock() ));
			
			setListAdapter(adapter);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public class ContactsAdapter extends ArrayAdapter<Contacts_db>
	{
		private final Context context;
		ArrayList<Contacts_db> rows;

		class ViewHolder
		{
			public ImageView iv_UserPhoto;
			public TextView tvUserName;
			public TextView tvUserPhone;
			public CheckBox ib_icoSelect;
		}

		public ContactsAdapter(Context context, ArrayList<Contacts_db> Rows)
		{
			super(context, R.layout.layo_row_contacts, Rows);
			this.context = context;
			this.rows = Rows;
		}
				
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{			
			View rowView = convertView;							
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_contacts, parent, false);
				ViewHolder viewHolder = new ViewHolder();
				viewHolder.iv_UserPhoto = (ImageView) rowView.findViewById(R.id.layo_row_contacts_iv_photo);
				viewHolder.tvUserName = (TextView) rowView.findViewById(R.id.layo_row_contacts_tv_userName);
				viewHolder.tvUserPhone = (TextView) rowView.findViewById(R.id.layo_row_contacts_tv_phoneNumber);
				viewHolder.ib_icoSelect = (CheckBox) rowView.findViewById(R.id.layo_row_contacts_cb_selector);
				rowView.setTag(viewHolder);
			}

			ViewHolder holder = (ViewHolder) rowView.getTag();
			holder.tvUserName.setText(rows.get(position).name);
			holder.tvUserPhone.setText(rows.get(position).phone);
			if (rows.get(position).is_selected)
				holder.ib_icoSelect.setChecked(true);
			else
				holder.ib_icoSelect.setChecked(false);
			
			if (rows.get(position).is_locked)
				holder.ib_icoSelect.setEnabled(false);
			else
				holder.ib_icoSelect.setEnabled(true);
										
			if (rows.get(position).has_photo)
				holder.iv_UserPhoto.setImageBitmap(rows.get(position).photo);
			else
				holder.iv_UserPhoto.setImageResource(R.drawable.ic_menu_emoticons);
			
			holder.ib_icoSelect.setTag(Integer.valueOf(position));
									 
			holder.ib_icoSelect.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					int position = (Integer) v.getTag();
					Contacts_db screen_pointer = rows.get(position);
					Contacts_db db_pointer = Main.Nexus.Storage.contacts.get(screen_pointer.phone);
					if (screen_pointer.is_selected)
					{
						db_pointer.is_selected = false;
						screen_pointer.is_selected = false;
					}
					else
					{
						db_pointer.is_selected = true;
						screen_pointer.is_selected = true;
					}
					notifyDataSetChanged();
				}
			});

			notifyDataSetChanged();
			return rowView;
		}						
	}
	
} // END CLASS
