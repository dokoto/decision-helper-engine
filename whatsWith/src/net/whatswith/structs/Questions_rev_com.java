package net.whatswith.structs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONObject;

public class Questions_rev_com
{
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
	public int question_id;
	public Date creation_date;		
	public int phase_id;
	public boolean is_phase_accepted = false;

	
	public Questions_rev_com(int question_id, String creation_date, int phase_id, boolean is_phase_accepted)
	{
		try
		{
			this.question_id = question_id;			
			this.creation_date = dateFormat.parse(creation_date);			
			this.phase_id = phase_id;
		} catch (Exception e)
		{			
			e.printStackTrace();
		}
	}		
	public Questions_rev_com(JSONObject ojb)
	{
		try
		{
			this.question_id = ojb.getInt("QUESTION_ID");			
			this.creation_date = dateFormat.parse(ojb.getString("CREATION_DATE"));			
			this.phase_id = ojb.getInt("PHASE_ID");
			this.is_phase_accepted = ojb.getBoolean("IS_PHASE_ACCEPTED");
			
		} catch (Exception e)
		{ 
			e.printStackTrace();
		}
	}
	
}
