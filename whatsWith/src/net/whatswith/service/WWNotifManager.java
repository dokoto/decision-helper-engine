package net.whatswith.service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class WWNotifManager
{
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;
	private Activity mainAct;
	private Service mainServ;
	private int id;

	public WWNotifManager(int id, Activity act)
	{
		this.id = id;
		this.mainAct = act;
		this.mainServ = null;
		this.mBuilder = new NotificationCompat.Builder(this.mainAct);
		this.mNotificationManager = (NotificationManager) this.mainAct.getSystemService(Context.NOTIFICATION_SERVICE);
	}
	
	private Context getContext()
	{
		return (null == mainAct) ? mainServ : mainAct;
	}
	
	public WWNotifManager(int id, Service serv)
	{
		this.id = id;
		this.mainAct = null;
		this.mainServ = serv;
		this.mBuilder = new NotificationCompat.Builder(this.mainServ);
		this.mNotificationManager = (NotificationManager) this.mainServ.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	public void setLayo(int icon, String title, String text)
	{
		mBuilder.setSmallIcon(icon);
		mBuilder.setContentTitle(title);
		mBuilder.setContentText(text);
	}

	public void setNotify(Class<?> targetClass)
	{
		Intent notifyIntent = new Intent(getContext(), targetClass);
		PendingIntent PnotifyIntent = PendingIntent.getActivity(getContext(), 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(PnotifyIntent);
		mNotificationManager.notify(id, mBuilder.build());
	}
}
