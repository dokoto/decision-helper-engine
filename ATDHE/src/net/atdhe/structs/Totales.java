package net.atdhe.structs;

public class Totales
{
	public int phase;
	public int NumOfQuestionTotal;
	public int NumOfQuestionsAccepted;		
	
	public Totales(int phase, int NumOfQuestionTotal, int NumOfQuestionsAccepted)
	{
		this.phase = phase;
		this.NumOfQuestionTotal = NumOfQuestionTotal;
		this.NumOfQuestionsAccepted = NumOfQuestionsAccepted;
	}
		
}
