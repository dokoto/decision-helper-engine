package net.atdhe.tools;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import android.telephony.TelephonyManager;

public class Phone
{

	public static String getDeviceId(TelephonyManager tm)
	{
		return tm.getDeviceId();
	}

	public static String getLine1Number(TelephonyManager tm)
	{
		return tm.getLine1Number();
	}

	public static String getLine1Number_RFC3966(String SimCountryIso)
	{
		return getLine1Number_RFC3966(SimCountryIso);
	}

	public static String FormatNumber(String Phone, String SimCountryIso)
	{
		return FormatNumber(SimCountryIso, Phone, PhoneNumberFormat.RFC3966);
	}

	public static String FormatNumber(String SimCountryIso, String Phone, PhoneNumberFormat f)
	{
		try
		{
			// http://code.google.com/p/libphonenumber/
			// http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber NumberProto;
			NumberProto = phoneUtil.parse(Phone, SimCountryIso);
			return phoneUtil.format(NumberProto, f);
		} catch (Exception e)
		{
			return new String();
		}
	}

	public static String getLine1Number_RFC3966(TelephonyManager tm, String SimCountryIso)
	{
		try
		{
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber NumberProto;
			String Phone = tm.getLine1Number();
			NumberProto = phoneUtil.parse(Phone, SimCountryIso);
			Phone = phoneUtil.format(NumberProto, PhoneNumberFormat.RFC3966);
			return Phone;
		} catch (Exception e)
		{
			return new String();
		}
	}

	public static String getLine1Number_E164(String SimCountryIso)
	{
		return getLine1Number_E164(SimCountryIso);
	}

	public static String getLine1Number_E164(TelephonyManager tm, String SimCountryIso)
	{
		try
		{
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber NumberProto;
			String Phone = tm.getLine1Number();
			NumberProto = phoneUtil.parse(Phone, SimCountryIso);
			Phone = phoneUtil.format(NumberProto, PhoneNumberFormat.E164);
			return Phone;
		} catch (Exception e)
		{
			return new String();
		}
	}

	public static String getDeviceSoftwareVersion(TelephonyManager tm)
	{
		return tm.getDeviceSoftwareVersion();
	}

	public static String getNetworkOperatorName(TelephonyManager tm)
	{
		return tm.getNetworkOperatorName();
	}

	public static String getSimCountryIso(TelephonyManager tm)
	{
		return tm.getSimCountryIso();
	}

	public static String getSimOperatorName(TelephonyManager tm)
	{
		return tm.getSimOperatorName();
	}

	public static String getSimSerialNumber(TelephonyManager tm)
	{
		return tm.getSimSerialNumber();
	}

	public static String getSubscriberId(TelephonyManager tm)
	{
		return tm.getSubscriberId();
	}

	public static String getNetworkType(TelephonyManager tm)
	{
		return getNetworkTypeString(tm.getNetworkType());
	}

	public static String getPhoneType(TelephonyManager tm)
	{
		return getPhoneTypeString(tm.getPhoneType());
	}

	private static String getNetworkTypeString(int type)
	{
		String typeString = "Unknown";

		switch (type)
		{
		case TelephonyManager.NETWORK_TYPE_EDGE:
			typeString = "EDGE";
			break;
		case TelephonyManager.NETWORK_TYPE_GPRS:
			typeString = "GPRS";
			break;
		case TelephonyManager.NETWORK_TYPE_UMTS:
			typeString = "UMTS";
			break;
		default:
			typeString = "UNKNOWN";
			break;
		}

		return typeString;
	}

	private static String getPhoneTypeString(int type)
	{
		String typeString = "Unknown";

		switch (type)
		{
		case TelephonyManager.PHONE_TYPE_GSM:
			typeString = "GSM";
			break;
		case TelephonyManager.PHONE_TYPE_NONE:
			typeString = "UNKNOWN";
			break;
		default:
			typeString = "UNKNOWN";
			break;
		}

		return typeString;
	}

}
