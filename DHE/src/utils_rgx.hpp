#ifndef _UTILS_RGX_CPP_
#define _UTILS_RGX_CPP_

#include <string>
#include <vector>

namespace rgx
{
	bool CheckPattern(const char* const pattern, const char* const text);
	std::string FindAll(const char* const pattern, const char* const text, std::vector<std::string>& Results);
	std::string RemplaceAll(const char* const pattern, const char* const text, const char* const remplaceWith, std::string& Result);
}

#endif // _UTILS_RGX_CPP_


