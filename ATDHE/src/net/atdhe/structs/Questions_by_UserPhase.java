package net.atdhe.structs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

public class Questions_by_UserPhase
{
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
	public int question_id;
	public String question;
	public String user_id;
	public Date exp_date;
	public int phase_id;
	public boolean is_phase_accepted;

	public Questions_by_UserPhase(int question_id, String question, String user_id, String exp_date, int revision, int phase_id,
			boolean is_phase_accepted)
	{
		try
		{
			this.question_id = question_id;
			this.question = question;
			this.user_id = user_id;
			this.exp_date = dateFormat.parse(exp_date);
			this.phase_id = phase_id;
			this.is_phase_accepted = is_phase_accepted;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Questions_by_UserPhase(final int Question_id, final Question_db q)
	{
		try
		{
			this.question_id = Question_id;
			this.question = q.question;
			this.user_id = q.user_id;
			this.exp_date = q.exp_date;
			this.phase_id = q.phase_id;
			// MEJORA PARA EL ORIGINAL
			this.is_phase_accepted = q.is_phase_accepted;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Questions_by_UserPhase(JSONObject ojb)
	{
		try
		{
			this.question_id = ojb.getInt("QUESTION_ID");
			this.question = ojb.getString("QUESTION");
			this.user_id = ojb.getString("USER_ID");
			this.exp_date = dateFormat.parse(ojb.getString("EXP_DATE"));
			this.phase_id = ojb.getInt("PHASE_ID");
			this.is_phase_accepted = ojb.getBoolean("IS_PHASE_ACCEPTED");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
