package net.atdhe.screens.menus;

import net.atdhe.Main;
import net.atdhe.R;
import android.os.Bundle;
import android.app.Activity;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast; 

public class Debug_Settings extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_debug_settings);

		final EditText host = (EditText) findViewById(R.id.layo_settings_et_host);
		host.setText(Main.Nexus.Storage.host);

		final EditText port = (EditText) findViewById(R.id.layo_settings_et_port);
		port.setText(String.valueOf(Main.Nexus.Storage.port));

		final EditText tmpPhoneNumber = (EditText) findViewById(R.id.layo_settings_et_tmp_phone_number);
		tmpPhoneNumber.setText(Main.Nexus.Storage.phoneNumber());

		final CheckBox is_virtual_machine = (CheckBox) findViewById(R.id.layo_settings_cb_crazy_mode);
		is_virtual_machine.setChecked(Main.Nexus.Storage.crazy_mode);

		findViewById(R.id.layo_settings_bt_set_host).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				final EditText host = (EditText) findViewById(R.id.layo_settings_et_host);
				Main.Nexus.Storage.host = host.getText().toString();
				Main.Nexus.Serial.Save(Main.Nexus.Storage);
				Toast.makeText(getApplicationContext(), "Host has switch to :  " + host.getText().toString() + " , MASTER !!",
						Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.layo_settings_bt_set_port).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				final EditText port = (EditText) findViewById(R.id.layo_settings_et_port);
				Main.Nexus.Storage.port = Integer.parseInt(port.getText().toString());
				Main.Nexus.Serial.Save(Main.Nexus.Storage);
				Toast.makeText(getApplicationContext(), "Port has switch to :  " + port.getText().toString() + " , MASTER !!",
						Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.layo_settings_bt_set_tmp_phone_number).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				final EditText phone = (EditText) findViewById(R.id.layo_settings_et_tmp_phone_number);
				Main.Nexus.Storage.phoneNumber(phone.getText().toString(), Main.Nexus.SimCountryIso);

				Main.Nexus.Serial.Save(Main.Nexus.Storage);
				Toast.makeText(getApplicationContext(), "PhoneNumber has switch to : " + phone.getText().toString() + " , MASTER !!",
						Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.layo_settings_bt_send_sms).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				try
				{
					final EditText phone = (EditText) findViewById(R.id.layo_settings_et_send_sms);
					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage(phone.getText().toString(), null, "Whats With: Esto es un SMS de prueba", null, null);
					Toast.makeText(getApplicationContext(), "SMS Sent!", Toast.LENGTH_LONG).show();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
/*
		findViewById(R.id.layo_settings_bt_set_password).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				try
				{
					final EditText password_new = (EditText) findViewById(R.id.layo_settings_et_password);
					if (Main.Nexus.Storage.password.compareTo(password_new.getText().toString()) == 0)
					{
						Toast.makeText(getApplicationContext(), "New passwd must be diferent", Toast.LENGTH_SHORT).show();
						return;
					}

					AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
						private ProgressDialog pd;
						private Return ret;

						@Override
						protected void onPreExecute()
						{
							pd = new ProgressDialog(Debug_Settings.this);
							pd.setTitle("Changing Password...");
							pd.setMessage("Please wait.");
							pd.setCancelable(false);
							pd.setIndeterminate(true);
							pd.show();
						}

						@Override
						protected Void doInBackground(Void... arg0)
						{
							ret = Commands.PasswordChange(Main.Nexus.Storage.login(), password_new.getText().toString());
							return null;
						}

						@Override
						protected void onPostExecute(Void result)
						{
							pd.dismiss();
							if (ret.status == true)
							{
								Main.Nexus.Storage.password = password_new.getText().toString();
								Main.Nexus.Serial.Save(Main.Nexus.Storage);
								Toast.makeText(getApplicationContext(), "New Password Save , MASTER !!", Toast.LENGTH_SHORT).show();
							} else
							{
								Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
							}
						}

					};
					task.execute((Void[]) null);

				} catch (Exception e)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
				}

			}
		});
*/
		findViewById(R.id.layo_settings_cb_crazy_mode).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				final CheckBox crazy_mode = (CheckBox) findViewById(R.id.layo_settings_cb_crazy_mode);
				Main.Nexus.Storage.crazy_mode = crazy_mode.isChecked();
				Main.Nexus.Serial.Save(Main.Nexus.Storage);
				Toast.makeText(getApplicationContext(), "Crazy Mode activated, MASTER !!", Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.layo_settings_cb_desactv_conn).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0)
			{
				final CheckBox desactv_conn = (CheckBox) findViewById(R.id.layo_settings_cb_desactv_conn);
				Main.Nexus.Storage.desactv_conn = desactv_conn.isChecked();
				Main.Nexus.Serial.Save(Main.Nexus.Storage);
				Toast.makeText(getApplicationContext(), "Check Connection des-activated, MASTER !!", Toast.LENGTH_SHORT).show();
			}
		});

	}

}
