package net.whatswith.screens.questions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.tools.DatePickerFragment;
import net.whatswith.R;
import android.os.Bundle;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.FragmentActivity;

public class Add_question extends FragmentActivity
{

	private EditText Et_exp_date;
	private Adapter_AddQuestion Adapter;
	private ListView lv_proposals;

	private class Adapter_row
	{

		public Adapter_row(ImageView ico, String proposal)
		{
			this.Proposal = proposal;
			this.Ico = ico;
		}

		public ImageView Ico;
		public String Proposal;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{

		try
		{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.layo_add_question);

			Et_exp_date = (EditText) findViewById(R.id.layo_add_question_et_exp_date);
			lv_proposals = (ListView) findViewById(R.id.layo_add_question_lv_proposal);
			EditText et_delay_days = (EditText) findViewById(R.id.layo_add_question_et_delay_between_phases);
			et_delay_days.setText("5");
			final ArrayList<Adapter_row> adapter_row = new ArrayList<Adapter_row>();
			Adapter = new Adapter_AddQuestion(this, adapter_row);
			lv_proposals.setAdapter(Adapter);

			findViewById(R.id.layo_add_question_ib_exp_date).setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0)
				{
					showDataPicker();
				}

			});

			findViewById(R.id.layo_add_question_ib_proposal).setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					EditText et_proposal = (EditText) findViewById(R.id.layo_add_question_et_proposal);
					ImageView iv_del_proposal = new ImageView(Add_question.this);
					iv_del_proposal.setImageResource(R.drawable.ic_menu_delete);
					Adapter.Rows.add(0, new Adapter_row(iv_del_proposal, et_proposal.getText().toString()));
					et_proposal.setText("");
					et_proposal.setHint("Proposal");
					et_proposal.requestFocus();
					Adapter.notifyDataSetChanged();

				}
			});

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void showDataPicker()
	{
		DatePickerFragment date = new DatePickerFragment();
		Calendar calender = Calendar.getInstance();
		Bundle args = new Bundle();
		args.putInt("year", calender.get(Calendar.YEAR));
		args.putInt("month", calender.get(Calendar.MONTH));
		args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
		date.setArguments(args);
		date.setCallBack(new OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				Et_exp_date.setText(((dayOfMonth < 10) ? "0" : "") + String.valueOf(dayOfMonth) + "/" + (((monthOfYear+1) < 10) ? "0" : "")
						+ String.valueOf(monthOfYear+1) + "/" + String.valueOf(year));
				lv_proposals.requestFocus();
			}
		});
		date.show(getSupportFragmentManager(), "Date Picker");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_add_question, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.menu_add_question_save)
			save_Open_Question();
		else
			Main.Nexus.MenuSelector(this, item);

		return true;
	}

	private boolean ET_CheckEmpty(final String ErrorText, EditText obj)
	{
		if (obj.getText().toString().isEmpty())
		{
			obj.setError(ErrorText);
			return true;
		} else
		{
			obj.setError(null);
			return false;
		}
	}

	private void save_Open_Question()
	{
		try
		{
			boolean error = false;
			EditText et_question = (EditText) findViewById(R.id.layo_add_question_et_question);
			if (ET_CheckEmpty("Question is mandatory.", et_question))
				error = true;
			

			EditText et_exp_date = (EditText) findViewById(R.id.layo_add_question_et_exp_date);
			if (ET_CheckEmpty("Expiration Date is mandatory.", et_exp_date))
				error = true;

			EditText et_delay_days = (EditText) findViewById(R.id.layo_add_question_et_delay_between_phases);
			if (ET_CheckEmpty("Delay days between phases is mandatory.", et_delay_days))
				error = true;
			int delay_days = Integer.valueOf(et_delay_days.getText().toString()); 
			if ( delay_days > 365 || delay_days < 0)
			{
				et_delay_days.setError("Delay days between phases can't be great than 365 days or less equal than zero days");
				error = true;
			}	
			
			SimpleDateFormat OutputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
			SimpleDateFormat InputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.getDefault());
			Date exp_date = null;
			try
			{
				exp_date = OutputFormat.parse(et_exp_date.getText().toString());
				if (exp_date.compareTo(new Date()) < 0)
				{
					et_exp_date.setError("Expiration Date must be great than current date.");
					error = true;
				} else et_exp_date.setError(null);
			} catch (ParseException e)
			{
				et_exp_date.setError("Expiration Date invalid format. Use 'dd/MM/yyyy'");
				error = true;
			}			

			EditText et_proposal = (EditText) findViewById(R.id.layo_add_question_et_proposal);

			ArrayList<String> proposals = new ArrayList<String>();
			if (Adapter.Rows.size() > 0)
			{
				for (Adapter_row row : Adapter.Rows)
					proposals.add(Commands.JsonSpecialCharFixer(row.Proposal));			
				et_proposal.setError(null);
			} else if (et_proposal.getText().toString().isEmpty())
			{
				et_proposal.setError("The proposals are mandatory, at least write one.");
				error = true;
			} 
			
			if (!et_proposal.getText().toString().isEmpty())
			{
				proposals.add(Commands.JsonSpecialCharFixer(et_proposal.getText().toString()));
				et_proposal.setError(null);
			}

			String[] users_ids = Commands.ContactsToArrayOnlySelected(Main.Nexus.Storage.contacts, true);
			if (users_ids.length == 0)
			{
				et_question.setError("Is mandatory selecting a least one contact.");
				error = true;
			} 
			else if (users_ids.length == 1 && users_ids[0].compareTo(Main.Nexus.Storage.phoneNumber()) == 0)
			{
				et_question.setError("Is mandatory selecting a least one contact.");
				error = true;
			}
			else et_question.setError(null);			
						
			if (error == false)
			{
				Commands.Async_Open_Question(Main.Nexus.Storage.login(), Add_question.this, null, Commands.JsonSpecialCharFixer(et_question.getText().toString()),
						InputFormat.format(exp_date), delay_days, proposals.toArray(new String[proposals.size()]), users_ids, true);			
			}
						
		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), "ERROR : " + e.toString(), Toast.LENGTH_LONG).show();
		}
	}

	private static class ViewHolder
	{
		public ImageView iv_ico;
		public TextView et_proposal;
	}
	
	public class Adapter_AddQuestion extends ArrayAdapter<Adapter_row>
	{
		public final Context ContextAdapter;
		public ArrayList<Adapter_row> Rows;
		ViewHolder holder;



		public Adapter_AddQuestion(Context context, ArrayList<Adapter_row> rows)
		{
			super(context, R.layout.layo_add_question, rows);
			this.ContextAdapter = context;
			this.Rows = rows;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) ContextAdapter.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_proposal_add_del, parent, false);
				holder = new ViewHolder();
				holder.iv_ico = (ImageView) rowView.findViewById(R.id.layo_proposal_add_del_bt_del);
				holder.et_proposal = (TextView) rowView.findViewById(R.id.layo_proposal_add_del_et_proposal);
				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();
			if (Rows.size() == 0)
			{
				holder.et_proposal.setVisibility(View.GONE);
				holder.iv_ico.setVisibility(View.GONE);
			} else
			{
				//holder.et_proposal.setEnabled(false);
				holder.et_proposal.setText(Rows.get(position).Proposal);
				holder.iv_ico.setImageResource(R.drawable.ic_menu_delete);
				holder.iv_ico.setTag(Integer.valueOf(position));
				holder.iv_ico.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(final View view)
					{
						int position = (Integer) view.getTag();
						Rows.remove(position);
						notifyDataSetChanged();
					}
				});
			}
			notifyDataSetChanged();
			return rowView;
		}

	}

}
