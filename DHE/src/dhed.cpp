#include "dhed.hpp"
#include "utils_macros.hpp"
#include "utils_rgx.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>

namespace dhe
{

	const char* argsTX[] = 
	{"--confpath", "certfile", "keyfile", "max_password_fails", "port", "proxy_user",
	"proxy_password", "max_pool_size", "db_host", "db_name", "db_user", 
	"db_password", "encrypt_method", "timeout_in_secs", "log_to_file", "log_path", "log_max_size"};


	dhed::dhed(const int argc, const char** argv) : 
		NumOfArgs(argc), 
		ArrayOfArgs(argv), 
		ctx(NULL), 
		server_sock_fd(0), 
		client_sock_fd(0), 
		Thread_RC(0),		
		max_pool_size(0), 
		timeout_in_secs(0),
		max_password_fails(0)
	{
		memset(port, 0, MAX_LENGTH_PORT);
	}

	dhed::dhed(void) : 
		NumOfArgs(0), 
		ArrayOfArgs(NULL), 
		ctx(NULL), 
		server_sock_fd(0), 
		client_sock_fd(0),
		Thread_RC(0),
		max_pool_size(0), 
		timeout_in_secs(0),
		max_password_fails(0)
	{
		memset(port, 0, MAX_LENGTH_PORT);
	}


	dhed::dhed(const dhed& d) : 
		NumOfArgs(0), 
		ArrayOfArgs(NULL), 
		ctx(NULL), 
		server_sock_fd(0), 
		client_sock_fd(0), 
		Thread_RC(0),		
		max_pool_size(0), 
		timeout_in_secs(0),
		max_password_fails(0)
	{ 
		NOT_USED(d); 
		memset(port, 0, MAX_LENGTH_PORT);
	}

	dhed& dhed::operator=(const dhed& d) { NOT_USED(d); return *this; }

	dhed::~dhed() 
	{
		// Se Liberan todas las funcionalidades relacionadas con la comunicacion SSL
		SSL_Destructor();
		closePgDB();
		pthread_attr_destroy(&Thread_Attr);
		CloseCoscketSession(server_sock_fd);			
		ERR_free_strings();
		SSL_CTX_free(ctx);
	}

	bool dhed::run(void)
	{
		while(true)
		{
			struct sockaddr_in addr;
			socklen_t len = sizeof(addr);
			int thread_rc = 0;
			pthread_t Thread_ID;

			client_sock_fd = accept(server_sock_fd, (struct sockaddr*)&addr, &len);
            if (-1 == client_sock_fd) continue;
			SetTimeOut(client_sock_fd, timeout_in_secs);
			INFO_T("************* NEW CONNECTION [%s:%d] ***********************", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
            thread_args_t thread_args = {ctx, client_sock_fd, max_password_fails};
			thread_rc = pthread_create(&Thread_ID, &Thread_Attr, ServetThread, reinterpret_cast<void*>(&thread_args)); /* service connection by isolate thread*/

			client_sock_fd = 0;
			len = 0;			
			memset(&addr, 0, sizeof(struct sockaddr));
		}
		return true;
	}


	bool dhed::parse_args(void)
	{
		if (NumOfArgs == 1)
		{
			printf("\nDecision Helper Engine Daemon.\n"
				"Use: Command [--argument] [value of Argument]\n\n"
				"--confpath : Path to Config file. Always with quotes. Ej: --confpath \"../../../conf/dhed.conf\"\n\n");
			return false;
		}

		INFO("Parsing initial aguments..");
		if (NumOfArgs != MAX_NUM_OF_ARGS + 1)
		{
			ERR_T("The right number of arguments is : %d", MAX_NUM_OF_ARGS);
			return false;
		}

		int i = 1;
		while(i < NumOfArgs)
		{
			if (strcmp(argsTX[CONFPATH], ArrayOfArgs[i]) == 0)
			{
				passArgs.insert( std::make_pair(CONFPATH, ArrayOfArgs[i+1]) );			
				if (!file_exist(ArrayOfArgs[i+1]))
				{
					ERR_T("The path '%s' no found.", ArrayOfArgs[i+1]);
					return false;
				}
				i += 2;    
				continue;
			}
			else
			{
				ERR_T("Param '%s' no allowed. Use command without arguments to get help.", ArrayOfArgs[i]);
				return false;
			}
		}

		return true;
	}

	bool dhed::configure_serlet(void)
	{
		try
		{
			utils_macros_log_to_file = (confArgs[LOG_TO_FILE].compare("TRUE") == 0) ? true : false;
			utils_macros_log_max_fize = atoi(confArgs[LOG_MAX_SIZE].c_str());
			if (confArgs[LOG_PATH].size() > 0 && confArgs[LOG_PATH].size() < UTIL_MACROS_MAX_LENGHT_PATH)			
			{
				memset(utils_macros_log_path, 0, UTIL_MACROS_MAX_LENGHT_PATH);
				strncpy(&utils_macros_log_path[0], confArgs[LOG_PATH].c_str(), confArgs[LOG_PATH].size());						
				if (!(utils_macros_log_max_fize < GB_IN_BYTES && utils_macros_log_max_fize > 0))
				{
					utils_macros_log_to_file = false;
					PERR_T("Log max lenght must be between 0 and 1GB : %d", utils_macros_log_max_fize);	
					PERR("Switching to log to stdout");
				}
				if (utils_macros_log_to_file)
					PINFO_T("Switching to log to file: %s", utils_macros_log_path);
			}
			else
			{
				utils_macros_log_to_file = false;
				PERR_T("Log path must be between 1 and %s charaters : %d, switching to stdout.", utils_macros_log_path, UTIL_MACROS_MAX_LENGHT_PATH);				
				PINFO("Switching to log to stdout");
			}

			
			INFO("Configuring dhed..");			
			INFO_T("Log path set to : %s", (utils_macros_log_to_file)? "TRUE" : "FALSE");
			INFO_T("Log path : %s", utils_macros_log_path);			
			INFO_T("Log max lenght set : %d", utils_macros_log_max_fize);

			INFO_T("PostgreSQL thread safe mode : %s", (PQisthreadsafe() == 0)? "FALSE" : "TRUE");
			strncpy(port, confArgs[PORT].c_str(), confArgs[PORT].size());
			INFO_T("Set DHED Service port to : %s", port);

			max_pool_size = atoi(confArgs[MAX_POOL_SIZE].c_str());
			INFO_T("Set max Pool of Connections to : %d", max_pool_size);

			timeout_in_secs = atoi(confArgs[TIMEOUT_IN_SECS].c_str());
			INFO_T("Set timeOut in seconds to : %ld", timeout_in_secs);

			max_password_fails = atoi(confArgs[MAX_PASSWORD_FAILS].c_str());
			if (max_password_fails < 0)
			{
				ERR_T("'MAX_PASSWORD_FAILS' parameter must greate than 0 and is : %d", max_password_fails);
				return false;
			}
			INFO_T("Set max password fails to : %d", max_password_fails);

			INFO("Open DataBase Connection..");
			if ( !init_db_conn(confArgs[DB_HOST], confArgs[DB_NAME], confArgs[DB_USER], confArgs[DB_PASSWORD]) )
				return false;
			
			// Se inicializan todas las funcionalidades relacionadas con la comunicacion SSL
			SSL_Constructor();
			INFO_T("Data Base [%s] conneccted successful in [%s] with user [%s]", confArgs[DB_NAME].c_str(), 
				confArgs[DB_HOST].c_str(), confArgs[DB_USER].c_str());

			INFO("Init pthreads engine..");
			if (0 != pthread_attr_init(&Thread_Attr))
			{
				ERR("Thread initialization has failed");
				return false;
			}
			pthread_attr_setdetachstate(&Thread_Attr, PTHREAD_CREATE_DETACHED);
			INFO("Init openSSL engine..");
			SSL_library_init();	
			encrypt_method_t encrypt_method;
			if ( confArgs[ENCRYPT_METHOD].compare(encryptTX[TLSv1]) == 0 )
				encrypt_method = TLSv1;
			else if ( confArgs[ENCRYPT_METHOD].compare(encryptTX[TLSv1_1]) == 0 )
				encrypt_method = TLSv1_1;
			else if ( confArgs[ENCRYPT_METHOD].compare(encryptTX[TLSv1_2]) == 0 )
				encrypt_method = TLSv1_2;
			else if ( confArgs[ENCRYPT_METHOD].compare(encryptTX[SSLv2]) == 0 )
				encrypt_method = SSLv2;
			else if ( confArgs[ENCRYPT_METHOD].compare(encryptTX[SSLv3]) == 0 )
				encrypt_method = SSLv3;
			else
			{
				ERR_T("openSSL initialization has failed, encryption type : %s unknowed.", confArgs[ENCRYPT_METHOD].c_str());
				return false;
			}
			ctx = InitServerCTX( encrypt_method );
			if (NULL == ctx)
			{
				ERR("openSSL initialization has failed");
				return false;
			}

			if (LoadCertificates(ctx, confArgs[CERTFILE].c_str(), confArgs[KEYFILE].c_str()) == false)
				return false;		
			INFO_T("Encrypted method %s..", confArgs[ENCRYPT_METHOD].c_str());


			if (SetSocketToListen(atoi(port), max_pool_size, server_sock_fd) == false) 
				return false;

			confArgs.clear();
			INFO_T("DHED listening in port %s", port);

			return true;
		}
		catch (const std::exception& e) 
		{
			PERR_T("[%s] Unexpect exception : %s", __FUNCTION__, e.what());
			return false;
		}
		catch(...)
		{
			PERR_T("[%s] unknown exception!", __FUNCTION__);
			return false;
		}
	}

	bool dhed::parse_arg(const int arg, const std::string& line, std::map<int, std::string>& confArgs)
	{	
		size_t posDiv = 0;
		std::string value, key;	
		posDiv = line.find_first_of("=");
		if (posDiv == std::string::npos) 
		{
			PERR_T("'=' separator not found in %s.", line.c_str());
			return false;
		}
		key = line.substr(0, posDiv);
		rgx::RemplaceAll("((\")|(\\s))", key.c_str(), "", key);
		if (key.compare(argsTX[arg]) == 0)
		{		
			value = line.substr(posDiv+1, line.size());		
			rgx::RemplaceAll("((\")|(\\s))", value.c_str(), "", value);		
			confArgs[arg] = value;
			if (strcmp(argsTX[arg], "db_password") != 0 && strcmp(argsTX[arg], "proxy_password") != 0)
				PINFO_T("Adding [ %s ] : %s", argsTX[arg], value.c_str());
			return true;
		}
		else
			return false;
	}

	bool dhed::parse_conf(void)
	{
		PINFO("Getting config parameters");
		std::ifstream file_in(passArgs[CONFPATH].c_str());
		if (file_in.is_open())
		{
			std::string line;
			while (std::getline(file_in, line))
			{
				if (line.empty()) continue;

				if (line[0] == '#') continue;
				if (line[0] == '\r') continue;

				if (parse_arg(CERTFILE, line, confArgs))
				{
					if (confArgs[CERTFILE].size() > MAX_LENGHT_PATH)
					{
						PERR_T("The path lenght must be less than %d characters", MAX_LENGHT_PATH);
						return false;
					}
					if (!file_exist(confArgs[CERTFILE].c_str()))
					{
						PERR_T("File not found : %s ", confArgs[CERTFILE].c_str() );
						return false;
					}
					continue;
				}

				else if (parse_arg(KEYFILE, line, confArgs))
				{
					if (confArgs[KEYFILE].size() > MAX_LENGHT_PATH)
					{
						PERR_T("The path lenght must be less than %d characters", MAX_LENGHT_PATH);
						return false;
					}
					if (!file_exist(confArgs[KEYFILE].c_str()))
					{
						PERR_T("File not found : %s ", confArgs[KEYFILE].c_str() );
						return false;
					}
					continue;
				}

				else if (parse_arg(MAX_PASSWORD_FAILS, line, confArgs))
					continue;

				else if (parse_arg(PORT, line, confArgs))
					continue;			

				else if (parse_arg(MAX_POOL_SIZE, line, confArgs))
					continue;

				else if (parse_arg(DB_HOST, line, confArgs))
					continue;			

				else if (parse_arg(DB_NAME, line, confArgs))
					continue;

				else if (parse_arg(DB_USER, line, confArgs))
					continue;

				else if (parse_arg(DB_PASSWORD, line, confArgs))
					continue;			

				else if (parse_arg(ENCRYPT_METHOD, line, confArgs))
					continue;

				else if (parse_arg(TIMEOUT_IN_SECS, line, confArgs))
					continue;

				else if (parse_arg(LOG_TO_FILE, line, confArgs))
					continue;

				else if (parse_arg(LOG_PATH, line, confArgs))
					continue;

				else if (parse_arg(LOG_MAX_SIZE, line, confArgs))
					continue;

				else
				{
					PINFO_T("Unknown Config Argument : %s", line.c_str());
					continue;
				}			
			}

			file_in.close();
			return true;
		}

		return false;
	}

}




