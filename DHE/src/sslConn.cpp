#include "sslConn.hpp"
#ifdef _MSC_VER
extern "C"
{
#include "openssl/applink.c"
}
#endif
extern "C"
{
#include "contrib/cJSON/cJSON.h"
}
#include <sstream> 
#include <map>
#include <string>
#include <cstring>
#include <cerrno>
#include <memory>
#include "utils_macros.hpp"
#include "utils_rgx.hpp"
#include "IPChecker.hpp"
#include "ThreadAcctions.hpp"
#include "contrib/utf8.h"

#include "utils_proc.hpp"

namespace dhe
{
	const char* encryptTX[] = {"TLSv1", "TLSv1_1", "TLSv1_2", "SSLv2", "SSLv3"};
	typedef bool (*ptr_func_t)(const int, cJSON*, std::string&);
	std::map<std::string, ptr_func_t> funcs_pool;
	static ipchecker IpChecker;
	static ThreadAcctions RootAcctions;
	pthread_mutex_t* mutex_buffer;

	static std::size_t utf8_length(const int SessionID, std::string const &s);
	static void getIPfromSockFD(const int sockFD, std::string& IP);
	static bool reading_process(const int SessionID, SSL* ssl, std::string& request);
	static bool writing_process(const int SessionID, SSL* ssl, const std::string& response);	
	static bool process_request_and_response(const int SessionID, const std::string& ip, const char* const request, std::string& response, bool& is_auth);
	static bool user_is_auth(const int SessionID, const bool auth, cJSON* root, std::string& response );
	static const std::string buildError(const std::string& errMsg);
	static const char* buildOk(void);
	static const char* buildOk(bool result);

	// Dynamic functions	
	static bool USER_PASSWORD_EXIST(const int SessionID, cJSON* root, std::string& response );
	static bool USER_EXIST(const int SessionID, cJSON* root, std::string& response );
	static bool USER_ARRAY_EXIST(const int SessionID, cJSON* root, std::string& response );
	static bool USER_ADD(const int SessionID,  cJSON* root, std::string& response );
	static bool PASSWORD_CHANGE(const int SessionID, cJSON* root, std::string& response );
	static bool USER_LOCK(const int SessionID, cJSON* root, std::string& response );
	static bool USER_UNLOCK(const int SessionID, cJSON* root, std::string& response );    

	static bool SYS_TERMINATE_THREAD(const int SessionID, cJSON* root, std::string& response );    
	static bool SYS_BANNED_IP(const int SessionID, cJSON* root, std::string& response );  
	static bool SYS_UNBANNED_IP(const int SessionID, cJSON* root, std::string& response );

	static bool QUESTION_OPEN(const int SessionID, cJSON* root, std::string& response );		
	static bool QUESTION_ACCEPT(const int SessionID, cJSON* root, std::string& response );
	static bool QUESTION_SWITCH_FASE(const int SessionID, cJSON* root, std::string& response );
	static bool QUESTION_DEPRECATED(const int SessionID, cJSON* root, std::string& response );
	static bool QUESTION_GET_LAST_REV(const int SessionID, cJSON* root, std::string& response );
	static bool QUESTION_IS_ACCEPTED(const int SessionID,  cJSON* root, std::string& response );
	static bool QUESTION_ADD_USERS(const int SessionID, cJSON* root, std::string& response );

	static bool PROPOSALS_ADD(const int SessionID, cJSON* root, std::string& response );
	static bool USERPROPOSALS_ADD(const int SessionID, cJSON* root, std::string& response );

	static bool SYNC_GET_QUESTIONS_REVS_OF_USER(const int SessionID, cJSON* root, std::string& response );
	static bool SYNC_GET_QUESTIONS_BY_USER_PHASE(const int SessionID, cJSON* root, std::string& response );
	static bool SYNC_GET_TOP_PROPOSALS_OF_QUESTION(const int SessionID, cJSON* root, std::string& response );
	static bool SYNC_GET_QUESTIONS_SORT(const int SessionID, cJSON* root, std::string& response );
	static bool SYNC_GET_PHASE_SORT(const int SessionID, cJSON* root, std::string& response );
	static bool SYNC_GET_SORT_PROPOSALS_OF_QUESTION(const int SessionID, cJSON* root, std::string& response );	
	static bool SYNC_GET_USERS_OF_QUESTION(const int SessionID, cJSON* root, std::string& response );	

	static bool SESSION_CLOSE(const int SessionID, cJSON* root, std::string& response );
	static bool SESSION_PING(const int SessionID, cJSON* root, std::string& response );

	static bool DEBUG_ARE_THERE_QUESTIONS_IN_PHASE(const int SessionID, cJSON* root, std::string& response );

	static unsigned long thread_id_function(void)
	{
#ifdef _MSC_VER	
		return ( (unsigned long) GetCurrentThreadId() );
#else
		return ((unsigned long) pthread_self() );
#endif
	}

	static void locking_function(int mode, int id, const char* file, int line)
	{
		NOT_USED(file);
		NOT_USED(line);
		if(mode & CRYPTO_LOCK)
			pthread_mutex_lock(&mutex_buffer[id]);
		else
			pthread_mutex_unlock(&mutex_buffer[id]);
	}

	static void ConstructThreadSafeSSL(void)
	{
		mutex_buffer = new pthread_mutex_t[CRYPTO_num_locks()];
		for (int i = 0; i < CRYPTO_num_locks(); i++)
		{
			pthread_mutex_init(&mutex_buffer[i], NULL);
		}
		CRYPTO_set_id_callback(thread_id_function);
		CRYPTO_set_locking_callback(locking_function);
	}

	static void DestructThreadSafeSSL(void)
	{
		CRYPTO_set_id_callback(NULL);
		CRYPTO_set_locking_callback(NULL);
		for (int i = 0; i < CRYPTO_num_locks(); i++) 
		{
			pthread_mutex_destroy(&mutex_buffer[i]);
		}
		delete mutex_buffer;
		mutex_buffer = NULL; 
	}

	void SSL_Constructor(void)
	{
		init_map_of_pointer_funcs();
		ConstructThreadSafeSSL();
	}

	void SSL_Destructor(void)
	{
		DestructThreadSafeSSL();
	}

	bool CloseSocket(int sockFD)
	{
#ifdef _MSC_VER	
		int iResult = closesocket(sockFD);
		if (iResult == SOCKET_ERROR) 
		{
			ERR_T("closesocket failed with error = %d", WSAGetLastError() );     
			return false;
		}  		
		return true;
#else
		return (close(sockFD) == 0)? true : false;
#endif
	}

	bool CloseCoscketSession(int sockFD)
	{
#ifdef _MSC_VER	
		int iResult = closesocket(sockFD);
		if (iResult == SOCKET_ERROR) 
		{
			ERR_T("closesocket failed with error = %d", WSAGetLastError() );     
			WSACleanup();
			return false;
		}  		
		WSACleanup();
		return true;
#else
		return (close(sockFD) == 0)? true : false;
#endif
	}

	const std::string buildError(const std::string& errMsg)
	{
		std::stringstream ss;
		ss << "{\"RESULT\":false,\"ERROR\":\"" << errMsg << "\"}";
		return ss.str();
	}

	const char* buildOk(bool result)
	{		
		return (result) ? "{\"RESULT\":true,\"ERROR\":\"\"}" : "{\"RESULT\":false,\"ERROR\":\"\"}";	
	}

	const char* buildOk(void)
	{
		return "{\"RESULT\":true,\"ERROR\":\"\"}";
	}

	bool USER_PASSWORD_EXIST(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* passwd = cJSON_GetObjectItem(Inroot, "PASSWD");
		if (user_id == NULL || passwd == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s, PASSWORD: %s" ,SessionID, __FUNCTION__, user_id->valuestring, passwd->valuestring);

		std::string error;
		if(user_password_exist(user_id->valuestring, passwd->valuestring, error))
		{			
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			ERR_T("[%5d] User & Password authentication failed. %s", SessionID, error.c_str());
			response.append(buildError("User & Password authentication failed" + ( (error.empty() )? ".":" : " + error )  ));
			return false;
		}		
	}

	bool PASSWORD_CHANGE(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* passwd_old = cJSON_GetObjectItem(Inroot, "PASSWD_OLD");
		cJSON* passwd_new = cJSON_GetObjectItem(Inroot, "PASSWD_NEW");
		if (user_id == NULL || passwd_old == NULL || passwd_new == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s, PASSWORD_OLD: %s, PASSWORD_NEW: %s", SessionID, __FUNCTION__, user_id->valuestring,
			passwd_old->valuestring, passwd_new->valuestring);


		std::string error;
		if(password_change(user_id->valuestring, passwd_old->valuestring, passwd_new->valuestring, error))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			ERR_T("[%5d][%s] User & Password authentication failed. %s", SessionID, __FUNCTION__, error.c_str());
			response.append(buildError("User & Password authentication failed" + ( (error.empty() )? ".":" : " + error )  ));
			return false;
		}
	}


	bool USER_EXIST(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		if (user_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID,  __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s", SessionID, __FUNCTION__, user_id->valuestring);

		std::string error;
		if(user_exist(user_id->valuestring, error))
		{			
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			ERR_T("[%5d] User does no Exist. %s", SessionID, error.c_str());
			response.append(buildError("User doesn't Exist" + ( (error.empty() )? ".":" : " + error )  ));
			return false;
		}		
	}

	bool USER_ARRAY_EXIST(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_users_ids = cJSON_GetObjectItem(Inroot, "USERS_IDS");
		if (cJSON_users_ids == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s]", SessionID, __FUNCTION__);

		std::vector<std::string> users_ids;      
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_users_ids); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_users_ids, i);
			users_ids.push_back(subitem->valuestring);
		}

		std::vector<user_array_exist_t> result;
		std::string error;
		user_array_exist(users_ids, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);

			for(std::vector<user_array_exist_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(line, "USER_ID", cJSON_CreateString(it->user_id.c_str()));
				cJSON_AddItemToObject(line, "SIGN_DATE", cJSON_CreateString(it->sign_date.c_str()));
				cJSON_AddItemToArray(subitems, line);
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			if (error.empty() == false)
			{
				rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
				for (unsigned i = 0; i < error.size(); i += 50)
					ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
				response.append(buildError(error));
			}
			else
				response.append(buildError("You haven't any shared contact in the Data Base. "));
			return false;
		}

	}

	bool USER_ADD(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* passwd = cJSON_GetObjectItem(Inroot, "PASSWD");
		if (user_id == NULL || passwd == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s, PASSWORD: %s", SessionID, __FUNCTION__, user_id->valuestring, passwd->valuestring);		

		if ( std::string(user_id->valuestring).size() < 5)
		{
			ERR_T("[%5d] Request [%s] :> User ID lenght can't be less than 5 characters", SessionID, __FUNCTION__);
			response.append(buildError("User ID lenght can't be less than 5 characters"));
			return false;
		}
		if ( std::string(passwd->valuestring).size() < 5)
		{
			ERR_T("[%5d] Request [%s] :> Password lenght can't be less than 5 characters", SessionID, __FUNCTION__);
			response.append(buildError("Passwordlenght can't be less than 5 characters"));
			return false;
		}

		std::string error;
		if(user_add(user_id->valuestring, passwd->valuestring, error))
		{			
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{			
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());		
			response.append(buildError(error));
			return false;
		}
	}

	bool USER_LOCK(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		if (user_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s", SessionID, __FUNCTION__, user_id->valuestring);

		std::string error;
		if(user_lock(user_id->valuestring, error))
		{			
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}
	}

	bool USER_UNLOCK(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		if (user_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s", SessionID, __FUNCTION__, user_id->valuestring);

		std::string error;
		if(user_unlock(user_id->valuestring, error))
		{			
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}
	}

	bool SYS_TERMINATE_THREAD(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* thread_id = cJSON_GetObjectItem(Inroot, "THREAD_ID");
		if (thread_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}

		RootAcctions.addOverThread(thread_id->valueint, TERMINATE_THREAD);
		response.append(buildOk());
		return true;
	}

	bool SYS_BANNED_IP(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* banned_ip = cJSON_GetObjectItem(Inroot, "IP");
		if (banned_ip == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}

		RootAcctions.addOverIP(banned_ip->valuestring, BANNED_IP);
		response.append(buildOk());
		INFO_T("[%5d][%s] IP: %s Banned by Root Request", SessionID, __FUNCTION__, banned_ip->valuestring);
		return true;
	}

	bool SYS_UNBANNED_IP(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* banned_ip = cJSON_GetObjectItem(Inroot, "IP");
		if (banned_ip == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}

		if (RootAcctions.removeOverIP(banned_ip->valuestring, UNBANNED_IP))
			ERR_T("[%5d][%s] RootAcctions.IP : %s not found.", SessionID, __FUNCTION__, banned_ip->valuestring);
		if (IpChecker.remove(banned_ip->valuestring))
			ERR_T("[%5d][%s] IpChecker.IP : %s not found.", SessionID, __FUNCTION__, banned_ip->valuestring);
		response.append(buildOk());
		INFO_T("[%5d][%s] IP: %s UnBanned by Root Request", SessionID, __FUNCTION__, banned_ip->valuestring);
		return true;
	}

	bool QUESTION_OPEN(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* cJSON_question = cJSON_GetObjectItem(Inroot, "QUESTION");
		cJSON* cJSON_exp_date = cJSON_GetObjectItem(Inroot, "EXP_DATE");
		cJSON* cJSON_delay_days = cJSON_GetObjectItem(Inroot, "DELAY_BETWEEN_PHASES_IN_DAYS");
		cJSON* cJSON_proposals = cJSON_GetObjectItem(Inroot, "PROPOSALS");
		cJSON* cJSON_users = cJSON_GetObjectItem(Inroot, "USERS_IDS");

		if (cJSON_user_id == NULL || cJSON_question == NULL || cJSON_exp_date == NULL || cJSON_delay_days == NULL
			|| cJSON_proposals == NULL || cJSON_users == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
		{
			INFO_T("[%5d] Request [%s] :> USER_ID : %s QUESTION : %s EXP_DATE : %s", SessionID, __FUNCTION__, 
				cJSON_user_id->valuestring, cJSON_question->valuestring, cJSON_exp_date->valuestring);
		}


		std::vector<std::string> proposals;      
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_proposals); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_proposals, i);
			proposals.push_back(subitem->valuestring);
		}


		std::vector<std::string> usersquestion;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_users); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_users, i);
			usersquestion.push_back(subitem->valuestring);
		}
		std::string error;
		int question_id = question_open(cJSON_user_id->valuestring, cJSON_question->valuestring, 
			cJSON_exp_date->valuestring, cJSON_delay_days->valueint, proposals, usersquestion, error);
		if (-1 != question_id)
		{
			INFO_T("[%5d] Request [%s] :> New question create with id : %d", SessionID, __FUNCTION__, question_id);
			cJSON* auxJson = cJSON_CreateObject();
			subitem = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", subitem);
			cJSON_AddItemToObject(subitem, "QUESTION_ID", cJSON_CreateNumber((double)question_id));
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}

	}

	
	bool DEBUG_ARE_THERE_QUESTIONS_IN_PHASE(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_sort_index = cJSON_GetObjectItem(Inroot, "SORT_INDEX");
		if (cJSON_sort_index == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> SORT_INDEX: %d", SessionID, __FUNCTION__, cJSON_sort_index->valueint);

		std::string error;

		bool result = debug_are_there_questions_in_phase(cJSON_sort_index->valueint, error);
		if (!error.empty())
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}

		response.append(buildOk(result));
		return true;
	}


	bool QUESTION_ACCEPT(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* cJSON_question = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_user_id == NULL || cJSON_question == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s QUESTION_ID: %d", SessionID, __FUNCTION__, cJSON_user_id->valuestring, cJSON_question->valueint);

		std::string error;
		if(question_accept(cJSON_user_id->valuestring, cJSON_question->valueint, error))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}

	}

	bool QUESTION_SWITCH_FASE(const int SessionID, cJSON* Inroot, std::string& response )
	{		
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :>  QUESTION_ID: %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);

		std::string error;
		if(question_switch_fase(cJSON_question_id->valueint, error))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}
	}

	bool QUESTION_DEPRECATED(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :>  QUESTION_ID: %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);

		std::string error;
		if(question_deprecated(cJSON_question_id->valueint, error))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}
	}

	bool QUESTION_IS_ACCEPTED(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :>  QUESTION_ID: %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);

		std::string error;
		if(question_is_accepted(cJSON_question_id->valueint, error))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}
	}

	bool QUESTION_GET_LAST_REV(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :>  QUESTION_ID: %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);

		std::string error;
		int last_rev = question_get_last_rev(cJSON_question_id->valueint, error);

		if (-1 != last_rev)
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateNumber((double)last_rev));
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());	
			response.append(buildError(error));
			return false;
		}

	}

	bool QUESTION_ADD_USERS(const int SessionID, cJSON* Inroot, std::string& response )
	{		
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		cJSON* cJSON_users_ids = cJSON_GetObjectItem(Inroot, "USERS_IDS");
		if (cJSON_question_id == NULL || cJSON_users_ids == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> QUESTION_ID : %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);

		std::vector<std::string> users_ids;      
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_users_ids); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_users_ids, i);
			users_ids.push_back(subitem->valuestring);
		}

		std::string error;
		bool ret = question_add_users(cJSON_question_id->valueint, users_ids, error);
		if (true == ret)
		{
			response.append(buildOk());
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}		
	}

	bool PROPOSALS_ADD(const int SessionID, cJSON* Inroot, std::string& response )
	{        
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		cJSON* cJSON_proposals = cJSON_GetObjectItem(Inroot, "PROPOSALS");
		if (cJSON_question_id == NULL || cJSON_user_id == NULL || cJSON_proposals == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s QUESTION_ID : %d", SessionID, __FUNCTION__, cJSON_user_id->valuestring, cJSON_question_id->valueint);

		std::vector<std::string> proposals;      
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_proposals); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_proposals, i);
			proposals.push_back(subitem->valuestring);
		}

		std::string error;
		int proposal_id = proposals_add(cJSON_user_id->valuestring, cJSON_question_id->valueint, proposals, error);
		if (-1 != proposal_id)
		{
			response.append(buildOk());
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool USERPROPOSALS_ADD(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		cJSON* cJSON_proposals_ids = cJSON_GetObjectItem(Inroot, "PROPOSALS_IDS");
		if (cJSON_question_id == NULL || cJSON_user_id == NULL || cJSON_proposals_ids == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> USER_ID : %s QUESTION_ID : %d", SessionID, __FUNCTION__, cJSON_user_id->valuestring, cJSON_question_id->valueint);

		std::vector<unsigned> proposals_ids;      
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_proposals_ids); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_proposals_ids, i);
			proposals_ids.push_back((unsigned)subitem->valueint);
		}

		std::string error;
		if(userproposals_add(cJSON_user_id->valuestring, cJSON_question_id->valueint, proposals_ids, error ))
		{
			cJSON* auxJson = cJSON_CreateObject();
			cJSON_AddItemToObject(auxJson, "RESULT", cJSON_CreateTrue());
			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_USERS_OF_QUESTION(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> %s : %d" , SessionID, __FUNCTION__, cJSON_question_id->string, cJSON_question_id->valueint);

		std::vector<std::string> result;       
		std::string error;
		sync_get_users_of_question(cJSON_question_id->valueint, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);
			for(std::vector<std::string>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();								
				cJSON_AddItemToObject(line, "USER_ID", cJSON_CreateString(it->c_str()));				
				cJSON_AddItemToArray(subitems, line);		
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		} 
		else if (result.empty() && error.empty())
		{
			response.append(buildError("Question has no one users_ids"));
			return true;
		}
		else 
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_SORT_PROPOSALS_OF_QUESTION(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> %s : %d" , SessionID, __FUNCTION__, cJSON_question_id->string, cJSON_question_id->valueint);

		std::vector<sync_get_sort_proposals_of_question_t> result;       
		std::string error;
		sync_get_sort_proposals_of_question(cJSON_question_id->valueint, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);
			for(std::vector<sync_get_sort_proposals_of_question_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(line, "PROPOSAL_ID", cJSON_CreateNumber(it->proposal_id));
				cJSON_AddItemToObject(line, "QUESTION_ID", cJSON_CreateNumber(it->question_id));
				cJSON_AddItemToObject(line, "USER_ID", cJSON_CreateString(it->user_id.c_str()));
				cJSON_AddItemToObject(line, "PROPOSAL", cJSON_CreateString(it->proposal.c_str()));
				cJSON_AddItemToArray(subitems, line);	
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		} 
		else if (result.empty() && error.empty())
		{
			response.append(buildOk());
			return true;
		}
		else 
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_QUESTIONS_BY_USER_PHASE(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		cJSON* cJSON_phase_id = cJSON_GetObjectItem(Inroot, "PHASE_ID");
		if (cJSON_user_id == NULL || cJSON_phase_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> %s : %s %s : %d" , SessionID, __FUNCTION__, cJSON_user_id->string, cJSON_user_id->valuestring,
			cJSON_phase_id->string, cJSON_phase_id->valueint);

		std::vector<sync_get_questions_by_user_phase_t> result;       
		std::string error;
		sync_get_questions_by_user_phase(cJSON_user_id->valuestring, cJSON_phase_id->valueint, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);
			for(std::vector<sync_get_questions_by_user_phase_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(line, "QUESTION_ID", cJSON_CreateNumber(it->question_id));
				cJSON_AddItemToObject(line, "QUESTION", cJSON_CreateString(it->question.c_str()));
				cJSON_AddItemToObject(line, "USER_ID", cJSON_CreateString(it->user_id.c_str()));
				cJSON_AddItemToObject(line, "EXP_DATE", cJSON_CreateString(it->exp_date.c_str()));				
				cJSON_AddItemToObject(line, "PHASE_ID", cJSON_CreateNumber(it->phase_id));
				cJSON_AddItemToObject(line, "IS_PHASE_ACCEPTED", 
					cJSON_CreateBool( (it->is_phase_accepted.substr(0, 1).compare("t") == 0) ? true : false )
					);
				cJSON_AddItemToArray(subitems, line);		
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		} 
		else if (result.empty() && error.empty())
		{
			response.append(buildOk());
			return true;
		}
		else 
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_QUESTIONS_REVS_OF_USER(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_user_id = cJSON_GetObjectItem(Inroot, "USER_ID");
		if (cJSON_user_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> %s : %s", SessionID, __FUNCTION__, cJSON_user_id->string, cJSON_user_id->valuestring);

		std::vector<sync_get_questions_revs_of_user_t> result;       
		std::string error;
		sync_get_questions_revs_of_user(cJSON_user_id->valuestring, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);
			for(std::vector<sync_get_questions_revs_of_user_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(line, "QUESTION_ID", cJSON_CreateNumber(it->question_id));
				cJSON_AddItemToObject(line, "CREATION_DATE", cJSON_CreateString(it->creation_date.c_str()));
				cJSON_AddItemToObject(line, "PHASE_ID", cJSON_CreateNumber(it->phase_id));
				cJSON_AddItemToObject(line, "IS_PHASE_ACCEPTED", 
					cJSON_CreateBool( (it->is_phase_accepted.substr(0, 1).compare("t") == 0) ? true : false )
					);

				cJSON_AddItemToArray(subitems, line);	
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		} 
		else if (result.empty() && error.empty())
		{
			response.append(buildOk());
			return true;
		}
		else 
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_PHASE_SORT(const int SessionID, cJSON* Inroot, std::string& response )
	{
		INFO_T("[%5d] Request [%s]", SessionID, __FUNCTION__);
		std::vector<sync_get_phase_sort_t> result;
		std::string error;
		sync_get_phase_sort(result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);

			for(std::vector<sync_get_phase_sort_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(line, "PHASE_ID", cJSON_CreateNumber(it->phase_id));
				cJSON_AddItemToObject(line, "SORT_INDEX", cJSON_CreateNumber(it->sort_index));
				cJSON_AddItemToObject(line, "PHASE", cJSON_CreateString(it->phase.c_str() ));
				cJSON_AddItemToObject(line, "DESCRIPTION", cJSON_CreateString(it->description.c_str() ));
				cJSON_AddItemToObject(line, "COLOR", cJSON_CreateString(it->color.c_str() ));
				cJSON_AddItemToArray(subitems, line);
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}

	}

	bool SYNC_GET_QUESTIONS_SORT(const int SessionID, cJSON* Inroot, std::string& response )
	{
		INFO_T("[%5d] Request [%s]", SessionID, __FUNCTION__);

		cJSON* cJSON_questions_ids = cJSON_GetObjectItem(Inroot, "QUESTIONS_IDS");
		std::vector<unsigned> questions_ids;
		cJSON* subitem = NULL;
		for (int i = 0; i < cJSON_GetArraySize(cJSON_questions_ids); i++)
		{
			subitem = cJSON_GetArrayItem(cJSON_questions_ids, i);
			questions_ids.push_back((unsigned)subitem->valueint);
		}
		std::vector<sync_get_questions_sort_t> result;

		std::string error;
		sync_get_questions_sort(questions_ids, result, error);
		if (!result.empty())
		{
			cJSON* line = NULL;
			cJSON* auxJson = cJSON_CreateObject();
			cJSON* subitems = cJSON_CreateArray();
			cJSON_AddItemToObject(auxJson, "RESULT", subitems);

			for(std::vector<sync_get_questions_sort_t>::iterator it = result.begin(); it != result.end(); it++)
			{
				line = cJSON_CreateObject();
				cJSON_AddItemToObject(subitems, "QUESTION_ID", cJSON_CreateNumber(it->question_id));
				cJSON_AddItemToObject(subitems, "QUESTION", cJSON_CreateString(it->question_text.c_str() ));
				cJSON_AddItemToObject(subitems, "USER_ID", cJSON_CreateString(it->user_id.c_str() ));
				cJSON_AddItemToObject(subitems, "CREATION_DATE", cJSON_CreateString(it->creation_date.c_str() ));
				cJSON_AddItemToObject(subitems, "DEPRECATION_DATE", cJSON_CreateString(it->deprecated_date.c_str() ));
				cJSON_AddItemToObject(subitems, "REVISION", cJSON_CreateNumber(it->revision));
				cJSON_AddItemToObject(subitems, "FASE", cJSON_CreateNumber(it->fase));
				cJSON_AddItemToArray(subitems, line);
			}

			char* json = cJSON_PrintUnformatted(auxJson);
			response.append(json);
			if (json) free(json);
			cJSON_Delete(auxJson);
			return true;
		}
		else
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SYNC_GET_TOP_PROPOSALS_OF_QUESTION(const int SessionID, cJSON* Inroot, std::string& response )
	{
		cJSON* cJSON_question_id = cJSON_GetObjectItem(Inroot, "QUESTION_ID");
		if (cJSON_question_id == NULL)
		{
			ERR_T("[%5d] Request [%s] :> NULL parameters", SessionID, __FUNCTION__);
			response.append(buildError("NULL parameters"));
			return false;
		}
		else
			INFO_T("[%5d] Request [%s] :> QUESTION_ID : %d", SessionID, __FUNCTION__, cJSON_question_id->valueint);
		std::vector<calc_better_proposal_t> result;

		std::string error;
		sync_get_top_proposals_of_question(cJSON_question_id->valueint, result, error);
		if (!result.empty())
		{
			calc_better_proposal calc(result);
			int proposal_id = calc.get();
			if (-1 != proposal_id)
			{				
				cJSON* auxJson = cJSON_CreateObject();
				cJSON* subitems = cJSON_CreateObject();
				cJSON_AddItemToObject(subitems, "PROPOSAL_ID", cJSON_CreateNumber(proposal_id));
				cJSON_AddItemToObject(auxJson, "RESULT", subitems);
				char* json = cJSON_PrintUnformatted(auxJson);
				response.append(json);
				if (json) free(json);
				cJSON_Delete(auxJson);
				return true;
			}
			else
			{
				response.append(buildError("Process error in the calc of betther proposal"));
				return false;
			}
		} 
		else 
		{
			rgx::RemplaceAll("([\\n|\\r])", error.c_str(), " ", error);
			for (unsigned i = 0; i < error.size(); i += 50)
				ERR_T("[%5d] %s[%d] %s", SessionID, __FUNCTION__, i, error.substr(i, 50).c_str());
			response.append(buildError(error));
			return false;
		}
	}

	bool SESSION_CLOSE(const int SessionID, cJSON*, std::string& response )
	{
		INFO_T("[%5d] Request [USER_CLOSE] Closing session..", SessionID);
		response.append(buildOk());
		return false;
	}

	bool SESSION_PING(const int SessionID, cJSON*, std::string& response )
	{
		INFO_T("[%5d] Request [USER_PING] Ping request", SessionID);
		response.append(buildOk());
		return true;
	}

	bool init_map_of_pointer_funcs(void)
	{
		funcs_pool["USER_EXIST"] = &USER_EXIST;
		funcs_pool["USER_ARRAY_EXIST"] = &USER_ARRAY_EXIST;
		funcs_pool["USER_PASSWORD_EXIST"] = &USER_PASSWORD_EXIST;
		funcs_pool["PASSWORD_CHANGE"] = &PASSWORD_CHANGE;
		funcs_pool["USER_ADD"] = &USER_ADD;
		funcs_pool["USER_LOCK"] = &USER_LOCK;
		funcs_pool["USER_UNLOCK"] = &USER_UNLOCK;

		funcs_pool["SYS_TERMINATE_THREAD"] = &SYS_TERMINATE_THREAD;
		funcs_pool["SYS_BANNED_IP"] = &SYS_BANNED_IP;
		funcs_pool["SYS_UNBANNED_IP"] = &SYS_UNBANNED_IP;

		funcs_pool["QUESTION_OPEN"] = &QUESTION_OPEN;		
		funcs_pool["QUESTION_ACCEPT"] = &QUESTION_ACCEPT;
		funcs_pool["QUESTION_SWITCH_FASE"] = &QUESTION_SWITCH_FASE;
		funcs_pool["QUESTION_DEPRECATED"] = &QUESTION_DEPRECATED;
		funcs_pool["QUESTION_ADD_USERS"] = &QUESTION_ADD_USERS;

		funcs_pool["QUESTION_GET_LAST_REV"] = &QUESTION_GET_LAST_REV;
		funcs_pool["QUESTION_IS_ACCEPTED"] = &QUESTION_IS_ACCEPTED;
		funcs_pool["USERPROPOSALS_ADD"] = &USERPROPOSALS_ADD;
		funcs_pool["PROPOSALS_ADD"] = &PROPOSALS_ADD;

		funcs_pool["SYNC_GET_QUESTIONS_REVS_OF_USER"] = &SYNC_GET_QUESTIONS_REVS_OF_USER;
		funcs_pool["SYNC_GET_QUESTIONS_BY_USER_PHASE"] = &SYNC_GET_QUESTIONS_BY_USER_PHASE;
		funcs_pool["SYNC_GET_TOP_PROPOSALS_OF_QUESTION"] = &SYNC_GET_TOP_PROPOSALS_OF_QUESTION;
		funcs_pool["SYNC_GET_QUESTIONS_SORT"] = &SYNC_GET_QUESTIONS_SORT;
		funcs_pool["SYNC_GET_PHASE_SORT"] = &SYNC_GET_PHASE_SORT;
		funcs_pool["SYNC_GET_SORT_PROPOSALS_OF_QUESTION"] = &SYNC_GET_SORT_PROPOSALS_OF_QUESTION;
		funcs_pool["SYNC_GET_USERS_OF_QUESTION"] = &SYNC_GET_USERS_OF_QUESTION;

		funcs_pool["DEBUG_ARE_THERE_QUESTIONS_IN_PHASE"] = &DEBUG_ARE_THERE_QUESTIONS_IN_PHASE;

		funcs_pool["SESSION_CLOSE"] = &SESSION_CLOSE;
		funcs_pool["SESSION_PING"] = &SESSION_PING;
		funcs_pool["USER_IS_AUTH"] = NULL;

		return true;
	}

	template<typename T>
	inline bool check_function_pointer(T func, const char* func_name, const int SessionID)
	{
		if (NULL == func)
		{
			ERR_T("[%5d][%s] The function pointer '%s' is NULL, check it please", SessionID, __FUNCTION__, func_name);
			return false;
		} return true;
	}

	bool user_is_auth(const int SessionID, const bool auth, cJSON* Inroot, std::string& response )
	{
		INFO_T("[%5d] Request [USER_IS_AUTH]", SessionID);
		cJSON* subitems = cJSON_CreateObject();
		if (auth)
			cJSON_AddItemToObject(subitems, "RESULT", cJSON_CreateTrue());
		else
			cJSON_AddItemToObject(subitems, "RESULT", cJSON_CreateFalse());
		char* json = cJSON_PrintUnformatted(subitems);
		response.append(json);
		if (json) free(json);
		cJSON_Delete(subitems);
		return true;
	}

	bool process_request_and_response(const int SessionID, const std::string& ip, const char* const request, std::string& response, bool& is_auth)
	{
		cJSON* root = NULL;
		try
		{

			if (RootAcctions.checkOverThread(SessionID, TERMINATE_THREAD) == true)
			{
				response.append(buildError("[TERMINATE] Root User has finished your connection."));
				INFO_T("[%5d] SESSION TERMINATION, REQUEST BY ROOT =========", SessionID);
				return false;
			}

			if (RootAcctions.checkOverIP(ip, BANNED_IP) == true)
			{
				response.append(buildError("[BANNED] Root User has banned your IP."));
				INFO_T("[%5d] IP: %s BANNED, REQUEST BY ROOT =========", SessionID, ip.c_str());
				return false;
			}

			char func_name[50];
			memset(func_name, 0, 50);
			root = cJSON_Parse(request);
			if (NULL == root)
			{
				ERR_T("[%5d][%s] Json Parser error before: [%s]\n", SessionID, __FUNCTION__, cJSON_GetErrorPtr());
				return false;
			}

			cJSON* cmd = cJSON_GetObjectItem(root,"CMD");
			if (NULL == cmd)
			{
				ERR_T("[%5d][%s] 'CMD' command not found, protocol error.", SessionID, __FUNCTION__);
				if (NULL != root) 
				{
					cJSON_Delete(root);
					root = NULL;
				}
				return false;
			}

			strncpy(func_name, cmd->valuestring, strlen(cmd->valuestring));

			cJSON* acction = cJSON_GetObjectItem(root,"ACCTION");
			if (NULL == acction)
			{
				ERR_T("[%5d][%s]'ACCTION' command not found, protocol error.", SessionID, __FUNCTION__);
				if (NULL != root) 
				{
					cJSON_Delete(root);
					root = NULL;
				}
				return false;
			}

			strncat(func_name, "_", 1);
			strncat(func_name, acction->valuestring, strlen(acction->valuestring));
			std::map<std::string, ptr_func_t>::iterator it = funcs_pool.find(func_name);
			if (it == funcs_pool.end())
			{
				ERR_T("[%5d] Funcion '%s' not found. Please insert it in 'funcs_pool'" 
					"std::map in function 'init_map_of_pointer_funcs'", SessionID, func_name);
				if (NULL != root) 
				{
					cJSON_Delete(root);
					root = NULL;
				}
				return false;
			}		

			if (is_auth)
			{
				if (it->first.compare("SESSION_CLOSE") == 0)
				{					
					if ( check_function_pointer( it->second, it->first.c_str(), SessionID ) )
					{
						bool ret = it->second(SessionID, root, response);
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return ret;
					}
					else					
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}										
				}
				else if (it->first.compare("USER_IS_AUTH") == 0)
				{					
					if ( user_is_auth(SessionID, is_auth, root, response) )
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return true;
					}
					else
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}
				}

				else if (strncmp("SYS", cmd->valuestring, strlen(cmd->valuestring)) == 0)
				{
					if (ip.compare("127.0.0.1") != 0)
					{
						ERR_T("[%5d][%s] To talk with me as Root, localhost connection is mandatory.", SessionID, __FUNCTION__);
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}
					it->second(SessionID, root, response);
					if (NULL != root) 
					{
						cJSON_Delete(root);
						root = NULL;
					}
					return true;
				}
				else
				{
					if (check_function_pointer(it->second, it->first.c_str(), SessionID))
					{
						it->second(SessionID, root, response);
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return true;
					}
					else
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}
				}
			}
			else
			{
				if (it->first.compare("USER_PASSWORD_EXIST") == 0)
				{
					if (check_function_pointer(it->second, it->first.c_str(), SessionID))		
					{
						is_auth = it->second(SessionID, root, response);		
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						IpChecker.IpBannedControl(is_auth, ip);						
						return (IpChecker.is_IpBanned(ip)) ? false : true;
					}
					else 
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;					
					}
				}	
				else if (it->first.compare("USER_EXIST") == 0 || it->first.compare("SESSION_PING") == 0
					|| it->first.compare("USER_ADD") == 0)
				{
					if (check_function_pointer(it->second, it->first.c_str(), SessionID))
					{
						it->second(SessionID, root, response);
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return true;
					}
					else
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}			
				}
				else if (it->first.compare("SESSION_CLOSE") == 0)
				{
					if (check_function_pointer(it->second, it->first.c_str(), SessionID))
					{
						bool ret = it->second(SessionID, root, response);
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return ret;
					}
					else
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}					
				}
				else if (it->first.compare("USER_IS_AUTH") == 0)
				{
					if ( user_is_auth(SessionID, is_auth, root, response) )
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return true;
					}
					else
					{
						if (NULL != root) 
						{
							cJSON_Delete(root);
							root = NULL;
						}
						return false;
					}
				}
				else
				{
					ERR_T("[%5d][%s] To talk with me, you need are logged, first.", SessionID, __FUNCTION__);
					if (NULL != root) 
					{
						cJSON_Delete(root);
						root = NULL;
					}
					return is_auth;
				}
			}		
		} 
		catch (const std::exception& e) 
		{
			ERR_T("[%5d][%s] Unexpect exception : %s", SessionID, __FUNCTION__, e.what());
			if (NULL != root) 
			{
				cJSON_Delete(root);
				root = NULL;
			}
			return false;
		}
		catch(...)
		{
			ERR_T("[%5d][%s] unknown exception!", SessionID, __FUNCTION__);
			if (NULL != root) 
			{
				cJSON_Delete(root);
				root = NULL;
			}
			return false;
		}
	}

	bool writing_process(const int SessionID, SSL* ssl, const std::string& response_in)
	{
		try
		{
			if (response_in.empty()) return false;
			std::ostringstream ss;
			size_t utf_size = utf8::distance(response_in.begin(), response_in.end());
			if (0 == utf_size) return false;
			ss << utf_size << response_in;
			std::string response(ss.str());
			ss.clear();

			const unsigned MAX_BUFFER_LEN = 2048;
			char buf[MAX_BUFFER_LEN];
			int bytes = 0, buffer_len = 0;
			bool is_writing = true;
			memset(buf, 0, MAX_BUFFER_LEN);
			size_t index = 0;

			buffer_len = (response.size() > MAX_BUFFER_LEN) ? MAX_BUFFER_LEN : response.size();
			strncpy(buf, response.substr(index, buffer_len).c_str(), buffer_len);

			while (is_writing)
			{
				bytes = SSL_write(ssl, buf, buffer_len);
				memset(buf, 0, MAX_BUFFER_LEN);
				if (SSL_ERROR_WANT_WRITE == SSL_get_error(ssl, bytes))
				{
					strncpy(buf, response.substr(index+bytes, MAX_BUFFER_LEN-(bytes+index)).c_str(), MAX_BUFFER_LEN-(bytes+index));
					index += bytes;
					is_writing = true;
				}
				else if ( bytes > 0 )
				{
					if ( (index + bytes + MAX_BUFFER_LEN) < response.size())
					{
						index += bytes;		
						buffer_len = MAX_BUFFER_LEN;
						strncpy(buf, response.substr(index, buffer_len).c_str(), buffer_len);
						is_writing = true;
					}
					else if ((index + bytes) < response.size())
					{
						index += bytes;
						buffer_len = response.size() - index;
						memset(buf, 0, MAX_BUFFER_LEN);
						strncpy(buf, response.substr(index, buffer_len).c_str(), buffer_len);
						is_writing = true;
					}
					else
						is_writing = false;
				}
				else
				{
					ERR_T("[%5d][%s] : %s", SessionID, __FUNCTION__, ERR_error_string(SSL_get_error(ssl, 0), NULL));
					return false;
				}
			}

			return true;
		}
		catch (const std::exception& e)
		{
			ERR_T("[%s] Unexpect exception :%s", __FUNCTION__, e.what());
			return false;
		}
		catch(...)
		{
			ERR_T("[%s] unknown exception!", __FUNCTION__);
			return false;
		}
	}    


	bool reading_process(const int SessionID, SSL* ssl, std::string& request)
	{
		try
		{
			const unsigned MAX_BUFFER_LEN = 1024;
			const unsigned MAX_NUM_BUFFER_LEN = 32;
			char buf[MAX_BUFFER_LEN];
			char buf_aux[MAX_BUFFER_LEN];
			int bytes = 0, bytes_read = 0;
			int buffer_size = 0;
			int shift = 0;
			char cbuffer_size[MAX_NUM_BUFFER_LEN];
			bool is_reading = true;

			memset(buf, 0, MAX_BUFFER_LEN);
			memset(buf_aux, 0, MAX_BUFFER_LEN);
			memset(cbuffer_size, 0, MAX_NUM_BUFFER_LEN);

			while (is_reading)
			{
				bytes = SSL_read(ssl, buf, sizeof(buf)); /* get request */
				if (0 == buffer_size && bytes > 0)
				{
					memset(buf_aux, 0, MAX_BUFFER_LEN);
					char* ptr = NULL;
					ptr = strchr(buf, '{');
					if (NULL == ptr)
					{
						INFO_T("[%5d][%s] Buffer Lenght doesnt appear at the beginig of Json String.", SessionID, __FUNCTION__);
						return false;
					}
					shift = ptr-&buf[0];
					bytes -= shift;
					strncpy(&cbuffer_size[0], &buf[0], shift);
					buffer_size = atoi(cbuffer_size);
					strncpy(buf_aux, ptr, strlen(ptr));
					memset(buf, 0, MAX_BUFFER_LEN);
					strncpy(buf, buf_aux, strlen(buf_aux));
				}
				bytes_read += bytes;
				if (bytes > 0 && buffer_size > bytes_read)
				{
					request.append(buf);
					is_reading = true;
				}
				else if ( bytes > 0 )
				{
					request.append(buf);
					is_reading = false;
				}	
#ifdef _MSC_VER	
				else if (GetLastError() == WSAETIMEDOUT)
				{
					INFO_T("[%5d][%s] TimeOut", SessionID, __FUNCTION__);
					return false;
				}
#else
				else if (-1 == bytes && errno == EAGAIN)
				{
					INFO_T("[%5d][%s] TimeOut", SessionID, __FUNCTION__);
					return false;
				}
#endif
				else if (0 == bytes)
				{
					INFO_T("[%5d][%s] Connexion close/broken from other side.", SessionID, __FUNCTION__);
					return false;
				}
				else
				{
#ifdef _MSC_VER	
					PrintWinSysError(TEXT(__FUNCTION__));			
#endif
					ERR_T("[%5d][%s] : %s", SessionID, __FUNCTION__, ERR_error_string(SSL_get_error(ssl, 0), NULL));
					return false;
				}					
				memset(buf, 0, MAX_BUFFER_LEN);
			}
			return true;
		}
		catch (const std::exception& e) 
		{
			ERR_T("[%s] Unexpect exception : %s", __FUNCTION__, e.what());
			return false;
		}
		catch(...)
		{
			ERR_T("[%s] unknown exception!", __FUNCTION__);
			return false;
		}
	}


	void SSLCheckErrors(int)
	{
	}


	void* ServetThread(void* thread_args_) /* Serve the connection -- threadable */
	{
		thread_args_t* thread_args = reinterpret_cast<thread_args_t*>(thread_args_);
		Servlet(*thread_args);
		return NULL;
	}


	void getIPfromSockFD(const int sockFD, std::string& IP)
	{
		char buf[INET_ADDRSTRLEN] = "";
		struct sockaddr_in name;
		socklen_t len = sizeof(name);

		if (getpeername(sockFD, (struct sockaddr *)&name, &len) != 0) 
		{
			perror("getpeername");
		} else 
		{
			inet_ntop(AF_INET, &name.sin_addr, buf, sizeof buf);
			IP.append(buf);
		}
	}

	void Servlet(thread_args_t& ThreadArgs) 
	{   
		try
		{
			SSL* ssl = SSL_new(ThreadArgs.ctx);           /* get new SSL state with context */
			if (NULL == ssl)
			{
				ERR_T("[%s] SSL pointer to context is NULL, aborting conexion...", __FUNCTION__);
				return;
			}
			SSL_set_fd(ssl, ThreadArgs.client_sock_fd);      /* set connection socket to SSL state */
			int SessionID = 0, err_no = 0;
			SessionID = SSL_get_fd(ssl);       /* get socket connection */
			std::string IP;
			getIPfromSockFD(SessionID, IP);
			INFO_T("[%5d] SESSION OPEN =========", SessionID);

			IpChecker.set(ThreadArgs.max_password_fails);            
			if (IpChecker.is_IpBanned(IP) == true)
			{
				ERR_T("%s has been banned by %d failed authentications", IP.c_str(), ThreadArgs.max_password_fails);
				SSL_free(ssl);
				CloseSocket(SessionID);
				return;
			}


			if ( -1 == ( err_no = SSL_accept(ssl)) )     /* do SSL-protocol accept */
			{			
#ifdef _MSC_VER	
				PrintWinSysError(TEXT(__FUNCTION__));			
#endif
				ERR_T("[%5d][%s] :  %s", SessionID, __FUNCTION__, ERR_error_string(err_no, NULL));
				while( ( err_no = ERR_get_error() ) != 0 )
					ERR(ERR_error_string(err_no, NULL));
				SSL_free(ssl);         /* release SSL state */
				CloseSocket(SessionID);
				INFO_T("[%5d] SESSION CLOSE =========", SessionID);
				return;
			}


			bool session_on = true, is_auth = false;
			std::string request, response;
			while(session_on)
			{
				if ( (session_on = reading_process(SessionID, ssl, request)) )
				{
					session_on = process_request_and_response(SessionID, IP, request.c_str(), response, is_auth);

					if (!session_on)
						writing_process(SessionID, ssl, response);
					else
						session_on = writing_process(SessionID, ssl, response);

					request.clear();
					response.clear();
				}

			}

			if (NULL != ssl)
			{
				SSL_free(ssl);         /* release SSL state */
				ssl = NULL;
				INFO_T("[%5d] SSL resource freed", SessionID);
			}

			CloseSocket(SessionID);
			INFO_T("[%5d] SESSION CLOSE =========", SessionID);


		} 
		catch (const std::exception& e) 
		{
			ERR_T("[%s] Unexpect exception : %s", __FUNCTION__, e.what());
		}
		catch(...)
		{
			ERR_T("[%s] unknown exception!", __FUNCTION__);
		}

	}

	void SetTimeOut(const int sockFD, const long TimeOutInSecs)
	{
		struct timeval timeout;
#ifdef _MSC_VER	
		// En windows es en milliseconds por lo que multiplicamos por 1000 los segundos (PUTO WINDOWS !!)
		timeout.tv_sec = TimeOutInSecs * 1000;
#else
		timeout.tv_sec = TimeOutInSecs;
#endif
		timeout.tv_usec = 0;

		if (setsockopt (sockFD, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
			ERR_T("[%s][%d]setsockopt failed\n", __FUNCTION__, sockFD);

		if (setsockopt (sockFD, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
			ERR_T("[%s][%d]setsockopt failed\n", __FUNCTION__, sockFD);
	}

	bool InitSocketStream(int& sockFD)
	{
#ifdef _MSC_VER	
		WSADATA wsaData = {0};
		if( int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			ERR_T("WSAStartup failed: %d", iResult);
			return false;
		}
#endif
		sockFD = socket(PF_INET, SOCK_STREAM, 0);
#ifdef _MSC_VER
		if (sockFD == INVALID_SOCKET) 
		{
			ERR_T("socket function failed with error = %d", WSAGetLastError() );
			return false;
		} else
		{            
			return true;
		}
#else
		if (-1 == sockFD)
		{
			ERR_T("socket function failed with error = %d", sockFD);
			return false;
		} else
		{            
			return true;
		}
#endif
	}

	bool SetSocketToListen(const int port, const int poolSize, int& sockFD)
	{	
		struct sockaddr_in addr;
		if (InitSocketStream(sockFD) == false) return false;
		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = INADDR_ANY;

		if ( bind(sockFD, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
		{
			ERR("can't bind port");
			return false;
		}
		if ( listen(sockFD, poolSize) != 0 )
		{
			ERR("Can't configure listening port");
			return false;
		}
		return true;
	}

	bool LoadCertificates(SSL_CTX* ctx, const char* CertFile, const char* KeyFile)
	{
		int err_no = 0;
		/* set the local certificate from CertFile */	
		if ( (err_no = SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM)) <= 0  )
		{		
			ERR(ERR_error_string(err_no, NULL));
			return false;
		}

		/* set the private key from KeyFile (may be the same as CertFile) */
		if ( ( err_no = SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) ) <= 0 )
		{
			ERR(ERR_error_string(err_no, NULL));
			return false;
		}

		/* verify private key */
		if ( !SSL_CTX_check_private_key(ctx) )
		{
			ERR("Private key does not match the public certificate");
			return false;
		}

		return true;
	}


	SSL_CTX* InitServerCTX(encrypt_method_t encrypt_method)
	{   
		SSL_CTX* ctx = NULL;

		OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
		SSL_load_error_strings();   /* load all error messages */
		ERR_load_crypto_strings();
		SSL_METHOD* method = NULL;
		if (encrypt_method == SSLv2)
			method = const_cast<SSL_METHOD*>(SSLv23_server_method());  /* create new server-method instance */		
		else if (encrypt_method == SSLv3)
			method = const_cast<SSL_METHOD*>(SSLv3_server_method());  /* create new server-method instance */
		else if (encrypt_method == TLSv1)
			method = const_cast<SSL_METHOD*>(TLSv1_server_method());  /* create new server-method instance */
		/*
		else if (encrypt_method == TLSv1_1 || encrypt_method == TLSv1_2)
		ERR("TLSv1_1 and TLSv1_2 no supported in MAC OS X, please use TLSv1.")
		else if (encrypt_method == TLSv1_1)
		method = const_cast<SSL_METHOD*>(TLSv1_1_server_method());
		else if (encrypt_method == TLSv1_2)
		method = const_cast<SSL_METHOD*>(TLSv1_2_server_method());
		*/
		else
		{
			ERR_T("Encrypted method %s not supported. Use ['TLSv1', 'TLSv1_1', 'TLSv1_2', 'SSLv2', 'SSLv3']", encryptTX[encrypt_method]);
			return NULL;
		}
		ctx = SSL_CTX_new(method);   /* create new context from method */
		if ( NULL == ctx )
		{
			ERR(ERR_error_string(ERR_pop_to_mark(), NULL));
		}
		return ctx;
	}

	void ShowCerts(SSL* ssl)
	{   
		X509 *cert;
		char line[2048];

		cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
		if ( cert != NULL )
		{
			INFO("============================================= BEGIN - Server certificates =============================================");
			X509_NAME_oneline(X509_get_subject_name(cert), line, 2048);
			INFO_T("SUBJECT: %s", line);		
			X509_NAME_oneline(X509_get_issuer_name(cert), line, 2048);
			INFO_T("ISSUER: %s", line);		
			INFO("============================================= END - Server certificates   =============================================");
			X509_free(cert);
		}
		else
			INFO("Client didn't send any certificate.");
	}


	bool PrintlnFieldOfSubject(const char* const pathToServCertFile, const char* const fieldName)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);

		if (NULL != cert)
		{
			X509_NAME* nameSubject = X509_get_subject_name(cert);
			if (NULL != nameSubject)
			{
				char buffer[256];
				int nid = OBJ_txt2nid(fieldName);
				X509_NAME_get_text_by_NID(nameSubject, nid, buffer, 256);
				INFO_T("%s", buffer);		
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}

	bool PrintlnFieldOfIssuer(const char* const pathToServCertFile, const char* const fieldName)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameIssuer = X509_get_issuer_name(cert);
			if (NULL != nameIssuer)
			{
				char buffer[256];
				int nid = OBJ_txt2nid(fieldName);
				X509_NAME_get_text_by_NID(nameIssuer, nid, buffer, 256);
				INFO_T("%s", buffer);
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}

		return false;
	}

	bool GetFieldOfSubject(const char* const pathToServCertFile, const char* const fieldName, char* buffer, int sizeofBuffer)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameSubject = X509_get_subject_name(cert);
			if (NULL != nameSubject)
			{
				int nid = OBJ_txt2nid(fieldName);
				X509_NAME_get_text_by_NID(nameSubject, nid, buffer, sizeofBuffer);
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}

	bool GetFieldOfIssuer(const char* const pathToServCertFile, const char* const fieldName, char* buffer, int sizeofBuffer)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameIssuer = X509_get_issuer_name(cert);
			if (NULL != nameIssuer)
			{
				int nid = OBJ_txt2nid(fieldName);
				X509_NAME_get_text_by_NID(nameIssuer, nid, buffer, sizeofBuffer);
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}


	bool GetSubjectInfo(const char* const pathToServCertFile, char* buffer, int sizeofBuffer)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameSubject = X509_get_subject_name(cert);
			if (NULL != nameSubject)
			{
				X509_NAME_oneline(nameSubject, buffer, sizeofBuffer); 
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}

	bool GetIssuerInfo(const char* const pathToServCertFile, char* buffer, int sizeofBuffer)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameIssuer = X509_get_issuer_name(cert);
			if (NULL != nameIssuer)
			{
				X509_NAME_oneline(nameIssuer, buffer, sizeofBuffer); 
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}

	bool PrintlnSubjectInfo(const char* const pathToServCertFile)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameSubject = X509_get_subject_name(cert);
			if (NULL != nameSubject)
			{
				INFO_T("Subject: %s", X509_NAME_oneline(nameSubject, 0, 0) ); 
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		} else return false;
	}

	bool PrintlnIssuerInfo(const char* const pathToServCertFile)
	{
		X509* cert = NULL;
		cert = GetCertFromFile(pathToServCertFile);
		if (NULL != cert)
		{
			X509_NAME* nameSubject = X509_get_issuer_name(cert);
			if (NULL != nameSubject)
			{
				INFO_T("Issuer: %s", X509_NAME_oneline(nameSubject, 0, 0) ); 
				X509_free(cert);
			}
			else
			{
				X509_free(cert);
				return false;
			}
			return true;
		}
		return false;
	}

	X509* GetCertFromFile(const char* const pathToServCertFile)
	{
		FILE* fpem;
		X509* cert = NULL;		

		if( !( fpem = fopen( pathToServCertFile, "r" ))) 
		{
			ERR_T( "Couldn't open the PEM file: %s", pathToServCertFile );
			return NULL;
		}

		if (!(cert = PEM_read_X509 (fpem, NULL, NULL, NULL)))
		{
			fclose( fpem );
			ERR_T( "Failed to read the PEM file: %s", pathToServCertFile );
			return NULL;
		}
		fclose (fpem);

		return cert;
	}

	/*
	* format YYmmddHHMMSS or YYYYmmddHHMMSS (ASN1_TIME->data)
	* ASN1_TIME* tt = cert->cert_info->validity->notAfter;
	*/
	time_t ASN1_GetTimeT(ASN1_TIME* time)
	{
		struct tm t;
		const char* str = (const char*) time->data;
		size_t i = 0;

		memset(&t, 0, sizeof(t));

		if (time->type == V_ASN1_UTCTIME) /* two digit year */
		{
			t.tm_year = (str[i++] - '0') * 10 + (str[++i] - '0');
			if (t.tm_year < 70)
				t.tm_year += 100;
		}
		else if (time->type == V_ASN1_GENERALIZEDTIME) /* four digit year */
		{
			t.tm_year = (str[i++] - '0') * 1000 + (str[++i] - '0') * 100 + (str[++i] - '0') * 10 + (str[++i] - '0');
			t.tm_year -= 1900;
		}
		t.tm_mon = ((str[i++] - '0') * 10 + (str[++i] - '0')) - 1; // -1 since January is 0 not 1.
		t.tm_mday = (str[i++] - '0') * 10 + (str[++i] - '0');
		t.tm_hour = (str[i++] - '0') * 10 + (str[++i] - '0');
		t.tm_min  = (str[i++] - '0') * 10 + (str[++i] - '0');
		t.tm_sec  = (str[i++] - '0') * 10 + (str[++i] - '0');

		/* Note: we did not adjust the time based on time zone information */
		return mktime(&t);
	}
}

