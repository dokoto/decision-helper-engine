package net.whatswith.ssl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.whatswith.json.ResponseBuilder;
import net.whatswith.structs.Return;

public class SSLConnSever
{
	private SSLContext sslContext;
	private SSLSocketFactory sf;
	private SSLSocket sock;
	private OutputStream outChan;
	private InputStream inChan;
	private long TimeOutInSec;
	private final String ServerEncoding = "UTF-8";

	public SSLConnSever()
	{
		TimeOutInSec = 10;	
	}
	
	public Return IsConnected()
	{
		try
		{
			StringBuilder response = new StringBuilder();
			if (send("{\"CMD\":\"SESSION\",\"ACCTION\":\"PING\"}", response) == false)
			{
				close();
				return new Return(false, "No Connection", "Ping");
			} else
			{
				StringBuilder error = new StringBuilder();
				if (ResponseBuilder.extractResulSimple(response.toString(), error) == false)
				{
					if (error.indexOf("[BANNED]") > 0)
						return new Return(false, error.toString(), "Ping", Return.types.BANNED_IP);
					else if (error.indexOf("[TERMINATE]") > 0)
						return new Return(false, error.toString(), "Ping", Return.types.TERMINATE_THREAD);
					else
						return new Return(false, error.toString(), "Ping");
				}
				else
					return new Return(true, "Ok", "Ping");
			}
		} catch (Exception e)
		{
			close();
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return new Return(false, e.toString(), "Ping");
		}
	}

	public boolean IsAuth()
	{
		try
		{
			StringBuilder response = new StringBuilder();
			send("{\"CMD\":\"USER\",\"ACCTION\":\"IS_AUTH\"}", response);
			return ResponseBuilder.is_auth(response.toString(), new StringBuilder());
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}
	
	public boolean CloseSession(StringBuilder response)
	{
		try
		{			
			if (send("{\"CMD\":\"SESSION\",\"ACCTION\":\"CLOSE\"}", response) == false)
			{
				close();
				return false;
			} else
				return true;
		} catch (Exception e)
		{
			close();
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}				
	}

	public boolean send(String Request, StringBuilder response)
	{
		BufferedWriter w = null; 
		BufferedReader r = null;
		try
		{
			StringBuilder request = new StringBuilder(Request.length() + Request);
			w = new BufferedWriter(new OutputStreamWriter(outChan, ServerEncoding));
			w.write(request.toString(), 0, request.length());
			w.flush();
			r = new BufferedReader(new InputStreamReader(inChan, ServerEncoding));
			final int BUFF_LEN = 1024;
			char[] buff = new char[BUFF_LEN];
			int offset = 0, read = 0;
			boolean is_reading = true;
			int bufferSize = 0, index = 0;
			
			while(is_reading)
			{
				read = r.read(buff, offset, offset + BUFF_LEN);
				if (-1 == read) break;
				String aux = String.copyValueOf(buff).trim();
				if (read > 0 && 0 == bufferSize)
				{					
					bufferSize = Integer.parseInt( aux.substring( 0,  aux.indexOf("{")) );
					read -= aux.substring( 0,  aux.indexOf("{")).length();
					aux =  aux.substring( aux.indexOf("{"), aux.length() );					
				}
				
				if (read > 0 && index + read < bufferSize)
				{
					index += read;
					response.append(aux);
					read = 0;
					Arrays.fill(buff, ' ');
					is_reading = true;
				}
				else
				{
					response.append(aux);
					is_reading = false;
				}
			}	
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
		finally
		{
			try
			{
				if (w != null)
					w.close();
				if (r != null)
					r.close();
			}catch (Exception e)
			{
				System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
				return false;
			}
		}
	}

	public boolean close()
	{
		try
		{
			inChan.close();
			outChan.close();
			sock.close();
			return true;
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public Return open(final String host, final int port)
	{

		java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");

		X509TrustManager trustManager = new X509TrustManager()
		{
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
				// Don't do anything.
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
				// Don't do anything.
			}

			public X509Certificate[] getAcceptedIssuers()
			{
				return new java.security.cert.X509Certificate[0];
			}
		};

		try
		{		
			sslContext = SSLContext.getInstance("TLSv1");
			sslContext.init(null, new TrustManager[] { trustManager }, new SecureRandom());
			sf = sslContext.getSocketFactory();
			sock = (SSLSocket) sf.createSocket(host, port);
			sock.setSoTimeout((int) TimeUnit.SECONDS.toMillis(TimeOutInSec));
			sock.setEnabledProtocols(new String[] { "TLSv1" });
			sock.startHandshake();
			outChan = sock.getOutputStream();
			outChan.flush();
			inChan = sock.getInputStream();
			return new Return(true, "Ok", "Open SSL Connections");
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return new Return(false, e.toString(), "Open SSL Connections");
		}
	}	
}
