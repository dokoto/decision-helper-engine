#include "IPChecker.hpp"

namespace dhe
{

	ipchecker::ipchecker(void)
	{
		max_password_fails = 5;			
		pthread_mutex_init(&mutex, NULL);
	}
	
	ipchecker::ipchecker(const int MaxPasswordFails)
	{
		assert(MaxPasswordFails > 0);
		max_password_fails = MaxPasswordFails;
		pthread_mutex_init(&mutex, NULL);
	}

	ipchecker::~ipchecker(void) 
	{
		pthread_mutex_destroy(&mutex);
	}

	void ipchecker::set(const int MaxPasswordFails)
	{
		assert(MaxPasswordFails > 0);
		max_password_fails = MaxPasswordFails;			
	}

	bool ipchecker::is_IpBanned(const std::string& ip)
	{
		std::map<std::string, int>::iterator it = Banned_ips.find(ip);
		if ( it == Banned_ips.end())
			return false;
		else		
			return (it->second >= max_password_fails);	
	}

	bool ipchecker::remove(const std::string& ip)
	{
		std::map<std::string, int>::iterator it = Banned_ips.find(ip);
		if ( it != Banned_ips.end())
		{
			Banned_ips.erase(it);
			return true;
		} else return false;
	}

	void ipchecker::IpBannedControl(const bool is_auth, const std::string& ip)
	{
		pthread_mutex_lock(&mutex);
		if (false == is_auth)
		{
			if (Banned_ips.find(ip) == Banned_ips.end())
				Banned_ips[ip] = 1;
			else
				Banned_ips[ip]++;
		}
		else
		{
			std::map<std::string, int>::iterator it = Banned_ips.find(ip);
			if ( it != Banned_ips.end())
				Banned_ips.erase(it);
		}
		pthread_mutex_unlock(&mutex);
	}
}

