package requests;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ddbb.Query;
import servlet.Cmds;

public class ProcRequest
{
	public enum type_auth
	{
		CONTINUE_WITH_CMD, NO_AUTH, AUTH
	};

	public static Query proccess(HttpServletRequest request, HttpServletResponse response, Cmds cmds) throws Exception
	{
		ObjectMapper mapper = null;
		JsonNode rootNode = null;
		Query query = null;
		mapper = new ObjectMapper();
		rootNode = mapper.readTree(request.getInputStream());
		String cmd = rootNode.path("CMD").asText() + "_" + rootNode.path("ACCTION").asText();
		query = cmds.find(cmd);
		query.SetCmdAction(rootNode.path("CMD").asText(), rootNode.path("ACCTION").asText());
		query.SetJsonResquest(rootNode);
		query.GenerateRequest();

		return query;
	}
}
