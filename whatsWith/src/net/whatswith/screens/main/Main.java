package net.whatswith.screens.main;

import net.whatswith.R;
import net.whatswith.structs.Nexus;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.content.Intent;
//import android.content.Intent;
//import android.content.IntentFilter;
import android.content.IntentFilter;

public class Main extends Activity
{
	public static Nexus Nexus = null;
	private boolean mainHasRun = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_main);
		setTitle("");
		Init();
	}

	private void Init()
	{
		Nexus = new Nexus(this);
		Nexus.InitApp();
		/*
		 * Sustituir con Google Cloud Messages
		Nexus.setWakeService();				 
		registerReceiver(Nexus.receiver, new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
		unregisterReceiver(Nexus.receiver);
		*/
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (mainHasRun == true)
		{
			if (Nexus.Close)
			{				
				/*
				 * 	En el siguiente parche
				 
				    Nexus.setWakeService();				 
					registerReceiver(Nexus.receiver, new IntentFilter(Intent.ACTION_BOOT_COMPLETED));
					unregisterReceiver(Nexus.receiver);
					*/
				
				this.finish();
			}
			else
				Nexus.InitApp();
		} else
			mainHasRun = true;
	}



	@Override
	protected void onDestroy()
	{
		Nexus.DownApp();
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (true == Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_debug_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		
		Main.Nexus.MenuSelector(this, item);
		return true;
	}	

}
