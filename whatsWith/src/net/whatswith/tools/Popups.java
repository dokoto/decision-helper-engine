package net.whatswith.tools;

import net.whatswith.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

public class Popups
{

	public static void ErrorNoButtons(String msg, String Title, Context context)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);
		alertDialog.setMessage(msg);
		alertDialog.setIcon(R.drawable.ic_dialog_alert);
		alertDialog.show();
	}
	
	public static void InfoNoButtons(String msg, String Title, Context context)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);
		alertDialog.setMessage(msg);
		alertDialog.setIcon(R.drawable.ic_menu_myplaces);
		alertDialog.show();
	}	
	
	public static void InfoOneButton(String msg, String Title, String PosButtonTx, Context context, DialogInterface.OnClickListener func)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton(PosButtonTx, func);				
		alertDialog.setIcon(R.drawable.ic_menu_myplaces);
		alertDialog.show();
	}

		
	public static void ErrorOneButton(String msg, String Title, String PosButtonTx, Context context, DialogInterface.OnClickListener func)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton(PosButtonTx, func);				
		alertDialog.setIcon(R.drawable.ic_dialog_alert);
		alertDialog.show();
	}
	
	public static void InfoTwoButtons(String msg, String Title, String PosButtonTx, String NegButtonTx, Context context, 
			DialogInterface.OnClickListener funcPos, DialogInterface.OnClickListener funcNeg)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton(PosButtonTx, funcPos);		
		alertDialog.setNegativeButton(NegButtonTx, funcNeg);		
		alertDialog.setIcon(R.drawable.ic_dialog_alert);
		alertDialog.show();
	}
	
	public static void InfoTwoButtonsOneInput(String msg, String Title, String PosButtonTx, String NegButtonTx, Context context, 
			EditText input, DialogInterface.OnClickListener funcPos, DialogInterface.OnClickListener funcNeg)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(Title);		
		alertDialog.setMessage(msg);
		alertDialog.setView(input);
		alertDialog.setPositiveButton(PosButtonTx, funcPos);		
		alertDialog.setNegativeButton(NegButtonTx, funcNeg);		
		alertDialog.setIcon(R.drawable.ic_dialog_alert);
		alertDialog.show();
	}

}
