package servlet;

import java.util.HashMap;

import ddbb.Query;

public class Cmds
{
	private HashMap<String, Query> queries;
	public Cmds() throws Exception
	{
		queries.put("USER_EXIST", new Query("SELECT user_exist('$1')", "RESULT:%b", "User doesn't exist"));
		queries.put("USER_ARRAY_EXIST", new Query("SELECT * from user_array_exist(ARRAY[$1]", "RESULT:USER_ID%s, SIGN_DATE%s", "No user found"));
		queries.put("USER_PASSWORD_EXIST", new Query("SELECT user_password_exist('$1', '$2')", "RESULT:%b", "Password doesn't exist"));
		queries.put("PASSWORD_CHANGE", new Query("SELECT password_change('$1', '$2', '$3')", "RESULT:%b", "Password change error"));
		queries.put("USER_ADD", new Query("SELECT user_add('$1', '$2')", "RESULT:%b", "Adding user has failed"));
		//queries.put("USER_LOCK", new Query("SELECT user_lock('$1')", "RESULT:%b", "Bloking user has failed"));
		//queries.put("USER_UNLOCK", new Query("SELECT user_unlock('$1')", "RESULT:%b", "unBloking user has failed"));
		queries.put("QUESTION_OPEN", new Query("SELECT question_open('$1', '$2', '$3', '$4', ARRAY[$5], ARRAY[$6])", "RESULT:QUESTION_ID%d", "Question open has failed"));
		queries.put("QUESTION_ACCEPT", new Query("SELECT question_accept('$1', '$2')", "RESULT:%b", "Question Accept has failed"));
		//queries.put("QUESTION_SWITCH_FASE", new Query("SELECT question_switch_fase('$1')", "RESULT:%b", "Question switch phase has failed"));
		//queries.put("QUESTION_DEPRECATED", new Query("SELECT question_deprecated('$1')", "RESULT:%b", "Question deprecate has failed"));
		queries.put("QUESTION_ADD_USERS", new Query("SELECT usersquestion_add('$1', ARRAY[$2])", "RESULT:%b", "Add users has failed"));
		//queries.put("QUESTION_GET_LAST_REV", new Query("SELECT question_get_last_rev('$1')", "REVISION%d", "Get last revision has failed"));
		//queries.put("QUESTION_IS_ACCEPTED", new Query("SELECT question_is_accepted('%1')", "RESULT:%b", "Is accept has failed"));
		queries.put("USERPROPOSALS_ADD", new Query("SELECT userproposals_add('$1', '$2', ARRAY[$3])", "RESULT:%b", "User proposals add has failed"));
		queries.put("PROPOSALS_ADD", new Query("SELECT proposals_add('$1', '$2', ARRAY[$3])", "RESULT:%b", "Add proposals has failed"));
		queries.put("SYNC_GET_QUESTIONS_REVS_OF_USER", new Query("SELECT * FROM sync_get_questions_revs_of_user('$1')", 
				"RESULT:ARRAY[QUESTION_ID%d, CREATION_DATE%s, PHASE_ID%d, IS_PHASE_ACCEPTED%b]", "Get revisions has failed"));
		queries.put("SYNC_GET_QUESTIONS_BY_USER_PHASE", new Query("SELECT * FROM sync_get_questions_by_user_phase('$1', '$2')", 
				"RESULT:ARRAY[QUESTION_ID%d, QUESTION%s, USER_ID%s, EXP_DATE%s, PHASE_ID%s, IS_PHASE_ACCEPTED%b]", "Get question by phase has failed"));		
		queries.put("SYNC_GET_TOP_PROPOSALS_OF_QUESTION", new Query("SELECT * FROM sync_get_top_proposals_of_question('$1')", "PROPOSAL_ID%d", "Get Top Proposals has failed"));
		//queries.put("SYNC_GET_QUESTIONS_SORT", new Query("SELECT * FROM sync_get_new_update(ARRAY[$1])", 
		//		"RESULT:ARRAY[QUESTION_ID%d, QUESTION%s, USER_ID%s, CREATION_DATE%s, DEPRECATION_DATE%s, REVISION%d, FASE%d]", "Get sorted question by phase has failed"));
		queries.put("SYNC_GET_PHASE_SORT", new Query("SELECT * FROM sync_get_phase_sort()", 
				"RESULT:ARRAY[PHASE_ID%d, SORT_INDEX%d, PHASE%s, DESCRIPTION%s, COLOR%s]", "Get sorted phases has failed"));
		queries.put("SYNC_GET_SORT_PROPOSALS_OF_QUESTION", new Query("SELECT * FROM sync_get_sort_proposals_of_question('$1')", 
				"RESULT:ARRAY[PROPOSAL_ID%d, QUESTION_ID%d, USER_ID%s, PROPOSAL%s]", "Get sorted proposals has failed"));
		queries.put("SYNC_GET_USERS_OF_QUESTION", new Query("SELECT * FROM sync_get_users_of_question('$1')", 
				"RESULT:ARRAY[USER_ID%s]", "Get user of question has failed"));
	}
	
	public Query find(String cmd)
	{
		return queries.get(cmd);
	}
}
