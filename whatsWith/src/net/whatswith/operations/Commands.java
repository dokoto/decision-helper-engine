package net.whatswith.operations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Map.Entry;

import net.whatswith.json.RequestBuilder;
import net.whatswith.json.ResponseBuilder;
import net.whatswith.screens.main.Main;
import net.whatswith.ssl.SSLConnSever;
import net.whatswith.structs.Contacts_db;
import net.whatswith.structs.IActivityLuncher;
import net.whatswith.structs.Login;
import net.whatswith.structs.Phases_db;
import net.whatswith.structs.Proposals_db;
import net.whatswith.structs.Question_db;
import net.whatswith.structs.Questions_by_Phase;
import net.whatswith.structs.Questions_by_UserPhase;
import net.whatswith.structs.Questions_rev_com;
import net.whatswith.structs.Return;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.widget.Toast;

public class Commands
{
	public static void Async_Question_Accept(final Login login, final Activity act, final IActivityLuncher func, final boolean finish_at_end,
			final int question_id)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return ret;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = QuestionAccept(Main.Nexus.Storage.login(), question_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
				{
					if (finish_at_end)
						act.finish();
				}
			}
		};
		task.execute((Void[]) null);
	}

	public static void Async_Open_Question(final Login login, final Activity act, final IActivityLuncher func, final String question, final String exp_date,
			final int delay_days, final String[] proposals, final String[] users_ids, final boolean finish_at_end)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return ret = new Return();
			private Integer question_id;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = QuestionOpen(login, question, exp_date, delay_days, proposals, users_ids, question_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
				} else
				{
					Toast.makeText(act.getApplicationContext(), "New question created success", Toast.LENGTH_SHORT).show();
					if (func != null)
						func.Launch();
				}
				if (finish_at_end)
					act.finish();
			}

		};
		task.execute((Void[]) null);
	}

	public static Return QuestionOpen(final Login login, final String question, final String exp_date, final int delay_days, final String[] proposals,
			final String[] users_ids, Integer question_id)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return QuestionOpen_(login.user, question, exp_date, delay_days, proposals, users_ids, question_id);
	}

	public static void Async_Commit_Update_Contacts(final Login login, final Activity act, final IActivityLuncher func)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret = new Return();

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(act);
				pd.setTitle("Upgrading Contacts...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				if (android.os.Build.VERSION.SDK_INT > 10)
					Main.Nexus.Storage.contacts = getContactsFromDevice(act);
				else
					Main.Nexus.Storage.contacts = Commands.getContactsFromDevice_BEF_API10(act);
				
				if (!Main.Nexus.Storage.contacts.isEmpty())
					ret = ContactsUpdate(Main.Nexus.Storage.login(), ContactsToArray(Main.Nexus.Storage.contacts));

				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				if (Main.Nexus.Storage.contacts.isEmpty())
					Toast.makeText(act.getApplicationContext(), "It seems as if contacts data base was empty", Toast.LENGTH_SHORT).show();
				else
				{
					if (ret.status == false)
					{
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
						Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
					} else
					{
						// Toast.makeText(act.getApplicationContext(),
						// "Upgrade Contacts success",
						// Toast.LENGTH_SHORT).show();
						Main.Nexus.Serial.Save(Main.Nexus.Storage);
						if (func != null)
							func.LaunchWithKey();
					}
				}
			}

		};
		task.execute((Void[]) null);
	}

	public static String GetContactNameFromPhone(String phone, Activity act)
	{
		if (phone.compareTo(Main.Nexus.login().user) == 0)
			return "I";
		
		if (null == Main.Nexus.Storage.contactsForUser)
		{
			if (android.os.Build.VERSION.SDK_INT > 10)
				Main.Nexus.Storage.contactsForUser = getContactsALLFromDevice(act);
			else
				Main.Nexus.Storage.contactsForUser = getContactsALLFromDevice_BEF_API10(act);
		}

		Contacts_db name = Main.Nexus.Storage.contactsForUser.get(phone);
		if (null == name)
		{
			if (android.os.Build.VERSION.SDK_INT > 10)
				Main.Nexus.Storage.contacts = getContactsFromDevice(act);
			else
				Main.Nexus.Storage.contacts = Commands.getContactsFromDevice_BEF_API10(act);
			name = Main.Nexus.Storage.contactsForUser.get(phone);
			if (null == name)
				return phone;
			else
				return name.name;
		} else
			return name.name;

	}

	public static void Async_Commit_Update_Contacts(final Login login, final int Question_id, final Activity act, final IActivityLuncher func)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret = new Return();
			private ArrayList<String> usersOfQuestion;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(act);
				pd.setTitle("Upgrading Contacts...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();

				usersOfQuestion = new ArrayList<String>();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				if (android.os.Build.VERSION.SDK_INT > 10)
					Main.Nexus.Storage.contacts = getContactsFromDevice(act);
				else
					Main.Nexus.Storage.contacts = Commands.getContactsFromDevice_BEF_API10(act);
				if (!Main.Nexus.Storage.contacts.isEmpty())
				{
					if ((ret = GetUsersOfQuestion(login, Question_id, usersOfQuestion)).status == true)
					{
						if ((ret = ContactsUpdate(Main.Nexus.Storage.login(), ContactsToArray(Main.Nexus.Storage.contacts))).status == true)
							FilterContacts(Main.Nexus.Storage.contacts, usersOfQuestion);
					}
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				if (Main.Nexus.Storage.contacts.isEmpty())
					Toast.makeText(act.getApplicationContext(), "It seems as if contacts data base was empty", Toast.LENGTH_SHORT).show();
				else
				{
					if (ret.status == false)
					{
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
						Toast.makeText(act.getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
					} else
					{
						// Toast.makeText(act.getApplicationContext(),
						// "Upgrade Contacts success",
						// Toast.LENGTH_SHORT).show();
						Main.Nexus.Serial.Save(Main.Nexus.Storage);
						if (func != null)
							func.LaunchWithKey();
					}
				}
			}

		};
		task.execute((Void[]) null);
	}

	public static void FilterContacts(HashMap<String, Contacts_db> contacts, ArrayList<String> usersOfQuestion)
	{
		assert (null != contacts);
		assert (null != usersOfQuestion);
		for (String uu : usersOfQuestion)
			contacts.remove(uu);
	}

	public static void Async_Commit_Refresh(final Login login, final IActivityLuncher func)
	{
		try
		{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
			{
				private Return ret_q, ret_p;

				@Override
				protected Void doInBackground(Void... arg0)
				{
					ret_q = QuestionsSync(login);
					ret_p = PhasesUpdate(login);
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					if (ret_q.status == false)
					{
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret_q.message);
					}
					if (ret_p.status == false)
					{
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret_p.message);
					}
					if (ret_q.status && ret_p.status)
					{
						Main.Nexus.Serial.Save(Main.Nexus.Storage);
						if (func != null)
							func.Launch();
					}
				}
			};
			task.execute((Void[]) null);

		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
		}
	}

	public static void Async_Commit_PhaseUpdate(final Login login, final IActivityLuncher func)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return lret = new Return();

			@Override
			protected Void doInBackground(Void... arg0)
			{
				lret = Commands.PhasesUpdate(login);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (lret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + lret.message);
				} else
				{
					Main.Nexus.Serial.Save(Main.Nexus.Storage);
					if (func != null)
						func.Launch();
				}
			}

		};
		task.execute((Void[]) null);
	}

	public static void Async_Commit_UnPlug(final Login login, final IActivityLuncher func)
	{
		try
		{
			Thread t = new Thread(new Runnable()
			{
				public void run()
				{
					Return ret = isConnected();
					if (ret.status == false)
						ret = new Return(true, "Success", "Close Session");
					else
					{
						ret = UnPlug_(login);
						CloseSeverConn();
					}

					if (ret.status == false)
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					else
					{
						Main.Nexus.Serial.Save(Main.Nexus.Storage);
						if (func != null)
							func.Launch();
					}

				}
			});
			t.start();
			t.join();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Deprecated
	public static void Async_Commit_UnPlug_OLD(final Login login, final IActivityLuncher func)
	{
		try
		{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
			{
				private Return ret;

				@Override
				protected Void doInBackground(Void... arg0)
				{
					ret = isConnected();
					if (ret.status == false)
						ret = new Return(true, "Success", "Close Session");
					else
					{
						ret = UnPlug_(login);
						CloseSeverConn();
					}
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					if (ret.status == false)
						System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					else
					{
						Main.Nexus.Serial.Save(Main.Nexus.Storage);
						if (func != null)
							func.Launch();
					}
				}
			};
			task.execute((Void[]) null);

		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
		}
	}

	public static Return UserExist(final String hostname, final int port, final String user_id)
	{
		try
		{
			Return ret = OpenServerConn(hostname, port);
			if (ret.status == false)
				return ret;
			else
			{
				String query_user_exist = RequestBuilder.user_exist(Main.Nexus.Storage.phoneNumber());
				StringBuilder response = new StringBuilder();
				SendServer(query_user_exist, response);
				StringBuilder error = new StringBuilder();
				if (ResponseBuilder.user_exist(response.toString(), error) == false)
					return new Return(false, error.toString(), "User Exist");
				else
					return new Return(true, "User : " + user_id + " exist", "User Exist");
			}
		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		}
	}

		
	
	private static boolean debug_are_there_questions_in_phase_(final int sort_index, Return ret)
	{
		try
		{
			String debug_are_there_questions_in_phase = RequestBuilder.debug_are_there_questions_in_phase(sort_index);
			StringBuilder response = new StringBuilder();
			SendServer(debug_are_there_questions_in_phase, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.debug_are_there_questions_in_phase(response.toString(), error) == false)
			{
				ret.Set(false, error.toString(), "Are There question in phase");
				return false;
			}
			else
			{
				ret.Set(true, error.toString(), "Are There question in phase");
				return true;
			}
		} catch (Exception e)
		{
			ret.Set(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Accept");
			return false;
		}
	}
	
	public static Return FirstSession(final String hostname, final int port, final String user_id, final String password)
	{
		try
		{
			Return ret = OpenServerConn(hostname, port);
			if (ret.status == false)
				return ret;
			else
			{
				String query_user_exist = RequestBuilder.user_exist(Main.Nexus.Storage.phoneNumber());
				StringBuilder response = new StringBuilder();
				SendServer(query_user_exist, response);
				StringBuilder error = new StringBuilder();
				if (ResponseBuilder.user_exist(response.toString(), error) == false)
				{
					Return Ret = CreateUser(user_id, password);
					if (!Ret.status)
						return Ret;
					Ret = AuthUser(user_id, password);
					if (Ret.status)
						return new Return(true, "A new user : " + user_id + " has been successful Created and Authenticated", "Add and Auth new User", password);
					return Ret;
				} else
					return AuthUser(user_id, password);
			}
		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		}
	}

	/*
	 * Metodo que conecta y autoriza contra el servidor si es necesario)
	 */
	public static Return ConnectAndAuth(final Login login)
	{
		return ConnectAndAuth(login.host, login.port, login.user, login.password);
	}

	public static Return PasswordChange(final Login login, final String password_new)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return PasswordChange_(login.user, login.password, password_new);
	}

	
	public static Return QuestionsUpdate(final Login login)
	{
		try
		{
			return new Return(true, "Success", "Update All Questions");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Questions");
		}
	}

	public static Return ProposalsUpdate(final Login login)
	{
		try
		{
			return new Return(true, "Success", "Update All Proposals");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Proposals");
		}
	}

	// SIN PROBAR !!
	public static boolean debug_are_there_questions_in_phase(final Login login, final int sort_index, Return ret)
	{
		if (ConnectAndAuth(login.host, login.port, login.user, login.password).status == false)
		{
			return debug_are_there_questions_in_phase_(sort_index, ret);
		}
		return false;
	}
	
	public static SparseArray<Phases_db> PhasesUpdate_ISOLATE(final Login login, Return ret)
	{
		if (ConnectAndAuth(login.host, login.port, login.user, login.password).status == false)
		{
			ret.Set(false, "Connection error", "Update All Phases");
			return new SparseArray<Phases_db>();
		}
		return PhasesUpdate_ISOLATE_(ret);
	}

	public static Return PhasesUpdate(final Login login)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return PhasesUpdate_();
	}

	public static Return ContactsUpdate(final Login login, String[] users_ids)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return ContactsUpdate_(users_ids);
	}

	public static Return AddProposals(final Login login, final int question_id, final String[] proposals)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return AddProposals_(login.user, question_id, proposals);
	}

	private static Return AddProposals_(final String user_id, final int question_id, String[] proposals)
	{
		try
		{
			String proposals_add = RequestBuilder.proposals_add(user_id, question_id, proposals);
			StringBuilder response = new StringBuilder();
			SendServer(proposals_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.proposals_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Add Proposals");
			else
				return new Return(true, "Success", "Add Proposals");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Contacts");
		}
	}

	public static Return AddProposalsPreferenceOrder(final Login login, final int question_id, final int[] proposals_ids)
	{

		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return AddProposalsPreferenceOrder_(login.user, question_id, proposals_ids);
	}

	public static Return AddProposalsPreferenceOrder_(final String user_id, final int question_id, final int[] proposals_ids)
	{
		try
		{
			String userproposals_add = RequestBuilder.userproposals_add(user_id, question_id, proposals_ids);
			StringBuilder response = new StringBuilder();
			SendServer(userproposals_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.userproposals_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Save Preference Proposals Order");
			else
				return new Return(true, "Success", "Save Preference Proposals Ordern");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Save Preference Proposals Order");
		}

	}

	public static Return Question_add_users(final Login login, final int question_id, final String[] users_ids)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return Question_add_users_(question_id, users_ids);
	}

	private static Return Question_add_users_(final int question_id, String[] users_ids)
	{
		try
		{
			String question_add_users = RequestBuilder.question_add_users(question_id, users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(question_add_users, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_add_users(response.toString(), error) == false)
				return new Return(false, error.toString(), "Add new User to Question");
			else
				return new Return(true, "Success", "Add new User to Question");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Add new User to Question");
		}
	}

	public static Return QuestionAccept(final Login login, final int question_id)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return QuestionAccept_(login.user, question_id);
	}

	private static Return QuestionAccept_(final String user_id, final int question_id)
	{
		try
		{
			String question_accept = RequestBuilder.question_accept(user_id, question_id);
			StringBuilder response = new StringBuilder();
			SendServer(question_accept, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_accept(response.toString(), error) == false)
				return new Return(false, error.toString(), "Question Accept");
			else
				return new Return(true, "Success", "Question Accept");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Accept");
		}
	}

	public static ArrayList<Questions_by_UserPhase> GetQuestionsByPhaseForList(final int Phase_id)
	{
		ArrayList<Questions_by_UserPhase> questionsByPU = new ArrayList<Questions_by_UserPhase>();
		for (int i = 0; i < Main.Nexus.Storage.questions.size(); ++i)
		{
			int key = Main.Nexus.Storage.questions.keyAt(i);
			if (-1 != key)
			{
				Question_db q = Main.Nexus.Storage.questions.get(key);
				if (q.phase_id == Phase_id)
				{
					questionsByPU.add(new Questions_by_UserPhase(key, q));
				}
			}
		}
		return questionsByPU;
	}

	public static ArrayList<Questions_by_Phase> GetQuestionsByPhaseForBoxes()
	{
		ArrayList<Questions_by_Phase> questionsByP = new ArrayList<Questions_by_Phase>();
		SparseIntArray maps = new SparseIntArray();

		for (int i = 0; i < Main.Nexus.Storage.phases.size(); ++i)
		{
			int key = Main.Nexus.Storage.phases.keyAt(i);
			questionsByP.add(new Questions_by_Phase(key, Main.Nexus.Storage.phases.get(key).phase));
			maps.put(key, i);
		}
		for (int i = 0; i < Main.Nexus.Storage.questions.size(); i++)
		{
			Question_db q = Main.Nexus.Storage.questions.valueAt(i);
			if (null == q)
				continue;
			int pos = maps.get(q.phase_id);
			if (-1 == pos)
				continue;
			questionsByP.get(pos).tot_ones++;
			if (q.is_phase_accepted == false)
				questionsByP.get(pos).new_ones++;
		}
		return questionsByP;
	}

	public static ArrayList<Questions_by_Phase> GetQuestionsByPhaseForBoxes_ISOLATE(SparseArray<Question_db> questions, SparseArray<Phases_db> phases)
	{
		ArrayList<Questions_by_Phase> questionsByP = new ArrayList<Questions_by_Phase>();
		SparseIntArray maps = new SparseIntArray();

		for (int i = 0; i < phases.size(); ++i)
		{
			int key = phases.keyAt(i);
			questionsByP.add(new Questions_by_Phase(key, phases.get(key).phase));
			maps.put(key, i);
		}
		for (int i = 0; i < questions.size(); i++)
		{
			Question_db q = questions.valueAt(i);
			if (null == q)
				continue;
			int pos = maps.get(q.phase_id);
			if (-1 == pos)
				continue;
			questionsByP.get(pos).tot_ones++;
			if (q.is_phase_accepted == false)
				questionsByP.get(pos).new_ones++;
		}
		return questionsByP;
	}

	public static Return QuestionsSync(final Login login)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return QuestionsSync_(login.user);
	}

	public static SparseArray<Question_db> QuestionsSync_ISOLATE(final Login login, Return ret)
	{
		if (ConnectAndAuth(login.host, login.port, login.user, login.password).status == false)
		{
			ret.Set(false, "Connection error", "Question Sync Rev");
			return new SparseArray<Question_db>();
		}
		return QuestionsSync_ISOLATE_(login.user, ret);
	}

	public static Return Get_top_proposals_of_question(final Login login, final int question_id, int[] proposal_id)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return Get_top_proposals_of_question_(question_id, proposal_id);
	}

	public static Return ProposalsSyncByQuestion(final Login login, final int question_id)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return ProposalsSyncByQuestion_(question_id);
	}

	public static Return GetUsersOfQuestion(final Login login, final int question_id, ArrayList<String> users_ids)
	{
		if (null == users_ids)
			return new Return(false, "users_ids ref param can't be null", "users_ids param can't be null");
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return GetUsersOfQuestion_(question_id, users_ids);
	}

	public static Return QuestionsSyncByPhase(final Login login, final int phase_id)
	{
		Return ret = ConnectAndAuth(login.host, login.port, login.user, login.password);
		if (ret.status == false)
			return ret;
		return QuestionsSyncByPhase_(login.user, phase_id);
	}

	public static Return ConnectAndAuth(final String hostname, final int port, final String user_id, final String password)
	{
		try
		{
			Return ret = isConnected();
			if (ret.status == false && (ret.type == Return.types.BANNED_IP || ret.type == Return.types.TERMINATE_THREAD))
				return ret;
			if (ret.status == false)
			{
				ret = OpenServerConn(hostname, port);
				if (ret.status == false)
					return ret;
				else
					return AuthUser(user_id, password);
			} else
			{
				if (isAuth() == false)
					return AuthUser(user_id, password);
				else
					return new Return(true, "User Authentication success", "User Auth", password);
			}
		} catch (Exception e)
		{
			return new Return(false, e.toString(), Thread.currentThread().getStackTrace()[2].getMethodName());
		}
	}

	private static Return ContactsUpdate_(String[] users_ids)
	{
		try
		{
			String user_array_exist = RequestBuilder.user_array_exist(users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(user_array_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_array_exist(response.toString(), Main.Nexus.Storage.contacts, error) == false)
				return new Return(false, error.toString(), "Update All Contacts");
			else
				return new Return(true, "Success", "Update All Contacts");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Contacts");
		}
	}

	private static SparseArray<Phases_db> PhasesUpdate_ISOLATE_(Return ret)
	{
		try
		{
			String sync_get_phase_sort = RequestBuilder.sync_get_phase_sort();
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_phase_sort, response);
			StringBuilder error = new StringBuilder();
			SparseArray<Phases_db> phases = new SparseArray<Phases_db>();
			if (ResponseBuilder.sync_get_phase_sort(response.toString(), phases, error) == false)
			{
				ret.Set(false, error.toString(), "Update All Phases");
				return new SparseArray<Phases_db>();
			} else
			{
				ret.Set(true, "Success", "Update All Phases");
				return phases;
			}
		} catch (Exception e)
		{
			ret.Set(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Phases");
			return new SparseArray<Phases_db>();
		}
	}

	private static Return PhasesUpdate_()
	{
		try
		{
			String sync_get_phase_sort = RequestBuilder.sync_get_phase_sort();
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_phase_sort, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.sync_get_phase_sort(response.toString(), Main.Nexus.Storage.phases, error) == false)
				return new Return(false, error.toString(), "Update All Phases");
			else
				return new Return(true, "Success", "Update All Phases");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Update All Phases");
		}
	}

	@Deprecated
	private static String contacts_getPhoneNumber(int contactId, Activity act)
	{
		final String[] projection = new String[] { Phone.NUMBER, Phone.TYPE };
		final Cursor phone = act.getContentResolver().query(Phone.CONTENT_URI, projection, Contacts._ID + "=?", new String[] { String.valueOf(contactId) },
				null);
		try
		{
			if (phone.moveToFirst())
			{
				final int contactNumberColumnIndex = phone.getColumnIndex(Phone.NUMBER);
				final int contactTypeColumnIndex = phone.getColumnIndex(Phone.TYPE);

				while (!phone.isAfterLast())
				{
					final String number = phone.getString(contactNumberColumnIndex);
					final int type = phone.getInt(contactTypeColumnIndex);
					if (type == Phone.TYPE_MOBILE)
						return number;
					phone.moveToNext();
				}
				return null;
			} else
				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		} finally
		{
			phone.close();
		}
	}

	@Deprecated
	private static Bitmap contacts_getPhoto(String photoId, Activity act)
	{
		final Cursor photo = act.getContentResolver().query(Data.CONTENT_URI, new String[] { Photo.PHOTO }, Data._ID + "=?", new String[] { photoId }, null);
		try
		{
			if (photo.moveToFirst())
			{
				byte[] photoBlob = photo.getBlob(photo.getColumnIndex(Photo.PHOTO));
				return BitmapFactory.decodeByteArray(photoBlob, 0, photoBlob.length);
			} else
				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		} finally
		{
			photo.close();
		}
	}

	@Deprecated
	private static Contacts_db Contacts_getBasic(int contactId, Activity act)
	{
		final String[] projection = new String[] { Contacts.DISPLAY_NAME, Contacts.PHOTO_ID };
		final Cursor contact = act.getContentResolver().query(Contacts.CONTENT_URI, projection, Contacts._ID + "=?",
				new String[] { String.valueOf(contactId) }, null);
		try
		{
			if (contact.moveToFirst())
			{
				final String phoneNumber = contacts_getPhoneNumber(contactId, act);
				if (phoneNumber == null)
					return null;
				final String name = contact.getString(contact.getColumnIndex(Contacts.DISPLAY_NAME));
				final String photoId = contact.getString(contact.getColumnIndex(Contacts.PHOTO_ID));
				Bitmap photoBitmap = null;
				if (photoId != null)
					photoBitmap = contacts_getPhoto(photoId, act);
				return new Contacts_db(name, phoneNumber, photoBitmap, (photoBitmap == null) ? false : true);
			} else
				return null;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		} finally
		{
			contact.close();
		}
	}

	@Deprecated
	public static HashMap<String, Contacts_db> getContactsFromDeviceTEST(Activity act)
	{
		final String[] projection = new String[] { RawContacts.CONTACT_ID, RawContacts.DELETED };
		final Cursor rawContacts = act.getContentResolver().query(RawContacts.CONTENT_URI, projection, null, null, null);
		final int contactIdColumnIndex = rawContacts.getColumnIndex(RawContacts.CONTACT_ID);
		final int deletedColumnIndex = rawContacts.getColumnIndex(RawContacts.DELETED);
		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();
		try
		{
			if (rawContacts.moveToFirst())
			{
				while (!rawContacts.isAfterLast())
				{ // still a valid entry left?
					final int contactId = rawContacts.getInt(contactIdColumnIndex);
					final boolean deleted = (rawContacts.getInt(deletedColumnIndex) == 1);
					if (!deleted)
					{
						Contacts_db row = Contacts_getBasic(contactId, act);
						if (row != null)
							ContactsInfo.put(row.phone, row);
					}
					rawContacts.moveToNext(); // move to the next entry
				}
				return ContactsInfo;
			} else
				return ContactsInfo;
		} catch (Exception e)
		{
			e.printStackTrace();
			return ContactsInfo;
		} finally
		{
			rawContacts.close();
		}
	}

	public static HashMap<String, Contacts_db> getContactsALLFromDevice(Activity act)
	{
		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.PHOTO_ID };
		Cursor people = act.getContentResolver().query(uri, projection, null, null, null);
		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
		int indexPhoto = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID);
		int indexNumberType = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);

		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();

		String phone = new String();
		try
		{
			people.moveToFirst();
			do
			{
				if (people.getInt(indexNumberType) == Phone.TYPE_MOBILE)
				{
					Bitmap BitPhoto = queryContactImage(people.getInt(indexPhoto), act);
					phone = (Main.Nexus.Storage.crazy_mode == false) ? net.whatswith.tools.Phone.FormatNumber(people.getString(indexNumber),
							Main.Nexus.SimCountryIso) : net.whatswith.tools.Phone.FormatNumber(people.getString(indexNumber), "ES");

					String name = people.getString(indexName);
					ContactsInfo.put(phone, new Contacts_db(name, phone, BitPhoto, (BitPhoto == null) ? false : true));
				}
			} while (people.moveToNext());

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return ContactsInfo;
	}

	public static HashMap<String, Contacts_db> getContactsALLFromDevice_BEF_API10(Activity act)
	{
		ContentResolver cr = act.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();
		if (cur.getCount() > 0)
		{
			while (cur.moveToNext())
			{
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
				{
					if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
					{
						Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);

						while (pCur.moveToNext())
						{
							if (pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)) == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
							{
								String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
								phone = (Main.Nexus.Storage.crazy_mode == false) ? net.whatswith.tools.Phone.FormatNumber(phone, Main.Nexus.SimCountryIso)
										: net.whatswith.tools.Phone.FormatNumber(phone, "ES");
								

									Bitmap BitPhoto = queryContactImage(pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID)), act);									
									ContactsInfo.put(phone, new Contacts_db(name, phone, BitPhoto, (BitPhoto == null) ? false : true));			
							}
							

						}
						pCur.close();
					}

				}
			}
		}
		return ContactsInfo; 
	}
	
	public static HashMap<String, Contacts_db> getContactsFromDevice_BEF_API10(Activity act)
	{
		ContentResolver cr = act.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();
		if (cur.getCount() > 0)
		{
			while (cur.moveToNext())
			{
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
				{
					if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
					{
						Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);

						while (pCur.moveToNext())
						{
							if (pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)) == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
							{
								String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
								phone = (Main.Nexus.Storage.crazy_mode == false) ? net.whatswith.tools.Phone.FormatNumber(phone, Main.Nexus.SimCountryIso)
										: net.whatswith.tools.Phone.FormatNumber(phone, "ES");
								
								if (Main.Nexus.Storage.phoneNumber().compareTo(phone) != 0)
								{
									Bitmap BitPhoto = queryContactImage(pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID)), act);									
									ContactsInfo.put(phone, new Contacts_db(name, phone, BitPhoto, (BitPhoto == null) ? false : true));
								}
							}
							

						}
						pCur.close();
					}

				}
			}
		}
		return ContactsInfo; 
	}

	public static HashMap<String, Contacts_db> getContactsFromDevice(Activity act)
	{
		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER,
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.PHOTO_ID };
		Cursor people = act.getContentResolver().query(uri, projection, null, null, null);
		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
		int indexPhoto = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID);
		int indexNumberType = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);

		HashMap<String, Contacts_db> ContactsInfo = new HashMap<String, Contacts_db>();

		String phone = new String();
		try
		{
			people.moveToFirst();
			do
			{
				if (people.getInt(indexNumberType) == Phone.TYPE_MOBILE)
				{
					Bitmap BitPhoto = queryContactImage(people.getInt(indexPhoto), act);
					phone = (Main.Nexus.Storage.crazy_mode == false) ? net.whatswith.tools.Phone.FormatNumber(people.getString(indexNumber),
							Main.Nexus.SimCountryIso) : net.whatswith.tools.Phone.FormatNumber(people.getString(indexNumber), "ES");

					if (Main.Nexus.Storage.phoneNumber().compareTo(phone) != 0)
					{
						String name = people.getString(indexName);
						ContactsInfo.put(phone, new Contacts_db(name, phone, BitPhoto, (BitPhoto == null) ? false : true));
					}
				}
			} while (people.moveToNext());

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return ContactsInfo;
	}

	public static String[] ContactsToArray(HashMap<String, Contacts_db> c)
	{
		ArrayList<String> ContactsInfo = new ArrayList<String>();
		Iterator<Entry<String, Contacts_db>> it = c.entrySet().iterator();
		while (it.hasNext())
		{
			if (it.next().getValue().phone.isEmpty())
				continue;
			ContactsInfo.add(it.next().getValue().phone);
		}

		return ContactsInfo.toArray(new String[ContactsInfo.size()]);
	}

	public static String[] ContactsToArrayOnlySelected(HashMap<String, Contacts_db> c, boolean add_device_phone)
	{
		ArrayList<String> ContactsInfo = new ArrayList<String>();
		Iterator<Entry<String, Contacts_db>> it = c.entrySet().iterator();
		while (it.hasNext())
		{
			Contacts_db row = it.next().getValue();
			if (row.is_selected)
				ContactsInfo.add(row.phone);
		}
		if (add_device_phone == true)
			ContactsInfo.add(Main.Nexus.Storage.phoneNumber());

		return ContactsInfo.toArray(new String[ContactsInfo.size()]);
	}

	public static class CMP_Contact_db_Name implements Comparator<Contacts_db>
	{
		@Override
		public int compare(Contacts_db object1, Contacts_db object2)
		{
			return object1.name.compareToIgnoreCase(object2.name);
		}
	}

	public static class CMP_Contact_db_lock implements Comparator<Contacts_db>
	{
		@Override
		public int compare(Contacts_db object1, Contacts_db object2)
		{
			return (object1.is_locked && object2.is_locked) ? 0 : -1;
		}
	}

	public static int GenerateRamdomInt(int min, int max)
	{
		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static ArrayList<Contacts_db> sortAndFilterContacts(HashMap<String, Contacts_db> c, Comparator<Contacts_db> cmp)
	{
		ArrayList<Contacts_db> ContactsInfo = new ArrayList<Contacts_db>();
		Iterator<Entry<String, Contacts_db>> it = c.entrySet().iterator();
		while (it.hasNext())
		{
			Contacts_db row = it.next().getValue();
			if (row.phone.compareTo(Main.Nexus.Storage.login().user) != 0)
			{
				if (row.is_hidden == false)
					ContactsInfo.add(row);
			}

		}

		Collections.sort(ContactsInfo, cmp);
		return ContactsInfo;
	}

	private static Return QuestionsSyncByPhase_(final String user_id, final int phase_id)
	{
		try
		{
			String sync_get_questions_by_user_phase = RequestBuilder.sync_get_questions_by_user_phase(user_id, phase_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_questions_by_user_phase, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Questions_by_UserPhase> questions = new ArrayList<Questions_by_UserPhase>();
			if (ResponseBuilder.sync_get_questions_by_user_phase(response.toString(), questions, error) == true)
			{
				Main.Nexus.Storage.questions= new SparseArray<Question_db>();
				for (Questions_by_UserPhase row : questions)
				{
					Main.Nexus.Storage.questions.put(row.question_id, new Question_db(row));
				}
				return new Return(true, "Success", "Sync All Questions");

			} else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Questions");
		}
	}

	private static Return Get_top_proposals_of_question_(final int question_id, int[] proposal_id)
	{
		try
		{
			String sync_get_top_proposals_of_question = RequestBuilder.sync_get_top_proposals_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_top_proposals_of_question, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.sync_get_top_proposals_of_question(response.toString(), proposal_id, error) == true)
				return new Return(true, "Success", "Get Results");
			else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Get Results");
		}
	}

	private static Return GetUsersOfQuestion_(final int question_id, ArrayList<String> users_ids)
	{
		try
		{
			String sync_get_users_of_question = RequestBuilder.sync_get_users_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_users_of_question, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.sync_get_users_of_question(response.toString(), users_ids, error) == true)
				return new Return(true, "Success", "Sync All Proposals");
			else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Proposals");
		}
	}

	private static Return ProposalsSyncByQuestion_(final int question_id)
	{
		try
		{
			String sync_get_sort_proposals_of_question = RequestBuilder.sync_get_sort_proposals_of_question(question_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_sort_proposals_of_question, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Proposals_db> proposals = new ArrayList<Proposals_db>();
			if (ResponseBuilder.sync_get_sort_proposals_of_question(response.toString(), proposals, error) == true)
			{
				Main.Nexus.Storage.proposals = new SparseArray<Proposals_db>();
				for (Proposals_db row : proposals)
				{
					Main.Nexus.Storage.proposals.put(row.proposal_id, new Proposals_db(row));
				}
				return new Return(true, "Success", "Sync All Proposals");

			} else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Proposals");
		}
	}

	private static SparseArray<Question_db> QuestionsSync_ISOLATE_(final String user_id, Return ret)
	{
		try
		{
			String sync_get_questions_revs_of_user = RequestBuilder.sync_get_questions_revs_of_user(user_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_questions_revs_of_user, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Questions_rev_com> questions_ret = new ArrayList<Questions_rev_com>();
			if (ResponseBuilder.sync_get_questions_revs_of_user(response.toString(), questions_ret, error) == true)
			{
				SparseArray<Question_db> questions = new SparseArray<Question_db>();
				for (Questions_rev_com row : questions_ret)
				{
					questions.put(row.question_id, new Question_db(row));
				}
				ret.Set(true, "Success", "Sync All Questions");
				return questions;
			} else
			{
				ret.Set(false, "Error", error.toString());
				return new SparseArray<Question_db>();
			}

		} catch (Exception e)
		{
			ret.Set(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Questions");
			return new SparseArray<Question_db>();
		}
	}

	private static Return QuestionsSync_(final String user_id)
	{
		try
		{
			String sync_get_questions_revs_of_user = RequestBuilder.sync_get_questions_revs_of_user(user_id);
			StringBuilder response = new StringBuilder();
			SendServer(sync_get_questions_revs_of_user, response);
			StringBuilder error = new StringBuilder();
			ArrayList<Questions_rev_com> questions = new ArrayList<Questions_rev_com>();
			if (ResponseBuilder.sync_get_questions_revs_of_user(response.toString(), questions, error) == true)
			{
				Main.Nexus.Storage.questions = new SparseArray<Question_db>();
				for (Questions_rev_com row : questions)
				{
					Main.Nexus.Storage.questions.put(row.question_id, new Question_db(row));
				}
				return new Return(true, "Success", "Sync All Questions");

			} else
				return new Return(false, "Error", error.toString());

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Sync All Questions");
		}
	}

	private static Return PasswordChange_(final String user_id, final String password_old, final String password_new)
	{
		try
		{
			String password_change = RequestBuilder.password_change(user_id, password_old, password_new);
			StringBuilder response = new StringBuilder();
			SendServer(password_change, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.password_change(response.toString(), error) == false)
				return new Return(false, error.toString(), "Password Change");
			else
				return new Return(true, user_id + " password success modified.", "New User");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Password Change");
		}
	}

	private static Return QuestionOpen_(final String user_id, final String question, final String exp_date, final int delay_days, final String[] proposals,
			final String[] users_ids, Integer question_id)
	{
		try
		{
			String question_open = RequestBuilder.question_open(user_id, question, exp_date, delay_days, proposals, users_ids);
			StringBuilder response = new StringBuilder();
			SendServer(question_open, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.question_open(response.toString(), question_id, error) == false)
				return new Return(false, error.toString(), "Quesion Open");
			else
				return new Return(true, user_id + "Question Created success.", "Question Open");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Question Open");
		}
	}

	private static Return CreateUser(final String user_id, final String password)
	{
		try
		{
			String query_user_add = RequestBuilder.user_add(user_id, password);
			StringBuilder response = new StringBuilder();
			SendServer(query_user_add, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "New User");
			else
				return new Return(true, user_id + " successful Created.", "New User");

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "New User");
		}
	}

	private static Return AuthUser(final String user_id, final String password)
	{
		try
		{
			String query_user_password_exist = RequestBuilder.user_password_exist(user_id, password);
			StringBuilder response = new StringBuilder();
			SendServer(query_user_password_exist, response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.user_add(response.toString(), error) == false)
				return new Return(false, error.toString(), "Auth User");
			else
				return new Return(true, user_id + " successful Authenticated", "Auth User", password);

		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "New User");
		}
	}

	private static boolean CloseSeverConn()
	{
		if (Conn == null) return true;	
		if (Conn.close())
		{
			return true;
		} else
			return false;
	}

	private static Return UnPlug_(Login login)
	{
		try
		{
			StringBuilder response = new StringBuilder();
			Conn.CloseSession(response);
			StringBuilder error = new StringBuilder();
			if (ResponseBuilder.session_close(response.toString(), error) == false)
				return new Return(false, error.toString(), "Close Session");
			else
				return new Return(true, "Success", "Close Session");
		} catch (Exception e)
		{
			return new Return(false, Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString(), "Close Sessio");
		}
	}

	private static Return OpenServerConn(final String hostname, final int port)
	{
		Conn = new SSLConnSever();
		Return ret = Conn.open(hostname, port);
		if (ret.status == true)
		{
			return ret;
		} else
			return ret;
	}

	private static boolean isAuth()
	{
		if (Conn == null)
			return false;
		return Conn.IsAuth();
	}

	private static Return isConnected()
	{
		if (Conn == null)
			return new Return(false, "Sock is Null", "Is Connected");
		return Conn.IsConnected();
	}

	private static boolean SendServer(String request, StringBuilder response)
	{
		if (Conn == null)
			return false;
		if (Conn.send(request, response) == true)
		{
			return true;
		} else
			return false;
	}

	private static Bitmap queryContactImage(int imageDataRow, Activity act)
	{
		Cursor c = act.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[] { ContactsContract.CommonDataKinds.Photo.PHOTO },
				ContactsContract.Data._ID + "=?", new String[] { Integer.toString(imageDataRow) }, null);
		byte[] imageBytes = null;
		if (c != null)
		{
			if (c.moveToFirst())
			{
				imageBytes = c.getBlob(0);
			}
			c.close();
		}

		if (imageBytes != null)
		{
			return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		} else
		{
			return null;
		}
	}

	public static String JsonSpecialCharFixer(String text)
	{
		String ret = text.replaceAll("\\\\", "\\\\\\\\");
		ret = text.replaceAll("\"", "\\\"");
		ret = text.replaceAll("'", "''");

		return ret;
	}

}
