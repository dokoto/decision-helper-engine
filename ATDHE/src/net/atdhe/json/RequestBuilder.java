package net.atdhe.json;

import org.json.JSONArray;
import org.json.JSONObject;

public class RequestBuilder
{
	public static String user_password_exist(final String user_id, final String password)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "USER_PASSWORD");
			jsonRoot.put("ACCTION", "EXIST");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("PASSWD", password);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String password_change(final String user_id, final String password_old, final String password_new)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "PASSWORD");
			jsonRoot.put("ACCTION", "CHANGE");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("PASSWD_OLD", password_old);
			jsonRoot.put("PASSWD_NEW", password_new);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String user_exist(final String user_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "USER");
			jsonRoot.put("ACCTION", "EXIST");
			jsonRoot.put("USER_ID", user_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String proposals_add(final String user_id, final int question_id, final String[] proposals)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "PROPOSALS");
			jsonRoot.put("ACCTION", "ADD");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("QUESTION_ID", question_id);
			JSONArray ArrayElem = new JSONArray();
			for (String row : proposals)
				ArrayElem.put(row);
			jsonRoot.put("PROPOSALS", ArrayElem);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String question_add_users(final int question_id, final String[] users_ids)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "QUESTION");
			jsonRoot.put("ACCTION", "ADD_USERS");
			jsonRoot.put("QUESTION_ID", question_id);
			JSONArray ArrayElem = new JSONArray();
			for (String row : users_ids)
				ArrayElem.put(row);
			jsonRoot.put("USERS_IDS", ArrayElem);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String userproposals_add(final String user_id, final int question_id, final int[] proposals_ids)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "USERPROPOSALS");
			jsonRoot.put("ACCTION", "ADD");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("QUESTION_ID", question_id);
			JSONArray ArrayElem = new JSONArray();
			for (int row : proposals_ids)
				ArrayElem.put(row);
			jsonRoot.put("PROPOSALS_IDS", ArrayElem);

			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	
	public static String debug_are_there_questions_in_phase(final int sort_index)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "DEBUG");
			jsonRoot.put("ACCTION", "ARE_THERE_QUESTIONS_IN_PHASE");
			jsonRoot.put("SORT_INDEX", sort_index);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}
	
	public static String question_accept(final String user_id, final int question_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "QUESTION");
			jsonRoot.put("ACCTION", "ACCEPT");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("QUESTION_ID", question_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}
	
	public static String sync_get_users_of_question(final int question_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_USERS_OF_QUESTION");
			jsonRoot.put("QUESTION_ID", question_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}		
	}		

	public static String user_array_exist(final String[] users_ids)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "USER");
			jsonRoot.put("ACCTION", "ARRAY_EXIST");
			JSONArray ArrayElem = new JSONArray();
			for (String row : users_ids)
				ArrayElem.put(row);
			jsonRoot.put("USERS_IDS", ArrayElem);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String user_add(final String user_id, final String password)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "USER");
			jsonRoot.put("ACCTION", "ADD");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("PASSWD", password);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String question_open(final String user_id, final String quesion, final String exp_date, final int delay_days,
			final String[] proposals, final String[] users_ids)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{

			jsonRoot.put("CMD", "QUESTION");
			jsonRoot.put("ACCTION", "OPEN");

			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("QUESTION", quesion);
			jsonRoot.put("EXP_DATE", exp_date);
			jsonRoot.put("DELAY_BETWEEN_PHASES_IN_DAYS", delay_days);

			JSONArray ArrayElem = new JSONArray();
			for (String row : proposals)
				ArrayElem.put(row);
			jsonRoot.put("PROPOSALS", ArrayElem);

			ArrayElem = new JSONArray();
			for (String row : users_ids)
				ArrayElem.put(row);
			jsonRoot.put("USERS_IDS", ArrayElem);

			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_phase_sort()
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_PHASE_SORT");
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_sort_proposals_of_question(final int question_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_SORT_PROPOSALS_OF_QUESTION");
			jsonRoot.put("QUESTION_ID", question_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_questions_revs_of_user(final String user_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_QUESTIONS_REVS_OF_USER");
			jsonRoot.put("USER_ID", user_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_top_proposals_of_question(final int question_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_TOP_PROPOSALS_OF_QUESTION");
			jsonRoot.put("QUESTION_ID", question_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_questions_by_user_phase(final String user_id, final int phase_id)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_QUESTIONS_BY_USER_PHASE");
			jsonRoot.put("USER_ID", user_id);
			jsonRoot.put("PHASE_ID", phase_id);
			return jsonRoot.toString();
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

	public static String sync_get_questions_sort(final int[] question_ids)
	{
		JSONObject jsonRoot = new JSONObject();
		try
		{
			jsonRoot.put("CMD", "SYNC");
			jsonRoot.put("ACCTION", "GET_QUESTIONS_SORT");

			JSONArray ArrayContent = new JSONArray();
			for (int question_id : question_ids)
				ArrayContent.put(question_id);
			jsonRoot.put("QUESTIONS_IDS", ArrayContent);
			return jsonRoot.toString();

		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " : " + e.toString());
			return new String();
		}
	}

}
