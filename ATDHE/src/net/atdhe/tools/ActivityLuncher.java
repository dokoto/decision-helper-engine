package net.atdhe.tools;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import net.atdhe.structs.IActivityLuncher;

public class ActivityLuncher implements IActivityLuncher
{
	private Activity act;
	private Class<?> obj;
	private String key;
	private int value;

	public ActivityLuncher(Activity act, Class<?> obj)
	{
		this.act = act;
		this.obj = obj;
	}

	public ActivityLuncher(Activity act, Class<?> obj, String key, int value)
	{
		this.act = act;
		this.obj = obj;
		this.key = key;
		this.value = value;
	}

	public void Launch()
	{
		Intent intent = new Intent(act, obj);
		act.startActivity(intent);
	}

	public void LaunchWithKey()
	{
		Intent intent = new Intent(act, obj);
		Bundle b = new Bundle();
		b.putInt(key, value);
		intent.putExtras(b);
		act.startActivity(intent);
	}
}
