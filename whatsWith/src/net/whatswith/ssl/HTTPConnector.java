package net.whatswith.ssl;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

public class HTTPConnector
{
	public enum protocol
	{
		HTTP, HTTPS
	};

	private static String PROTOCOL_HTTP = "http:";
	private static String PROTOCOL_HTTPS = "https:";
	private static String METHOD = "/jdhe";
	private static String url;
	private static int port;
	private static String host;
	private SSLSocketFactory sf;
	private static String user, password;
	

	public HTTPConnector(String host, int port, protocol proto, String user, String password) throws Exception
	{
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
		if (proto == protocol.HTTP)
			this.url = PROTOCOL_HTTP + "//" + host + ":" + port + METHOD;
		else
			this.url = PROTOCOL_HTTPS + "//" + host + ":" + port + METHOD;
	}

	private HttpClient getHttpClient(int port) throws Exception
	{
		java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
		X509TrustManager trustManager = new X509TrustManager()
		{
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
			{
			}

			public X509Certificate[] getAcceptedIssuers()
			{
				return new java.security.cert.X509Certificate[0];
			}
		};

		SSLContext sslContext = SSLContext.getInstance("TLSv1");
		sslContext.init(null, new TrustManager[] { trustManager }, new SecureRandom());
		sf = sslContext.getSocketFactory();

		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), port));
		registry.register(new Scheme("https", (SocketFactory) sf, port));

		ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

		return new DefaultHttpClient(ccm, params);
	}

	public String Send(String inputTyped)
	{
		try
		{
			DefaultHttpClient client = (DefaultHttpClient) getHttpClient(port);
			HttpPost post = new HttpPost(HTTPConnector.url);
			post.setEntity(new StringEntity(inputTyped, "utf-8"));
			HttpResponse response = client.execute(post);
			HttpEntity responseEntity = response.getEntity();
			InputStream is = responseEntity.getContent();
			byte[] bytes = new byte[1000];
			StringBuilder line = new StringBuilder();
			int numRead = 0;
			while ((numRead = is.read(bytes)) >= 0)
				line.append(new String(bytes, 0, numRead));

			return line.toString();

		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
