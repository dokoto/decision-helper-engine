package net.whatswith.screens.questions;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.screens.proposals.Set_proposals_preference;
import net.whatswith.screens.proposals.Show_proposals_result;
import net.whatswith.structs.Question_db;
import net.whatswith.structs.Questions_by_UserPhase;
import net.whatswith.structs.Return;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class List_questions extends ListActivity
{

	private boolean resumeHasRun = false;
	private ArrayList<Questions_by_UserPhase> questions_by_phase = null;
	private ListQuestionsAdapter adapter;
	private int Phase_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		try
		{	
			Bundle b = getIntent().getExtras();
			Phase_id = b.getInt("PhaseId");			
			setTitle(Main.Nexus.Storage.phases.get(Phase_id).phase);
			InitListQuestions();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{		
		Intent intent = null;		
		if (Main.Nexus.Storage.phases.get(Phase_id).sort_index == 1 )	
		{
			intent = new Intent(List_questions.this, View_question.class);
			Bundle b = new Bundle();
			b.putInt("QuestionId", questions_by_phase.get(position).question_id);
			b.putInt("PhaseId", questions_by_phase.get(position).phase_id);
			intent.putExtras(b);
		}
		else if (Main.Nexus.Storage.phases.get(Phase_id).sort_index == 2 )
		{			
			intent = new Intent(List_questions.this, Set_proposals_preference.class);
			Bundle b = new Bundle();
			b.putInt("QuestionId", questions_by_phase.get(position).question_id);
			b.putInt("PhaseId", questions_by_phase.get(position).phase_id);
			intent.putExtras(b);
		}
		else if (Main.Nexus.Storage.phases.get(Phase_id).sort_index == 3 )
		{
			intent = new Intent(List_questions.this, Show_proposals_result.class);
			Bundle b = new Bundle();
			b.putInt("QuestionId", questions_by_phase.get(position).question_id);
			b.putInt("PhaseId", questions_by_phase.get(position).phase_id);
			intent.putExtras(b);	
		}
		
		startActivity(intent);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (resumeHasRun == true)
		{
			InitListQuestions();
		}else resumeHasRun = true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_list_questions, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.menu_list_questions_refresh:
			InitListQuestions();
			break;
		default:
			Main.Nexus.MenuSelector(this, item);
			break;
		}

		return true;
	}
	
	private void InitListQuestions()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(List_questions.this);
				pd.setTitle("Upgrading Questions...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = Commands.QuestionsSyncByPhase(Main.Nexus.Storage.login(), Phase_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				}
				else
				{
					questions_by_phase = Commands.GetQuestionsByPhaseForList(Phase_id);
					if (questions_by_phase.isEmpty())
						List_questions.this.finish();
					if (null == adapter)
					{
						adapter = new ListQuestionsAdapter(List_questions.this, questions_by_phase);
						setListAdapter(adapter);
					}
					else
					{						
						adapter.clear();						
						if (android.os.Build.VERSION.SDK_INT > 10)
							adapter.addAll(questions_by_phase);
						else
						{
						    for (int i = 0; i < questions_by_phase.size(); i++) {
						        adapter.add(questions_by_phase.get(i));
						    }
						}
						adapter.setNotifyOnChange(true);						
					}
					
				}
			}
		};
		task.execute((Void[]) null);
	}
	
	private static class ViewHolder
	{
		public TextView tv_list_questions_question;
		public TextView tv_list_questions_user;
		public TextView tv_list_questions_exp_date;
	}
	
	public class ListQuestionsAdapter extends ArrayAdapter<Questions_by_UserPhase>
	{
		private final Context context;
		ViewHolder holder;
		ArrayList<Questions_by_UserPhase> rows;
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");



		public ListQuestionsAdapter(Context context, ArrayList<Questions_by_UserPhase> Rows)
		{
			super(context, R.layout.layo_row_list_questions, Rows);
			this.context = context;
			this.rows = Rows;
		}
				
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_list_questions, parent, false);
				holder = new ViewHolder();
				holder.tv_list_questions_question = (TextView) rowView.findViewById(R.id.layo_list_questions_question);
				holder.tv_list_questions_user = (TextView) rowView.findViewById(R.id.layo_list_questions_user);
				holder.tv_list_questions_exp_date = (TextView) rowView.findViewById(R.id.layo_list_questions_exp_date);

				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();
			Question_db q = Main.Nexus.Storage.questions.get(rows.get(position).question_id);
			
			if (q.is_phase_accepted == false)
				holder.tv_list_questions_question.setTypeface(null, Typeface.BOLD);
			else
				holder.tv_list_questions_question.setTypeface(null, Typeface.NORMAL);
			
			String scolor = Main.Nexus.Storage.phases.get(rows.get(position).phase_id).color;
			try
			{
				if (scolor.substring(0, 1).compareTo("#") != 0)
					scolor = "#" + scolor;
				int icolor = Color.parseColor(scolor);			
				holder.tv_list_questions_question.setTextColor(icolor);
			}
			catch (Exception e)
			{
				holder.tv_list_questions_question.setTextColor(Color.parseColor("black"));
			}
			
			holder.tv_list_questions_question.setText(rows.get(position).question);
			holder.tv_list_questions_user.setText(Commands.GetContactNameFromPhone(rows.get(position).user_id, List_questions.this));
			holder.tv_list_questions_exp_date.setText(formatter.format(rows.get(position).exp_date));

			return rowView;
		}
	}

}
