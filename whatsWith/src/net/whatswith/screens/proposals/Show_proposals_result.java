package net.whatswith.screens.proposals;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Question_db;
import net.whatswith.structs.Return;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Show_proposals_result extends Activity
{
	private int QuestionId;
	private int PhaseId;
	private ArrayList<Adapter_row> ProposalsAdapter = null;
	private Adapter_ViewQuestion Adapter;
	private ListView lv_proposals;
	private int[] proposal_id = new int[1];

	private class Adapter_row
	{

		public Adapter_row(int Proposal_id, String Proposal, String User_id)
		{
			this.Proposal_id = Proposal_id;
			this.User_id = User_id;
			this.Proposal = Proposal;
		}

		public int Proposal_id;
		public String Proposal;
		public String User_id;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_show_proposals_result);
		try
		{
			Bundle b = getIntent().getExtras();
			QuestionId = b.getInt("QuestionId");
			PhaseId = b.getInt("PhaseId");
			lv_proposals = (ListView) findViewById(R.id.layo_show_proposals_result_lv_proposal);
			InitViewProposalResult();
		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	private void InitViewProposalResult()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{			
			private Return ret;

			@Override
			protected void onPreExecute()
			{				
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = Commands.Get_top_proposals_of_question(Main.Nexus.login(), QuestionId, proposal_id);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{				
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
					getQuestion();
			}
		};
		task.execute((Void[]) null);
	}

	private void getQuestion()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret;
			private Format formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
			
			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(Show_proposals_result.this);
				pd.setTitle("Getting Results...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = Commands.ProposalsSyncByQuestion(Main.Nexus.login(), QuestionId);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
				{
					ProposalsAdapter = new ArrayList<Adapter_row>();
					for (int i = 0; i < Main.Nexus.Storage.proposals.size(); ++i)
					{
						int key = Main.Nexus.Storage.proposals.keyAt(i);
						ProposalsAdapter.add(new Adapter_row(key, Main.Nexus.Storage.proposals.get(key).proposal, Main.Nexus.Storage.proposals.get(key).user_id));
					}
					TextView et_question = (TextView) findViewById(R.id.layo_show_proposals_result_tv_question);
					TextView et_exp_date = (TextView) findViewById(R.id.layo_show_proposals_result_tv_exp_date);
					TextView et_user_id = (TextView) findViewById(R.id.layo_show_proposals_result_tv_user_id_owner);
					SetPhaseLayout(et_question);
					Question_db q = Main.Nexus.Storage.questions.get(QuestionId);
					if (null != q)
					{
						et_question.setText(q.question);
						et_exp_date.setText(formatter.format(q.exp_date));
						et_user_id.setText(Commands.GetContactNameFromPhone(q.user_id, Show_proposals_result.this));
					}

					if (null == Adapter)
					{
						Adapter = new Adapter_ViewQuestion(Show_proposals_result.this, ProposalsAdapter);
						lv_proposals.setAdapter(Adapter);
					} else
					{
						Adapter.clear();
						if (android.os.Build.VERSION.SDK_INT > 10)
							Adapter.addAll(ProposalsAdapter);
						else
						{
						    for (int i = 0; i < ProposalsAdapter.size(); i++) {
						    	Adapter.add(ProposalsAdapter.get(i));
						    }
						}
						Adapter.setNotifyOnChange(true);
					}
					Commands.Async_Question_Accept(Main.Nexus.login(), Show_proposals_result.this, null, false, QuestionId);
					pd.dismiss();
				}
			}
		};
		task.execute((Void[]) null);
	}

	private void SetPhaseLayout(TextView obj, int color)
	{
		try
		{
			obj.setTextColor(color);
			obj.setTypeface(null, Typeface.BOLD);
		} catch (Exception e)
		{
			obj.setTextColor(Color.parseColor("black"));
		}
	}	
	
	
	private void SetPhaseLayout(TextView obj)
	{
		String color = Main.Nexus.Storage.phases.get(PhaseId).color;
		try
		{
			if (color.substring(0, 1).compareTo("#") != 0)
				color = "#" + color;
			int icolor = Color.parseColor(color);
			obj.setTextColor(icolor);
			obj.setTypeface(null, Typeface.BOLD);
		} catch (Exception e)
		{
			obj.setTextColor(Color.parseColor("black"));
		}
	}
	
	private static class ViewHolder
	{
		public TextView tv_proposal;
		public TextView tv_user_id;
	}
	
	public class Adapter_ViewQuestion extends ArrayAdapter<Adapter_row>
	{
		public final Context ContextAdapter;
		public ArrayList<Adapter_row> Rows;
		ViewHolder holder;


		public Adapter_ViewQuestion(Context context, ArrayList<Adapter_row> rows)
		{
			super(context, R.layout.layo_row_show_proposals_result, rows);
			this.ContextAdapter = context;
			this.Rows = rows;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) ContextAdapter.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_show_proposals_result, parent, false);
				holder = new ViewHolder();
				holder.tv_proposal = (TextView) rowView.findViewById(R.id.layo_row_show_proposals_result_tv_proposal);
				holder.tv_user_id = (TextView) rowView.findViewById(R.id.layo_row_show_proposals_result_tv_user_id);
				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();
			holder.tv_user_id.setText(Commands.GetContactNameFromPhone(Rows.get(position).User_id, Show_proposals_result.this));		
			
			if (Rows.get(position).Proposal_id == proposal_id[0])
			{
				rowView.setBackgroundResource(R.drawable.shape_item_border);
				SetPhaseLayout(holder.tv_proposal);
				holder.tv_proposal.setTypeface(null, Typeface.BOLD);
			}
			else
			{
				rowView.setBackgroundResource(R.drawable.shape_item_border_default);
				SetPhaseLayout(holder.tv_proposal, Color.parseColor("black"));
				holder.tv_proposal.setTypeface(null, Typeface.NORMAL);
			}
			
			holder.tv_proposal.setText(Rows.get(position).Proposal);
			
			notifyDataSetChanged();
			return rowView;
		}

	}

}
