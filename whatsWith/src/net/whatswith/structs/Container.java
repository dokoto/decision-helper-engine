package net.whatswith.structs;

import java.util.HashMap;
import net.whatswith.screens.main.Main;
import net.whatswith.tools.Phone;
import com.google.gson.annotations.Expose;
import android.util.SparseArray;

public class Container
{
	@Expose public String password;	
	@Expose public String host;
	@Expose public int port;
	@Expose private String phoneNumber;
	@Expose public boolean crazy_mode;
	@Expose public int intervalInMin;
	@Expose public boolean desactv_conn;
	
	public boolean debug_menu_on;
	public SparseArray<Question_db> questions;
	public SparseArray<Proposals_db> proposals;
	public SparseArray<Phases_db> phases;
	public HashMap<String, Contacts_db> contacts;
	public HashMap<String, Contacts_db> contactsForUser;

	public Container()
	{
		password = new String();		
		host = "mansierra.homelinux.net";
		port = 59234;
		phoneNumber = new String();
		crazy_mode = false;
		debug_menu_on = true;
		intervalInMin = 1;
		questions = new SparseArray<Question_db>();
		proposals = new SparseArray<Proposals_db>();
		phases = new SparseArray<Phases_db>();
		contacts = new HashMap<String, Contacts_db>();
	}
	
	public Login login()
	{
		return new Login(host, port, phoneNumber(), password);
	}
	
	public void phoneNumber(String p, String SimCountryIso)
	{
		if (Main.Nexus.Storage.crazy_mode == false)
			phoneNumber = Phone.FormatNumber(p, SimCountryIso);
		else
			phoneNumber = Phone.FormatNumber(p, "ES");
	}
	
	public String phoneNumber()
	{
		return phoneNumber;
	}
}
