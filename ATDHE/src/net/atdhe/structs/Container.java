package net.atdhe.structs;

import java.util.HashMap;
import net.atdhe.Main;
import net.atdhe.tools.Phone;
import com.google.gson.annotations.Expose;
import android.util.SparseArray;

public class Container
{
	@Expose
	public String host;
	@Expose
	public int port;
	@Expose
	private String phoneNumber;
	@Expose
	public boolean crazy_mode;
	@Expose
	public int intervalInMin;
	@Expose
	public boolean desactv_conn;

	public boolean debug_menu_on;
	public SparseArray<Question_db> questions;
	public SparseArray<Proposals_db> proposals;
	public SparseArray<Phases_db> phases;
	public HashMap<String, Contacts_db> contacts;

	public Container()
	{
		host = "mansierra.homelinux.net";
		port = 59234;
		phoneNumber = new String();
		crazy_mode = false;
		debug_menu_on = true;
		intervalInMin = 1;
		questions = new SparseArray<Question_db>();
		proposals = new SparseArray<Proposals_db>();
		phases = new SparseArray<Phases_db>();
		contacts = new HashMap<String, Contacts_db>();
	}

	public Login login(Contacts_db cc)
	{
		return new Login(host, port, cc.phone, cc.password);
	}

	public void phoneNumber(String p, String SimCountryIso)
	{
		if (Main.Nexus.Storage.crazy_mode == false)
			phoneNumber = Phone.FormatNumber(p, SimCountryIso);
		else
			phoneNumber = Phone.FormatNumber(p, "ES");
	}

	public String phoneNumber()
	{
		return phoneNumber;
	}
}
