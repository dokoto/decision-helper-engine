package net.whatswith.json;

import java.util.ArrayList;
import java.util.HashMap;
import net.whatswith.structs.Contacts_db;
import net.whatswith.structs.Phases_db;
import net.whatswith.structs.Proposals_db;
import net.whatswith.structs.Questions_by_UserPhase;
import net.whatswith.structs.Questions_rev_com;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.SparseArray;

public class ResponseBuilder
{

	private static final String TAG_RESULT = "RESULT";
	private static final String TAG_ERROR = "ERROR";
	private static final String TAG_PHASE_ID = "PHASE_ID";
	private static final String TAG_USER_ID = "USER_ID";
	private static final String TAG_SIGN_DATE = "SIGN_DATE";
	private static final String TAG_QUESTION_ID = "QUESTION_ID";
	private static final String TAG_PROPOSAL_ID = "PROPOSAL_ID";

	public static boolean parse(final String json)
	{
		try
		{
			new JSONObject(json);
			return true;
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public static boolean extractResulSimple(final JSONObject jsonResponse, StringBuilder error)
	{
		try
		{
			boolean ret = jsonResponse.getBoolean(TAG_RESULT);
			if (!ret)
			{
				error.append(jsonResponse.getString(TAG_ERROR));
				return false;
			}
			return true;
		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public static boolean extractResulSimple(final String response, StringBuilder error)
	{
		try
		{
			JSONObject jsonResponse = new JSONObject(response);
			return extractResulSimple(jsonResponse, error);
		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public static boolean is_auth(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean session_close(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean user_password_exist(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean password_change(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean user_exist(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean user_add(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}
	
	public static boolean proposals_add(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}
	
	public static boolean question_add_users(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}
	
	public static boolean userproposals_add(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}
	
	public static boolean question_accept(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}

	public static boolean debug_are_there_questions_in_phase(final String response, StringBuilder error)
	{
		return extractResulSimple(response, error);
	}
	
	public static boolean sync_get_top_proposals_of_question(final String response, int[] proposal_id, StringBuilder error)
	{
		try
		{
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONObject jsonChild = jsonRoot.getJSONObject(TAG_RESULT);
				proposal_id[0] = jsonChild.getInt(TAG_PROPOSAL_ID);
				return true;
			} else
				return extractResulSimple(jsonRoot, error);
			
		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}		
	}
	
	public static boolean question_open(final String response, Integer quesion_id, StringBuilder error)
	{
		try
		{
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONObject jsonChild = jsonRoot.getJSONObject(TAG_RESULT);
				quesion_id = jsonChild.getInt(TAG_QUESTION_ID);
				return true;
			} else
				return extractResulSimple(jsonRoot, error);
			
		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}
	
	public static boolean user_array_exist(final String response, HashMap<String, Contacts_db> Contacts, StringBuilder error)
	{
		try
		{
			assert (Contacts != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)
				{
					String key = arrayContent.getJSONObject(i).getString(TAG_USER_ID);
					String sign_date = arrayContent.getJSONObject(i).getString(TAG_SIGN_DATE);
					if ( Contacts.containsKey(key) == true)					
						Contacts.get(key).upGradeWithDb(key, sign_date);					
					else
						Contacts.put( key, new Contacts_db(key, sign_date, false) );
				}
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}

	public static boolean sync_get_phase_sort(final String response, SparseArray<Phases_db> Phases, StringBuilder error)
	{
		try
		{
			assert (Phases != null);
			assert (response != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)
					Phases.put(arrayContent.getJSONObject(i).getInt(TAG_PHASE_ID), new Phases_db(arrayContent.getJSONObject(i)));
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}
	
	public static boolean sync_get_questions_by_user_phase(final String response, ArrayList<Questions_by_UserPhase> questions, StringBuilder error)
	{
		try
		{
			assert (questions != null);
			assert (response != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)
				{
					questions.add(new Questions_by_UserPhase(arrayContent.getJSONObject(i)));
				}
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}
	
	public static boolean sync_get_users_of_question(final String response, ArrayList<String> users_ids, StringBuilder error)
	{
		try
		{
			assert (users_ids != null);
			assert (response != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)				
					users_ids.add(arrayContent.getJSONObject(i).getString(TAG_USER_ID));				
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}
	public static boolean sync_get_sort_proposals_of_question(final String response, ArrayList<Proposals_db> proposals, StringBuilder error)
	{
		try
		{
			assert (proposals != null);
			assert (response != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)
				{
					proposals.add(new Proposals_db(arrayContent.getJSONObject(i)));
				}
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}
	
	public static boolean sync_get_questions_revs_of_user(final String response, ArrayList<Questions_rev_com> questions, StringBuilder error)
	{
		try
		{
			assert (questions != null);
			assert (response != null);
			JSONObject jsonRoot = new JSONObject(response);
			if (jsonRoot.has(TAG_ERROR) == false)
			{
				JSONArray arrayContent = jsonRoot.getJSONArray(TAG_RESULT);
				for (int i = 0; i < arrayContent.length(); i++)
				{
					questions.add(new Questions_rev_com(arrayContent.getJSONObject(i)));
				}
				return true;
			} else
				return extractResulSimple(jsonRoot, error);

		} catch (Exception e)
		{
			error.append(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			System.err.println(error);
			return false;
		}
	}

}
