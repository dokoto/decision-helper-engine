-- RESTAURAR DB
-- WIN: "C:/Program Files (x86)/PostgreSQL/8.4/bin\psql.exe" -U postgres dhe < "D:\DATOS\PERSONALES\DESARROLLOS\C++\GIT\decision-helper-engine.git\DHE\sql\dhe_backup.sql"
-- MAC: psql -h localhost -U postgres -W dhe < dhe_backup.sql
-- SERVER : /usr/bin/pg_restore --host mansierra.homelinux.net --port 5432 --username "postgres" --dbname "dhe" --no-password  --verbose "/dhe_bin_tar"
-- LINUX: createlang -h localhost -U postgres -W -d dhe plpgsql
--        psql -h localhost -U postgres -W -d dhe -f /usr/share/postgresql/8.4/contrib/pgcrypto.sql
--        psql -h localhost -U postgres -W dhe < dhe_backup.sql

CREATE TABLE users (
    user_id character varying(15) primary key,
    passwd character varying(80) NOT NULL,
    signin_date timestamp without time zone NOT NULL,
    is_locked boolean NOT NULL,
    locked_date timestamp without time zone,
    activation_date timestamp without time zone
);

CREATE TABLE questions (
    question_id serial primary key,
    user_id character varying(15) references users (user_id) on delete cascade,    
    creation_date timestamp without time zone NOT NULL,
    is_deprecated boolean NOT NULL,
    deprecated_date timestamp without time zone NOT NULL,
    accept boolean NOT NULL,
    text character varying(100) NOT NULL
);

CREATE TABLE proposals (
    proposals_id serial primary key,
    question_id integer references questions (question_id) on delete cascade,
    user_id character varying(15)  references users (user_id) on delete cascade,
    text character varying(100) NOT NULL,
    priority smallint NOT NULL
);

CREATE TABLE usersquestions (
    usersquestions SERIAL primary key,
    question_id integer references questions (question_id) on delete cascade,
    user_id character varying(15)  references users (user_id) on delete cascade,
	fase smallint NOT NULL
);


--select * from users;
--select question_add('622208039', 'Que hacemos este finde ?');
--select proposal_add('622208031', 1, 'Vamos de perras', 1);
--select proposals_add('622208031', 1, ARRAY['Ir al cine', 'Ir de pesca', 'Ir a la Montaña']);
--select question_open('622208031', 'Que hacemos este finde ?', ARRAY['Ir al cine', 'Ir de pesca', 'Ir a la Montaña']);

--select sync_get_all('62207835');
--select sync_get_new_update(ARRAY[48]);
--select userproposals_add('622207831', '48', ARRAY[99, 98, 97, 96, 95, 94, 93])
--select userproposals_add('622207834', '48', ARRAY[94, 93, 97, 96, 95, 99, 98])
--select sync_get_proposals_top(48);


CREATE OR REPLACE FUNCTION user_add(user_id character varying, passwd character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
INSERT INTO users VALUES (user_id, crypt(passwd, gen_salt('md5')), NOW(), FALSE, null);
END;
$$

CREATE OR REPLACE FUNCTION user_exist(in_user_id character varying, in_passwd character varying) RETURNS boolean 
    LANGUAGE plpgsql
    AS $$
DECLARE
	ret integer;
BEGIN
	select count(in_user_id) into ret from users where user_id = in_user_id and passwd = crypt(in_passwd, passwd);
	if (ret = 1) then
		return TRUE;
	else
		return FALSE;
	end if;
END;
$$

CREATE or replace FUNCTION question_add(in_user_id character varying, in_question character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	l_question_id integer;
BEGIN
	INSERT INTO questions VALUES (DEFAULT, in_user_id, NOW(), FALSE, null, FALSE, in_question) returning question_id into l_question_id;
	return l_question_id;
END;
$$;

CREATE or replace FUNCTION proposal_add(in_user_id character varying, in_question_id integer, in_proposal character varying, in_priority integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	l_proposals_id integer;
BEGIN
	INSERT INTO proposals VALUES (DEFAULT, in_question_id, in_user_id, in_proposal, in_priority) returning proposals_id into l_proposals_id;
	return l_proposals_id;
END;
$$;

CREATE or replace FUNCTION proposals_add(in_user_id character varying, in_question_id integer, in_proposals character varying[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	l_proposal character varying;
	l_priority integer default 1;
BEGIN
	foreach l_proposal in array in_proposals
	loop
		INSERT INTO proposals VALUES (DEFAULT, in_question_id, in_user_id, l_proposal, l_priority);
	l_priority = l_priority + 1;
	end loop;
END;
$$;

CREATE or replace FUNCTION question_open(in_user_id character varying, in_question character varying, in_proposals character varying[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE	
	l_question_id integer;
BEGIN
	select question_add(in_user_id, in_question) into l_question_id;
	perform proposals_add(in_user_id, l_question_id,  in_proposals);
	return l_question_id;
END;
$$;

CREATE or replace FUNCTION question_accept(in_user_id character varying, in_question_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	update usersquestions set accepted = TRUE where question_id = question_id and user_id = in_user_id;
END;
$$;

CREATE or replace FUNCTION question_deprecated(in_question character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	update questions set is_deprecated = TRUE, deprecated_date = NOW() where question_id = question_id;
END;
$$;

CREATE or replace FUNCTION question_is_accepted(in_question_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
		num_of_users integer;		
BEGIN
	select (user_id) into num_of_users from usersquestions where question_id = in_question_id and is_accepted = FALSE;
	if (num_of_users > 0) then
		return FALSE;
	else
		return TRUE;
	end if;
	
END;
$$;

CREATE or replace FUNCTION question_switch_fase(in_question_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	update questions set fase = 2 where question_id = in_question_id;
END;
$$;

CREATE or replace FUNCTION question_add_users(in_question_id integer, in_users_ids character varying[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	l_user_id character varying;
BEGIN
	foreach l_user_id in array in_users_ids
	loop
		INSERT INTO usersquestions VALUES (DEFAULT, in_question_id, l_user_id, FALSE);
	end loop;
END;
$$;

select t1.question_id, t2.revision, t2.fase
from usersquestions as t1 inner join questions as t2 
on t1.question_id = t2.question_id 
where t1.user_id = '622207834'
and t2.is_deprecated = FALSE;

select proposal_id, SUM(priority) as p from usersproposals 
where question_id = '48' 
and proposal_id in (select distinct(proposal_id) from usersproposals)
group by proposal_id
order by p desc;
