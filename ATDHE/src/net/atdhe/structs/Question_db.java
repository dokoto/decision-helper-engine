package net.atdhe.structs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Question_db
{
	public String question;
	public String user_id;
	public Date creation_date;
	public Date exp_date;
	public int phase_id;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
	public boolean is_phase_accepted = false;

	public Question_db(String Question, String User_id, Date Creation_date, Date Exp_date, int Phase_id, boolean is_phase_accepted)
	{
		this.question = Question;
		this.user_id = User_id;
		this.creation_date = Creation_date;
		this.exp_date = Exp_date;
		this.phase_id = Phase_id;
		this.is_phase_accepted = is_phase_accepted;
	}

	public Question_db(String Question, String User_id, String Creation_date, String Exp_date, int Revision, int Phase_id,
			boolean is_phase_accepted)
	{
		try
		{
			this.question = Question;
			this.user_id = User_id;
			this.creation_date = dateFormat.parse(Creation_date);
			this.exp_date = dateFormat.parse(Exp_date);
			this.phase_id = Phase_id;
			this.is_phase_accepted = is_phase_accepted;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Question_db()
	{
	}

	public Question_db(Questions_rev_com row)
	{
		this.creation_date = row.creation_date;
		this.phase_id = row.phase_id;
		this.is_phase_accepted = row.is_phase_accepted;
	}

	public Question_db(Questions_by_UserPhase row)
	{
		this.question = row.question;
		this.user_id = row.user_id;
		this.exp_date = row.exp_date;
		this.phase_id = row.phase_id;
		this.is_phase_accepted = row.is_phase_accepted;
		this.is_phase_accepted = row.is_phase_accepted;
	}
}
