#include "dhed.hpp"
#include "signalHandler.hpp"

int main(const int argc, const char** argv)
{
	try
	{
		SignalHandler signalHandler;
		// Register signal handler to handle kill signal
		signalHandler.setupSignalHandlers();

		dhe::dhed* DHEd = new dhe::dhed(argc, argv);
		if (!DHEd->parse_args()) return EXIT_FAILURE;
		if (!DHEd->parse_conf()) return EXIT_FAILURE;
		if (!DHEd->configure_serlet()) return EXIT_FAILURE;

		if (DHEd != NULL)
		{
			if (!DHEd->run())
			{
				if (DHEd)
					delete DHEd;
				return EXIT_FAILURE;
			}
			else
			{
				if (DHEd)
					delete DHEd;		
				return EXIT_SUCCESS;
			}
		} else return EXIT_FAILURE;		
	}
	catch (const std::exception& e) 
	{
		std::cerr << "[" << __FUNCTION__ << "] Unexpect exception : " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch(...)
	{
		std::cerr << "[" << __FUNCTION__ << "] Unknowed exception" << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


