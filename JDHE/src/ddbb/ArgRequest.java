package ddbb;

public class ArgRequest
{
	public enum ArgType {SIMPLE, MULTI};
	private ArgType arg_type;
	private String key;
	
	
	public ArgRequest(String key, ArgType argtype)
	{
		this.key = key;
		this.arg_type = argtype;
	}

	
	public ArgType arg_type()
	{
		return this.arg_type;
	}
	
	public String key()
	{
		return this.key;
	}
	
}
