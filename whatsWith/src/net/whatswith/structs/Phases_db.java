package net.whatswith.structs;

import org.json.JSONObject;

public class Phases_db
{
	public int sort_index;
	public String phase;
	public String description;
	public String color;	
		
	public Phases_db(int Sort_index, String Phase, String Description, String Color)
	{		
		sort_index = Sort_index;
		phase = Phase;
		description = Description;
		color = Color;
	}
	
	public Phases_db(JSONObject ojb)
	{
		try
		{
			sort_index = ojb.getInt("SORT_INDEX");
			phase = ojb.getString("PHASE");
			description = ojb.getString("DESCRIPTION");
			color = ojb.getString("COLOR");
		} catch (Exception e)
		{ 
			e.printStackTrace();
		}
	}	
	
	public boolean compareTo(Phases_db objB)
	{

		if (this.sort_index != objB.sort_index)
			return false;
		if (this.phase.compareTo(objB.phase) != 0)
			return false;
		if (this.description.compareTo(objB.description) != 0)
			return false;
		if (this.color.compareTo(objB.color) != 0)
			return false;
		
		return true;
	}
	
}
