#!/bin/bash

set -e
set -u

# Set these environmental variables to override them,
# but they have safe defaults.
export PGHOST=${PGHOST-localhost}
export PGPORT=${PGPORT-5432}
export PGDATABASE=${PGDATABASE-dhe}
export PGUSER=${PGUSER-developer}
export PGPASSWORD=${PGPASSWORD-5r4e3w2q}

RUN_PSQL="psql -X --set AUTOCOMMIT=off --set ON_ERROR_STOP=on "

${RUN_PSQL} <<SQL
	select * from update_delay_between_phases();
	select * from update_deprecated_questions();
SQL
