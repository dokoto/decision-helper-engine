#ifndef _IPCHECKER_HPP
#define _IPCHECKER_HPP

#include <assert.h>
#include <string>
#include <map>

extern "C"
{
#include "pthread.h"
}

namespace dhe
{
class ipchecker
{
	private:
		int max_password_fails;		
		pthread_mutex_t mutex;
		std::map<std::string, int> Banned_ips;
		
        ipchecker& operator=(const ipchecker&) { return *this; }
		ipchecker(const ipchecker&) {}

	public:

		ipchecker(void);
		ipchecker(const int MaxPasswordFails);
		~ipchecker(void);
		void set(const int MaxPasswordFails);
		bool remove(const std::string& ip);
		bool is_IpBanned(const std::string& ip);
		void IpBannedControl(const bool is_auth, const std::string& ip);

};
}
#endif // _IPCHECKER_HPP

