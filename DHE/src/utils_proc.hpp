#ifndef _UTILS_PROC_HPP
#define _UTILS_PROC_HPP

#include <vector>
#include <map>

#include "glob_structs.hpp"

namespace dhe
{


class calc_better_proposal
{
private:
	std::vector< std::vector<int> > matrix;
	//std::map<unsigned, std::string> map_user_id;
	std::map<unsigned, unsigned> map_proposal_id;

	int calc_distancia(const std::vector< std::vector<int> >& matrix, const std::vector<int>& repRows); // Return num proposal or -1 

public:
	~calc_better_proposal();
	calc_better_proposal(int num_of_proposals, int num_of_users);
	calc_better_proposal(const std::vector<calc_better_proposal_t>& resulset);	
	int get(void); // Return num proposal or -1 

};
}

#endif // _UTILS_PROC_HPP


