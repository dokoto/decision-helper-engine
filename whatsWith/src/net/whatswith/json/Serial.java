package net.whatswith.json;

import java.lang.reflect.Type;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class Serial<T>
{
	private T Obj;
	private Type TypeObj;
	private SharedPreferences Prefs;
	private SharedPreferences.Editor Editor;
	private String Key;
	private Activity App;

	public Serial(final Activity app_, final String Key_, T Obj_)
	{
		Obj = Obj_;
		TypeObj = new TypeToken<T>()
		{
		}.getType();
		Key = Key_;
		App = app_;
		Prefs = App.getSharedPreferences(App.getApplicationInfo().name, Context.MODE_PRIVATE);
		Editor = Prefs.edit();
	}

	public void Delete()
	{
		Prefs.edit().clear().commit();
	}
	
	public void Delete(String Key)
	{
		Prefs.edit().remove(Key).commit();
	}
	
	public boolean Load()
	{
		try
		{
			JsonParser parser = new JsonParser();
			String gsonString = Prefs.getString(Key, null);
			if (gsonString.isEmpty())
				return false;
			parser.parse(gsonString);
			Obj = new Gson().fromJson(gsonString, TypeObj);
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}

	public void Save()
	{
		String gsonString = new Gson().toJson(Obj);
		Editor.putString(Key, gsonString);
		Editor.commit();
	}
}
