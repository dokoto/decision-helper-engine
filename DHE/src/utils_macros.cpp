#include "utils_macros.hpp"
#include "utils_rgx.hpp"
#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>

bool utils_macros_log_to_file = false;
char utils_macros_log_path[UTIL_MACROS_MAX_LENGHT_PATH];
unsigned utils_macros_log_max_fize = 0;
FILE* utils_macros_fd_log = NULL;

std::string GetCurrentDateTime(void)
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	strftime (buffer, 80, "%Y-%m-%d %H:%M:%S", timeinfo);

	return std::string(buffer);
}
void fb_log_close(void)
{
	if (NULL != utils_macros_fd_log)
	{
		fclose(utils_macros_fd_log);
		utils_macros_fd_log = NULL;
	}
}

long GetFileSize(const char* filename)
{
	struct stat stat_buf;
	int rc = stat(filename, &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}

long FdGetFileSize(int fd)
{
	struct stat stat_buf;
	int rc = fstat(fd, &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}


void fd_log_handle(void)
{
	static int checkSize = 0;	
	if (utils_macros_log_to_file)
	{
		if (NULL == utils_macros_fd_log)
		{
			utils_macros_fd_log = fopen(utils_macros_log_path, "a+");
			if (NULL == utils_macros_fd_log)
			{
				utils_macros_log_to_file = false;
				PERR("Can't open the log file, switching to stdout");
				return;
			}
		}
		if (0 == checkSize || ++checkSize > 100)
		{
			checkSize = 1;
			off_t size = GetFileSize(  utils_macros_log_path );
			if (size > utils_macros_log_max_fize)
			{
				fclose(utils_macros_fd_log);
				utils_macros_fd_log = NULL;
				char new_name[UTIL_MACROS_MAX_LENGHT_PATH];
				memset(new_name, 0, UTIL_MACROS_MAX_LENGHT_PATH);
				std::string date;
				rgx::RemplaceAll("([-|:|\\s])", GetCurrentDateTime().c_str(), "_", date);
				sprintf(new_name, "%s_%s.log", utils_macros_log_path, date.c_str());
				bool resultRen = false;
#ifdef _MSC_VER
				try
				{
					convCharToWChar oldP(utils_macros_log_path);
					convCharToWChar newP(new_name);					
					resultRen = ( MoveFile(oldP.ValueLPCTSTR(), newP.ValueLPCTSTR()) )? true : false;
					if (!resultRen)
						PrintWinSysError(TEXT(__FUNCTION__), true);			
				} catch(...) { resultRen = false; }
#else
				resultRen = ( rename(utils_macros_log_path, new_name) )? true : false;
#endif			
				if (!resultRen)
					PERR("Can't rename log file, Open old log file to continue adding it");
				utils_macros_fd_log = fopen(utils_macros_log_path, "a+");
				if (NULL == utils_macros_fd_log)
				{
					utils_macros_log_to_file = false;
					PERR("Can't open the log file, switching to stdout");
					utils_macros_fd_log = NULL;
				}
			}		
		}
	}
	else
		utils_macros_fd_log = NULL;
}


#ifdef _MSC_VER

	void PrintWinSysError(LPTSTR lpszFunction, const bool forceStdOut)
	{
		LPVOID lpMsgBuf;
		LPVOID lpDisplayBuf;
		DWORD dw = GetLastError(); 

		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0, NULL );

		lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
			(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
		StringCchPrintf((LPTSTR)lpDisplayBuf, 
			LocalSize(lpDisplayBuf) / sizeof(TCHAR),
			TEXT("%s failed with error %d: %s"), 
			lpszFunction, dw, lpMsgBuf); 
		int cSize = WideCharToMultiByte (CP_ACP, 0, (LPCTSTR)lpDisplayBuf, wcslen((LPCTSTR)lpDisplayBuf), NULL, 0, NULL, NULL);
		char* tmpCad = NULL;
		tmpCad = new char[static_cast<size_t>(cSize)+10];
		if (tmpCad != NULL)
		{
			memset(tmpCad, 0, static_cast<size_t>(cSize)+10);
			WideCharToMultiByte (CP_ACP, 0, (LPCTSTR)lpDisplayBuf, wcslen((LPCTSTR)lpDisplayBuf),
				reinterpret_cast<char*>(&tmpCad[0]), cSize, NULL, NULL);
			std::string output(tmpCad);
			delete[] tmpCad;
			rgx::RemplaceAll("([\\n|\\r])", output.c_str(), " ", output);
			for (size_t i = 0; i < output.size(); i += 50)
			{
				if (!forceStdOut)
				{
					ERR_T("WIN - %s[%d] %s", __FUNCTION__, i, output.substr(i, 50).c_str());		
				}
				else
				{
					PERR_T("WIN - %s[%d] %s", __FUNCTION__, i, output.substr(i, 50).c_str());		
				}
			}
		}
		LocalFree(lpMsgBuf);
		LocalFree(lpDisplayBuf);
	}

static unsigned mmtime = GetTickCount();

unsigned get_mmtime (void)
{
	return mmtime;
}

float msToSec(const unsigned time_ms)
{
	float ftime = static_cast<float>(time_ms);
	return  ftime / 1000;
}

unsigned CalcElapsetTime(const unsigned time_ms)
{ 
	return  GetTickCount() - time_ms; 
}

#else
static timeval mmtime = StarTimeCount();

timeval get_mmtime (void)
{
	return mmtime;
}

double usecToSec(const double time_usec)
{ 
	return  time_usec / 1000000.0; 
}

double CalcElapsetTime(const timeval t1)
{
	timeval t2;
	gettimeofday(&t2, NULL);
	double diff = static_cast<float>(t2.tv_usec - t1.tv_usec);
	return diff;
}

const timeval StarTimeCount(void)
{ 
	timeval t1; 
	gettimeofday(&t1, NULL); 
	return t1; 
}

#endif


