package net.whatswith.screens.proposals;

import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.contrib.TouchInterceptor;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Return;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Set_proposals_preference extends ListActivity
{
	private class tv_struct
	{
		public tv_struct(int id, String value)
		{
			this.id = id;
			this.value = value;
		}

		public int id;
		public String value;
	}

	private ArrayList<tv_struct> ListValues;
	private TouchInterceptor mList;
	private ListProposalsAdapter ProposalsAdapter;
	private int QuestionId;
	private int PhaseId;
	private boolean view_mode = false;

	private TouchInterceptor.DropListener mDropListener = new net.whatswith.contrib.TouchInterceptor.DropListener()
	{

		public void drop(int from, int to)
		{
			if (view_mode == false)
			{
				tv_struct tmp = ListValues.get(from);
				ListValues.remove(from);
				if (from < to)
					ListValues.add(to - 1, tmp);
				else
					ListValues.add(to, tmp);
				ProposalsAdapter.notifyDataSetChanged();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_set_proposals_preference);

		try
		{
			Bundle b = getIntent().getExtras();
			QuestionId = b.getInt("QuestionId");
			PhaseId = b.getInt("PhaseId");
			if (Main.Nexus.Storage.questions.get(QuestionId).is_phase_accepted == true)
				view_mode = true;
			setTitle(Main.Nexus.Storage.questions.get(QuestionId).question);
			InitSetProposalsPreference();

		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		if (view_mode == false)
			getMenuInflater().inflate(R.menu.menu_set_proposals_preference, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.menu_set_proposals_preference_save)
			save_preference();
		else
			Main.Nexus.MenuSelector(this, item);

		return true;
	}

	void InitSetProposalsPreference()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(Set_proposals_preference.this);
				pd.setTitle("Recieving Proposals...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = Commands.ProposalsSyncByQuestion(Main.Nexus.login(), QuestionId);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{				
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
				{
					ListValues = new ArrayList<tv_struct>();
					for (int i = 0; i < Main.Nexus.Storage.proposals.size(); ++i)
					{
						int key = Main.Nexus.Storage.proposals.keyAt(i);
						ListValues.add(new tv_struct(key, Main.Nexus.Storage.proposals.get(key).proposal));
					}
					ProposalsAdapter = new ListProposalsAdapter(Set_proposals_preference.this, ListValues);
					setListAdapter(ProposalsAdapter);
					mList = (TouchInterceptor) getListView();
					mList.setDropListener(mDropListener);
					registerForContextMenu(mList);
				}
				pd.dismiss();
			}			
		};
		task.execute((Void[]) null);
	}

	private void add_preference_proposals_order(final int[] proposals_ids)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return retA;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				retA = Commands.AddProposalsPreferenceOrder(Main.Nexus.login(), QuestionId, proposals_ids);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (retA.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + retA.message);
					Toast.makeText(getApplicationContext(), retA.message, Toast.LENGTH_SHORT).show();
				} else
				{
					Commands.Async_Question_Accept(Main.Nexus.login(), Set_proposals_preference.this, null, true, QuestionId);
					Toast.makeText(getApplicationContext(), "Preference Order save successful", Toast.LENGTH_SHORT).show();
				}
			}
		};
		task.execute((Void[]) null);
	}

	void save_preference()
	{
		int proposals[] = new int[ListValues.size()];
		for (int i = 0; i < ListValues.size(); i++)
			proposals[i] = ListValues.get(i).id;
		add_preference_proposals_order(proposals);
	}

	
	private static class ViewHolder
	{
		public TextView tv_proposal;
	}
	
	public class ListProposalsAdapter extends ArrayAdapter<tv_struct>
	{
		private final Context context;
		ArrayList<tv_struct> rows;
		ViewHolder holder;

		public ListProposalsAdapter(Context context, ArrayList<tv_struct> Rows)
		{
			super(context, R.layout.layo_row_set_proposals_preference, Rows);
			this.context = context;
			this.rows = Rows;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_set_proposals_preference, parent, false);
				holder = new ViewHolder();
				holder.tv_proposal = (TextView) rowView.findViewById(R.id.layo_row_set_proposals_preference_tv_proposal);

				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();
			holder.tv_proposal.setText(rows.get(position).value);

			return rowView;
		}
	}

}
