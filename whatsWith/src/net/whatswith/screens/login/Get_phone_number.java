package net.whatswith.screens.login;

import java.util.Random;
import net.whatswith.R;
import net.whatswith.screens.main.Main;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class Get_phone_number extends Activity
{
	private final int low = 100;
	private final int high = 1000;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		// Para poner a pantalla completa la aplicaciones
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.layo_get_phone_number);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layo_title_get_phone_number);
		
		findViewById(R.id.layo_get_phone_number_bt_continue).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				try
				{
					final EditText phone = (EditText) findViewById(R.id.layo_get_phone_number_et_phone_number);
					if (phone.getText().toString().isEmpty())
					{
						Toast.makeText(getApplicationContext(), "Phone number can't be empty !", Toast.LENGTH_LONG).show();
						return;
					}
					Random r = new Random();
					int code = r.nextInt(high - low) + low;
					if (Main.Nexus.Storage.crazy_mode == false)
					{
						SmsManager smsManager = SmsManager.getDefault();
						smsManager.sendTextMessage(phone.getText().toString(), null, "Whats With CODE: " +  String.valueOf(code), null, null);
						Toast.makeText(getApplicationContext(), "SMS has sent success to your cell phone!", Toast.LENGTH_LONG).show();                                        
					}	
					else
						Toast.makeText(getApplicationContext(), "Youy code is : " + code, Toast.LENGTH_LONG).show();
					
					Intent intent = new Intent(Get_phone_number.this, Check_phone_number.class);
            		Bundle b = new Bundle();
            		b.putInt("Code", code);
            		b.putString("Phone", phone.getText().toString());
            		intent.putExtras(b);
            		startActivity(intent);
            		Get_phone_number.this.finish();                   
				}
				catch( Exception e)
				{
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
					Get_phone_number.this.finish();      
				}
			}
		});
		
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (true == Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_debug_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		
		Main.Nexus.MenuSelector(this, item);
		return true;
	}
	
}
