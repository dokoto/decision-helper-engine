#ifndef _GLOB_STRUCTS_HPP_
#define _GLOB_STRUCTS_HPP_

#include <cstring>
#include <string>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif
namespace dhe
{
	/*
	template < const unsigned sz >
	class podChar_t
	{
	public:
		podChar_t(const char* tx) { memcpy(ptr, tx, sz);  }
		podChar_t(const podChar_t& v) { memcpy(ptr, v.ptr, sz); }
		virtual const char* Value(void) const { return ptr; }
	private:
		char ptr[sz];
	};


	class char15_t : public podChar_t<15>
	{
	public: char15_t(const char* tx) : podChar_t(tx) {};
	};

	class char10_t : public podChar_t<10>
	{
	public: char10_t(const char* tx) : podChar_t(tx) {};
	};

	class char30_t : public podChar_t<30>
	{
	public: char30_t(const char* tx) : podChar_t(tx) {};
	};

	class char100_t : public podChar_t<100> 
	{
	public: char100_t(const char* tx) : podChar_t(tx) {};
	};


	class password_t : public podChar_t<80>
	{
	public: password_t(const char* tx) : podChar_t(tx) {};
	};

	class user_id_t : public podChar_t<50>
	{
	public: user_id_t(const char* tx) : podChar_t(tx) {};
	};

	class timestamp_t : public podChar_t<30>
	{
	public: timestamp_t(const char* tx) : podChar_t(tx) {};
	};
	*/
	struct user_array_exist_t
	{
		user_array_exist_t(const std::string& user_id_, const std::string& sign_date_)
			:
			user_id(user_id_),
			sign_date(sign_date_)
		{}

		std::string user_id;
		std::string sign_date;
	};

	struct sync_get_sort_proposals_of_question_t
	{
		sync_get_sort_proposals_of_question_t(const unsigned proposal_id_, const unsigned question_id_, const std::string& user_id_, 
			const std::string& proposal_)
			:
			proposal_id(proposal_id_),
			question_id(question_id_),
			user_id(user_id_),
			proposal(proposal_) {}

		unsigned proposal_id;
		unsigned question_id; 
		std::string user_id;
		std::string proposal;
	};


	struct sync_get_questions_by_user_phase_t
	{
		sync_get_questions_by_user_phase_t(const unsigned question_id_, const std::string& question_, const std::string& user_id_, 
			const std::string& exp_date_, const unsigned phase_id_, const std::string& is_phase_accepted_)
			:
		question_id(question_id_),
			question(question_),
			user_id(user_id_),
			exp_date(exp_date_),
			phase_id(phase_id_),
			is_phase_accepted(is_phase_accepted_){}

		unsigned question_id;
		std::string question; 
		std::string user_id; 
		std::string exp_date;
		unsigned phase_id;
		std::string is_phase_accepted;
	};

	struct sync_get_questions_revs_of_user_t
	{
		sync_get_questions_revs_of_user_t(const unsigned question_id_, const std::string& creation_date_,  
			const unsigned phase_id_, const std::string& is_phase_accepted_)
			:
			question_id(question_id_),    
			creation_date(creation_date_),
			phase_id(phase_id_),
			is_phase_accepted(is_phase_accepted_){}

		unsigned question_id;
		std::string creation_date;
		unsigned phase_id;
		std::string is_phase_accepted;
	};

	struct sync_get_phase_sort_t
	{
		sync_get_phase_sort_t(const unsigned phase_id_, const unsigned sort_index_, const std::string& phase_, const std::string& description_,
			const std::string& color_)
			: 
			phase_id(phase_id_), 
			sort_index(sort_index_),
			phase(phase_),
			description(description_),
			color(color_)
		{}
		unsigned phase_id; 
		unsigned sort_index;
		std::string phase;
		std::string description;
		std::string color;
	};


	struct sync_get_questions_sort_t
	{
		sync_get_questions_sort_t(const unsigned question_id_, const std::string& question_text_,
			const std::string& user_id_, const std::string& create_date_, const std::string& deprecated_date_,
			const unsigned revision_, const unsigned fase_, const std::string& proposal_text_)
			:
			question_id(question_id_),
			question_text(question_text_),
			user_id(user_id_),
			creation_date(create_date_),
			deprecated_date(deprecated_date_),
			revision(revision_),
			fase(fase_),
			proposal_text(proposal_text_)
		{}

		unsigned question_id;	
		std::string question_text;
		std::string user_id;
		std::string creation_date;
		std::string deprecated_date;
		unsigned revision;
		unsigned fase;
		std::string proposal_text;
	};


	struct calc_better_proposal_t
	{	
		calc_better_proposal_t(const unsigned proposal_id_, const std::string& user_id_, const unsigned priority_) 
			: 
			proposal_id(proposal_id_), 
			user_id(user_id_),
			priority(priority_) {}

		unsigned proposal_id; 
		std::string user_id;
		unsigned priority;
	};

}

#endif //_GLOB_STRUCTS_HPP_
