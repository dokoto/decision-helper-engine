#  cmake -DCMAKE_BUILD_TYPE=Debug ../../../.
#  cmake -DCMAKE_BUILD_TYPE=Release ../../../.
#  cmake -DCMAKE_BUILD_TYPE=Debug ../../../. -G Xcode
#  cmake -DCMAKE_BUILD_TYPE=Release ../../../. -G Xcode

SET(CMAKE_LEGACY_CYGWIN_WIN32 0)

project(DECISION-HELPER-ENGINE-DAEMON)
cmake_minimum_required( VERSION 2.6 FATAL_ERROR )

SET(TARGET "dhed")
 
MESSAGE( "SCAN DE SISTEMA" )
MESSAGE( "==========================================================================" )
MESSAGE( STATUS "SISTEMA ACTUAL              : "${CMAKE_SYSTEM_NAME} )
MESSAGE( STATUS "MODO                        : "${CMAKE_BUILD_TYPE} )
MESSAGE( STATUS "CMAKE_COMPILER_IS_GNUCXX    : "${CMAKE_COMPILER_IS_GNUCXX} )
MESSAGE( STATUS "UNIX                        : "${UNIX} )
MESSAGE( STATUS "WIN32                       : "${WIN32} )
MESSAGE( STATUS "APPLE                       : "${APPLE} )
MESSAGE( STATUS "MINGW                       : "${MINGW} )
MESSAGE( STATUS "MSYS                        : "${MSYS} )
MESSAGE( STATUS "CYGWIN                      : "${CYGWIN} )
MESSAGE( STATUS "BORLAND                     : "${BORLAND} )
MESSAGE( STATUS "WATCOM                      : "${WATCOM} )
MESSAGE( "==========================================================================" )

add_executable( ${TARGET}
../../../src/dbConn.cpp
../../../src/dbConn.hpp
../../../src/dhed.cpp
../../../src/dhed.hpp
../../../src/main.cpp
../../../src/sslConn.cpp
../../../src/sslConn.hpp
../../../src/utils_macros.cpp
../../../src/utils_macros.hpp
../../../src/utils_rgx.cpp
../../../src/utils_rgx.hpp
../../../src/ThreadAcctions.hpp
../../../src/ThreadAcctions.cpp
../../../src/IPChecker.cpp
../../../src/IPChecker.hpp
../../../src/signalHandler.cpp
../../../src/signalHandler.hpp
../../../src/glob_structs.hpp
../../../src/utils_proc.cpp
../../../src/utils_proc.hpp
../../../src/contrib/cJSON/cJSON.c
../../../src/contrib/cJSON/cJSON.h
../../../src/contrib/utf8.h
../../../src/contrib/utf8/checked.h
../../../src/contrib/utf8/core.h
../../../src/contrib/utf8/unchecked.h)
								
IF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
 
	  MESSAGE( "SISTEMA: Apple OS X" )
	  MESSAGE( "==========================================================================" )	
	  IF(${CMAKE_BUILD_TYPE} MATCHES "Release")
		  MESSAGE( "Compilando en MODO RELEASE" )
		  add_definitions("-O3 -Wall -Wextra -pedantic -W -Wno-variadic-macros") 
	  ELSE()
		  MESSAGE( "Compilando en MODO DEBUG" )
		  add_definitions("-g -O0 -Wall -Wextra -pedantic -W -Wno-variadic-macros") 
	  ENDIF()        
       
      FIND_PATH(POSTGRESQL_INCLUDE libpq-fe.h "/usr/include")	
      FIND_LIBRARY(POSTGRESQL_LIBRARY NAMES pq libpq PATHS /usr/lib)

	  FIND_PATH(SSL_INCLUDE ssl.h "/opt/local/include/openssl")	 
	  FIND_LIBRARY(SSL_LIBRARY NAMES ssl libssl PATHS /opt/local/lib) 
	  FIND_LIBRARY(CRYPTO_LIBRARY NAMES crypto libcrypto PATHS /opt/local/lib) 
	  FIND_LIBRARY(GNUTLS_XSSL_LIBRARY NAMES gnutls-xssl libgnutls-xssl PATHS /opt/local/lib) 
      FIND_LIBRARY(GNUTLS_LIBRARY NAMES gnutls-openssl libgnutls-openssl PATHS /opt/local/lib)

	  FIND_PATH(PTHREAD_INCLUDE pthread.h "/usr/include")	
	  FIND_LIBRARY(PTHREAD_LIBRARY NAMES pthread libpthread PATHS /usr/lib)
	  
	  FIND_PATH(PCRE_INCLUDE pcre.h "/opt/local/include")	  
	  FIND_LIBRARY(PCRE_LIBRARY NAMES pcre libpcre PATHS /opt/local/lib)

	  FIND_PATH(PCRECPP_INCLUDE pcrecpp.h "/opt/local/include")	  
	  FIND_LIBRARY( PCRECPP_LIBRARY NAMES pcrecpp libpcrecpp PATHS /opt/local/lib)

	  include_directories ( ${POSTGRESQL_INCLUDE} ${SSL_INCLUDE}  ${PTHREAD_INCLUDE} ${PCRE_INCLUDE}
	  ${PCRECPP_INCLUDE})
	  target_link_libraries ( ${TARGET} ${POSTGRESQL_LIBRARY} ${SSL_LIBRARY} ${CRYPTO_LIBRARY} ${GNUTLS_XSSL_LIBRARY} ${GNUTLS_LIBRARY} ${PTHREAD_LIBRARY} ${PCRE_LIBRARY} ${PCRECPP_LIBRARY}) 
	  
ENDIF()
