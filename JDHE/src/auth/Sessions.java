package auth;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Sessions
{
	private HttpSession session;
	private final static String user_key = "USER_ID";
	private final int InactiveTime = 5;
	public enum SessionReturn {SESSION_HAS_EXPIRED, SESSION_VALID, SESSION_VALID_USER_NO_FOUND};

	public SessionReturn getTicket(HttpServletRequest request)
	{
		if (request.getRequestedSessionId() != null && !request.isRequestedSessionIdValid())
		{
			return SessionReturn.SESSION_HAS_EXPIRED;
		}
		else
		{
			session = request.getSession();
			return (session.getAttribute(user_key) != null) ? SessionReturn.SESSION_VALID : SessionReturn.SESSION_VALID_USER_NO_FOUND;
		}
	}

	public void getNewTicket(HttpServletRequest request, HttpServletResponse response, String user)
	{
		session = request.getSession();
		session.setAttribute(user_key, user);
		session.setMaxInactiveInterval(InactiveTime * 60);
		Cookie userName = new Cookie(user_key, user);
		userName.setMaxAge(InactiveTime * 60);
		response.addCookie(userName);
	}
}
