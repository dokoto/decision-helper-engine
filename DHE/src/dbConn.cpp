#include "dbConn.hpp"
#include "utils_macros.hpp"
#include "utils_rgx.hpp"
#include <sstream>

extern "C"
{
#include "pthread.h"
}

namespace dhe
{

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

	PGconn* pgConn = NULL;
	pthread_mutex_t db_mutex;	

	void closePgDB(void)
	{
		PQfinish(pgConn);
	}

	bool init_db_conn(const std::string& db_host, const std::string& db_name, const std::string& db_user, const std::string& db_password)
	{
        pthread_mutex_init(&db_mutex, NULL);
		return ( (pgConn = openPgDB(db_host, db_name, db_user, db_password)) != NULL) ? true : false;
	}

	PGconn* openPgDB(const std::string& db_host, const std::string& db_name, const std::string& db_user, const std::string& db_password)
	{
		PGconn* pgConn = PQconnectdb( dbDataToString(db_host.c_str(), db_name.c_str(), db_user.c_str(), db_password.c_str()).c_str() );
		if (CONNECTION_OK != PQstatus(pgConn))
		{
			ERR(PQerrorMessage(pgConn));
			return NULL;
		}

		return pgConn;
	}

	std::string dbDataToString(const std::string& host, const std::string& name, const std::string& user, const std::string& password)
	{
		std::stringstream ss;
		ss << "host='" << host << "' " << "dbname='" << name << "' " << "user='" << user << "' " << "password='" << password << "'";
		return ss.str();
	}

	bool check_user_id(const std::string& user_id, std::string& error)
	{
		if (rgx::CheckPattern("(^[telTEL0-9:+-]{10,50}$)", user_id.c_str()))
			return true;
		else
		{
			error.append("USER_ID must have 10-50 character of lenght, and only can contain 't', 'e', 'l', '+', '-' as chars, "
				"and numbers. As example : tel:+34-600-00-00-01");
			return false;
		}

	}

	bool check_password(const std::string& password, std::string& error)
	{
		if (rgx::CheckPattern("(^[a-zA-Z0-9\\._]{5,20}$)", password.c_str()))
			return true;
		else
		{
			error.append("PASSWORD must have 5-20 character of lenght, and only can contain 'a-zA-Z' '_', '.' as chars, "
				"and numbers. As example : Password.0001");
			return false;
		}
	}

	bool check_timestamp(const std::string& timestamp, std::string& error)
	{
		if (rgx::CheckPattern("(^[0-9\\s\\.:-]{10,30}$)", timestamp.c_str()))
			return true;
		else
		{
			error.append("TIMESTAMP must have 10-30 character of lenght, and only can contain '-', '.', ':', ' ' as chars, "
				"and numbers. As example : 2013-09-17 18:23:48.872");
			return false;
		}
	}

	bool check_string_100(const std::string& string_100, std::string& error)
	{
		if (!rgx::CheckPattern("(^[a-zA-Z0-9\\W]{1,100}$)", string_100.c_str()))
		{
			error.append("TIMESTAMP must have 1-100 character of lenght, and can contain chars, "
				"and numbers. As example : 'ñ' is a spanish char.");
			return false;
		}
		
		return true;
	}

	PGresult* exec(const char* query, std::string& error, bool debug = true)
	{
        NOT_USED(debug);
		int rc = 0;		

		rc = pthread_mutex_lock(&db_mutex);		
		PGresult* pgResult = NULL;			
		pgResult = PQexec(pgConn, query);		
		if (PGRES_TUPLES_OK != PQresultStatus(pgResult))
		{
            error.append(PQerrorMessage(pgConn));
			PQclear(pgResult);
			rc = pthread_mutex_unlock(&db_mutex);
			return NULL;
		}
		else
		{
			rc = pthread_mutex_unlock(&db_mutex);
			return pgResult;
		}
	}

	bool user_add(const std::string& user_id, const std::string& passwd, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		if (!check_password(passwd, error)) return false;
		const char* const query = "SELECT user_add('%s', '%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str(), passwd.c_str());
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;		
	}

	bool user_password_exist(const std::string& user_id, const std::string& passwd, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		if (!check_password(passwd, error)) return false;
		const char* const query = "SELECT user_password_exist('%s', '%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str(), passwd.c_str());
		PGresult* pgResult = NULL;   
		if (NULL == (pgResult = exec(buffer, error)))
			return false;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			if (strncmp("t", result, 1) != 0) { PQclear(pgResult); return false; }
			PQclear(pgResult);
		}
		return true;
	}

    
	bool password_change(const std::string& user_id, const std::string& passwd_old, const std::string& passwd_new, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		if (!check_password(passwd_old, error)) return false;
		if (!check_password(passwd_new, error)) return false;
		const char* const query = "SELECT password_change('%s', '%s', '%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str(), passwd_old.c_str(), passwd_new.c_str());
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}
    
	bool user_exist(const std::string& user_id, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		const char* const query = "SELECT user_exist('%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str());
		PGresult* pgResult = NULL;   
		if (NULL == (pgResult = exec(buffer, error)))
			return false;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			if (strncmp("t", result, 1) != 0) { PQclear(pgResult); return false; }
			PQclear(pgResult);
		}
		return true;
	}

	bool user_array_exist(const std::vector<std::string>& users_ids, std::vector<user_array_exist_t>& result, std::string& error)
	{
		std::stringstream ss_query;
		ss_query << "SELECT * from user_array_exist(ARRAY[";
		for(std::vector<std::string>::const_iterator it = users_ids.begin(); it != users_ids.end(); it++)
		{
			if (!check_user_id(*it, error)) return false;		
			ss_query << "'" << it->c_str() << "'";
			if ((it+1) != users_ids.end())
				ss_query << ", ";
		}
		ss_query << "])";
		PGresult* pgResult = NULL;
		if (NULL == (pgResult = exec(ss_query.str().c_str(), error)))
			return false;
		else
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back
                (
                    user_array_exist_t
                    (
                     PQgetvalue(pgResult, irow, 0),
					 PQgetvalue(pgResult, irow, 1)		 
                     )
                );

			}
			PQclear(pgResult);
			return true;
		}
	}

	bool user_lock(const std::string& user_id, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		const char* const query = "SELECT user_lock('%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str());
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	bool user_unlock(const std::string& user_id, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		const char* const query = "SELECT user_unlock('%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str());
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	bool debug_are_there_questions_in_phase(const int sort_index, std::string& error)
	{
		if (sort_index > 3 || sort_index < 1) return false;
		const char* const query = "SELECT debug_are_there_questions_in_phase('%d')";
		char buffer[200];
		sprintf(buffer, query, sort_index);
		PGresult* pgResult = NULL;
		if (NULL == (pgResult = exec(buffer, error)))
			return false;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			if (strncmp("t", result, 1) != 0) { PQclear(pgResult); return false; }
			PQclear(pgResult);
		}
		return true;
	}

	bool question_accept(const std::string& user_id, const unsigned question_id, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		const char* const query = "SELECT question_accept('%s', '%d')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str(), question_id);
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	bool question_deprecated(const unsigned question_id, std::string& error)
	{
		const char* const query = "SELECT question_deprecated('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id);		

		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	bool question_switch_fase(const unsigned question_id, std::string& error)
	{
		const char* const query = "SELECT question_switch_fase('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id);
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	int question_get_last_rev(const unsigned question_id, std::string& error)
	{
		const char* const query = "SELECT question_get_last_rev('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id);
		PGresult* pgResult = NULL;
		if (NULL == (pgResult = exec(buffer, error)))
			return -1;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			return atoi(result);
			PQclear(pgResult);
		}
	}

	bool question_is_accepted(const unsigned question_id, std::string& error)
	{
		const char* const query = "SELECT question_is_accepted('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id);
		PGresult* pgResult = NULL;
		if (NULL == (pgResult = exec(buffer, error)))
			return false;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			if (strncmp("t", result, 1) != 0) { PQclear(pgResult); return false; }
			PQclear(pgResult);
		}
		return true;
	}

	int proposals_add(const std::string& user_id, const unsigned question_id,  const std::vector<std::string>& proposals, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		std::stringstream ss_query;
		ss_query << "SELECT proposals_add('" << user_id.c_str() << "', '" << question_id << "', ARRAY[";
		for(std::vector<std::string>::const_iterator it = proposals.begin(); it != proposals.end(); it++)
		{
			if (!check_string_100(*it, error)) return false;
			ss_query << "'" << it->c_str() << "'";
			if ((it+1) != proposals.end())
				ss_query << ", ";
		}
		ss_query << "])";
		PGresult* pgResult = NULL;
		if (NULL == (pgResult = exec(ss_query.str().c_str(), error)))
			return -1;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			int proposals_id = atoi(result);
			PQclear(pgResult);
			return proposals_id;
		}

	}

	void sync_get_phase_sort(std::vector<sync_get_phase_sort_t>& result, std::string& error)
	{
		const char* const query = "SELECT * FROM sync_get_phase_sort()";
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(query, error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back
                (
                    sync_get_phase_sort_t
                    (
                     atoi( PQgetvalue(pgResult, irow, 0)),
					 atoi( PQgetvalue(pgResult, irow, 1)),
                     PQgetvalue(pgResult, irow, 2),
					 PQgetvalue(pgResult, irow, 3),
					 PQgetvalue(pgResult, irow, 4)					 
                     )
                );

			}
			PQclear(pgResult);
		}
	}

	void sync_get_users_of_question(const int question_id, std::vector<std::string>& result, std::string& error)
	{
		const char* const query = "SELECT * FROM sync_get_users_of_question('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id );
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)			
				result.push_back( std::string(PQgetvalue(pgResult, irow, 0)));
			
			PQclear(pgResult);
		}	  
	}
	
	void sync_get_sort_proposals_of_question(const unsigned question_id, 
		std::vector<sync_get_sort_proposals_of_question_t>& result, std::string& error)
	{
		const char* const query = "SELECT * FROM sync_get_sort_proposals_of_question('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id );
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back
                (
                    sync_get_sort_proposals_of_question_t
                    (
                     atoi( PQgetvalue(pgResult, irow, 0)),
					 atoi( PQgetvalue(pgResult, irow, 1)),
                     std::string(PQgetvalue(pgResult, irow, 2)),
					 std::string(PQgetvalue(pgResult, irow, 3))
                     )
                );

			}
			PQclear(pgResult);
		}
	}

	void sync_get_questions_by_user_phase(const std::string& user_id, const unsigned phase_id, 
		std::vector<sync_get_questions_by_user_phase_t>& result, std::string& error)
	{
		if (!check_user_id(user_id, error)) return;
		const char* const query = "SELECT * FROM sync_get_questions_by_user_phase('%s', '%d')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str(), phase_id);
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back
                (
                    sync_get_questions_by_user_phase_t
                    (
                     atoi( PQgetvalue(pgResult, irow, 0)),
                     std::string(PQgetvalue(pgResult, irow, 1)),
					 std::string(PQgetvalue(pgResult, irow, 2)),
					 std::string(PQgetvalue(pgResult, irow, 3)),
					 atoi( PQgetvalue(pgResult, irow, 4)),					 
					 std::string(PQgetvalue(pgResult, irow, 5))
                     )
                );
			}
			PQclear(pgResult);
		}
	}

	void sync_get_questions_revs_of_user(const std::string& user_id, std::vector<sync_get_questions_revs_of_user_t>& result, std::string& error)
	{
		if (!check_user_id(user_id, error)) return;
		const char* const query = "SELECT * FROM sync_get_questions_revs_of_user('%s')";
		char buffer[200];
		sprintf(buffer, query, user_id.c_str());
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(buffer, error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back
                (
                    sync_get_questions_revs_of_user_t
                    (
                     atoi( PQgetvalue(pgResult, irow, 0)),
                     std::string(PQgetvalue(pgResult, irow, 1)),
					 atoi( PQgetvalue(pgResult, irow, 2)),
					 std::string(PQgetvalue(pgResult, irow, 3))
                     )
                );

			}
			PQclear(pgResult);
		}
	}

	bool userproposals_add(const std::string& user_id, const unsigned question_id, const std::vector<unsigned> proposals_ids, std::string& error)
	{
		if (!check_user_id(user_id, error)) return false;
		std::stringstream ss_query;
		ss_query << "SELECT userproposals_add('" << user_id.c_str() << "', '" << question_id << "', ARRAY[";
		for(std::vector<unsigned>::const_iterator it = proposals_ids.begin(); it != proposals_ids.end(); it++)
		{
			ss_query << *it;
			if ((it+1) != proposals_ids.end())
				ss_query << ", ";
		}
		ss_query << "])";
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(ss_query.str().c_str(), error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	void sync_get_top_proposals_of_question(const unsigned question_id,  
		std::vector<calc_better_proposal_t>& result, std::string& error)
	{
		const char* const query = "SELECT * FROM sync_get_top_proposals_of_question('%d')";
		char buffer[200];
		sprintf(buffer, query, question_id);
		PGresult* pgResult = NULL;

		if (NULL != (pgResult = exec(buffer, error) ))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back( calc_better_proposal_t(
					atoi(PQgetvalue(pgResult, irow, 0)),
					std::string(PQgetvalue(pgResult, irow, 1)),
					atoi(PQgetvalue(pgResult, irow, 2))
					));

			}
			PQclear(pgResult);
		}
	}

	void sync_get_questions_sort(const std::vector<unsigned> questions_ids, 
                                 std::vector<sync_get_questions_sort_t>& result, std::string& error)
	{
		std::stringstream ss_query;
		ss_query << "SELECT * FROM sync_get_new_update(ARRAY[";
		for(std::vector<unsigned>::const_iterator it = questions_ids.begin(); it != questions_ids.end(); it++)
		{
			ss_query << *it ;
			if ((it+1) != questions_ids.end())
				ss_query << ", ";
		}
		ss_query << "])";
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(ss_query.str().c_str(), error)))
		{
			int row = PQntuples(pgResult);
			for (int irow = 0; irow < row; irow++)
			{
				result.push_back( sync_get_questions_sort_t(
					atoi(PQgetvalue(pgResult, irow, 0)), 
					std::string(PQgetvalue(pgResult, irow, 1)),
                    std::string(PQgetvalue(pgResult, irow, 2)),
                    std::string(PQgetvalue(pgResult, irow, 3)),
                    std::string(PQgetvalue(pgResult, irow, 4)),
					atoi(PQgetvalue(pgResult, irow, 5)),
					atoi(PQgetvalue(pgResult, irow, 6)),
					std::string(PQgetvalue(pgResult, irow, 7)) ));

			}
			PQclear(pgResult);
		}
	}

	bool delete_all_tables(std::string& error)
	{
		const char* const query = "SELECT delete_all_tables()";
		
		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(query, error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;
	}

	int question_open(const std::string& user_id, const std::string& question, const std::string& exp_date, 
		const int delay_days_between_phases, const std::vector<std::string>& proposals,
                          const std::vector<std::string>& usersquestion, std::string& error)
	{
		if (!check_user_id(user_id, error)) return -1;
		if (!check_string_100(question, error)) return -1;
		if (!check_timestamp(exp_date, error)) return -1;
		std::stringstream ss_query;
		ss_query << "SELECT question_open('" << user_id.c_str() << "', '" << question.c_str() << "', '" << 
			exp_date.c_str() << "', '" << delay_days_between_phases << "', ARRAY[";

		for(std::vector<std::string>::const_iterator it = proposals.begin(); it != proposals.end(); it++)
		{		
			if (!check_string_100(*it, error)) return -1;
			ss_query << "'" << *it << "'";
			if ((it+1) != proposals.end())
				ss_query << ", ";
		}
		ss_query << "], ARRAY[";
		for(std::vector<std::string>::const_iterator it = usersquestion.begin(); it != usersquestion.end(); it++)
		{
			if (!check_user_id(*it, error)) return -1;
			ss_query << "'" << it->c_str() << "'";
			if ((it+1) != usersquestion.end())
				ss_query << ", ";
		}
		ss_query << "])";
		PGresult* pgResult = NULL; 
		if (NULL == (pgResult = exec(ss_query.str().c_str(), error)))
			return -1;
		else
		{
			const char* result = NULL;
			result = PQgetvalue(pgResult, 0, 0);
			int query_id = atoi(result);
			PQclear(pgResult);
			
			return query_id;
		}

	}

	bool question_add_users(const unsigned question_id,  const std::vector<std::string>& users_ids, std::string& error)
	{
		std::stringstream ss_query;
		ss_query << "SELECT usersquestion_add('" << question_id << "', ARRAY[";
		for(std::vector<std::string>::const_iterator it = users_ids.begin(); it != users_ids.end(); it++)
		{
			if (!check_user_id(*it, error)) return false;
			ss_query << "'" << it->c_str() << "'";
			if ((it+1) != users_ids.end())
				ss_query << ", ";
		}
		ss_query << "])";	

		PGresult* pgResult = NULL;
		if (NULL != (pgResult = exec(ss_query.str().c_str(), error)))
		{
			PQclear(pgResult);
			return true;
		}
		else return false;

	}
	
}


