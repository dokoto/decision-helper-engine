package net.atdhe.structs;

public class Login
{
	public String host;
	public int port;
	public String user;
	public String password;

	public Login()
	{
	}

	public Login(String Host, int Port, String User, String Password)
	{
		host = Host;
		port = Port;
		user = User;
		password = Password;
	}

	public String port_ToString()
	{
		return String.valueOf(port);
	}

	public void port_FromString(String Port)
	{
		port = Integer.parseInt(Port);
	}

}
