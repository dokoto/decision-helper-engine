package net.whatswith.screens.questions;

import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Questions_by_Phase;
import net.whatswith.structs.Return;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Boxes extends ListActivity
{

	private boolean resumeHasRun = false;
	private ArrayList<Questions_by_Phase> questions_by_phase = null;
	private BoxesAdapter adapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		InitBoxes();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if ((keyCode == KeyEvent.KEYCODE_BACK))
		{
			Main.Nexus.Close = true;
			this.finish();			
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (resumeHasRun == true)
		{
			InitBoxes();
		} else
			resumeHasRun = true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_boxes_dbg, menu);
		else
			getMenuInflater().inflate(R.menu.menu_boxes, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.menu_boxes_refresh:
			InitBoxes();
			break;
		default:
			Main.Nexus.MenuSelector(this, item);
			break;
		}

		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		Intent intent = new Intent(Boxes.this, List_questions.class);
		Bundle b = new Bundle();
		b.putInt("PhaseId", questions_by_phase.get(position).phase_id);
		intent.putExtras(b);
		startActivity(intent);
	}
	
	private static class ViewHolder
	{
		public TextView tv_boxes_tv_phase;
		public TextView tv_boxes_tv_new_msg;
		public TextView tv_boxes_tv_tot_msg;
	}

	public class BoxesAdapter extends ArrayAdapter<Questions_by_Phase>
	{
		private final Context context;
		ArrayList<Questions_by_Phase> Rows;
		ViewHolder holder;

		public BoxesAdapter(Context context, ArrayList<Questions_by_Phase> Rows_)
		{
			super(context, R.layout.layo_row_boxes, Rows_);
			this.context = context;
			Rows = Rows_;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_boxes, parent, false);
				holder = new ViewHolder();
				holder.tv_boxes_tv_new_msg = (TextView) rowView.findViewById(R.id.layo_boxes_tv_new_msg);
				holder.tv_boxes_tv_tot_msg = (TextView) rowView.findViewById(R.id.layo_boxes_tv_tot_msg);
				holder.tv_boxes_tv_phase = (TextView) rowView.findViewById(R.id.layo_boxes_tv_phase);

				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();
			if (Rows.get(position).new_ones > 0)
				holder.tv_boxes_tv_new_msg.setTypeface(null, Typeface.BOLD);
			else
				holder.tv_boxes_tv_new_msg.setTypeface(null, Typeface.NORMAL);
			if (Rows.get(position).phase.compareTo("RESULTS") != 0)
				holder.tv_boxes_tv_new_msg.setText(String.valueOf(Rows.get(position).new_ones) + " WAITING");
			else
				holder.tv_boxes_tv_new_msg.setText(String.valueOf(Rows.get(position).new_ones) + " NO READ");
			holder.tv_boxes_tv_tot_msg.setText(String.valueOf(Rows.get(position).tot_ones) + " TOT");
			holder.tv_boxes_tv_phase.setText(Rows.get(position).phase);

			return rowView;
		}

	}

	private void InitBoxes()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return ret_q, ret_p;

			@Override
			protected void onPreExecute()
			{
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret_q = Commands.QuestionsSync(Main.Nexus.Storage.login());
				ret_p = Commands.PhasesUpdate(Main.Nexus.Storage.login());
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				if (ret_q.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret_q.message);
					Toast.makeText(getApplicationContext(), ret_q.message, Toast.LENGTH_SHORT).show();
				}
				if (ret_p.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret_p.message);
					Toast.makeText(getApplicationContext(), ret_p.message, Toast.LENGTH_SHORT).show();
				}
				if (ret_q.status && ret_p.status)
				{
					questions_by_phase = Commands.GetQuestionsByPhaseForBoxes();
					if (null == adapter)
					{
						adapter = new BoxesAdapter(Boxes.this, questions_by_phase);
						setListAdapter(adapter);
					} else
					{
						adapter.clear();
						if (android.os.Build.VERSION.SDK_INT > 10)
							adapter.addAll(questions_by_phase);
						else
						{
						    for (int i = 0; i < questions_by_phase.size(); i++) {
						        adapter.add(questions_by_phase.get(i));
						    }
						}
						
						adapter.setNotifyOnChange(true);
					}
				}
			}
		};
		task.execute((Void[]) null);
	}

}
