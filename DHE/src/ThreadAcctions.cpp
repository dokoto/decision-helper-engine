#include "ThreadAcctions.hpp"
#include "utils_macros.hpp"

namespace dhe
{

	void ThreadAcctions::addOverThread(const int SessionID, acction_e Acction)
	{
		acctions.insert(std::make_pair(SessionID, Acction));
	}

	void ThreadAcctions::getOverThread(const int SessionID, std::vector<acction_e>& Acctions)
	{
		if (acctions.empty()) return;
		std::pair<std::multimap<int, acction_e>::iterator, std::multimap<int, acction_e>::iterator> ret;
		ret = acctions.equal_range(SessionID);
		for (std::multimap<int, acction_e>::iterator it = ret.first; it!=ret.second; ++it)
			Acctions.push_back(it->second);		
	}

	void ThreadAcctions::removeOverThread(const int SessionID, acction_e Acction)
	{
		if (acctions.empty()) return;
		std::pair<std::multimap<int, acction_e>::iterator, std::multimap<int, acction_e>::iterator> ret;
		ret = acctions.equal_range(SessionID);
		for (std::multimap<int, acction_e>::iterator it = ret.first; it!=ret.second; ++it)
		{
			if (it->second == Acction)
			{
				acctions.erase(it);
				break;
			}
		}
	}

	bool ThreadAcctions::checkOverThread(const int SessionID, acction_e Acction)
	{		
		if (acctions.empty()) return false;
		
		if (Acction == TERMINATE_THREAD)
		{
			std::multimap<int, acction_e>::const_iterator it = acctions.find(SessionID);
			return (it != acctions.end());		
		} 
		else return false;
	}


	void ThreadAcctions::addOverIP(const std::string& arg1, acction_e Acction)
	{
        NOT_USED(Acction);
		Ips.insert(arg1);
	}

	bool ThreadAcctions::checkOverIP(const std::string& arg1, acction_e Acction)
	{		
		if (Ips.empty()) return false;
		
		if (Acction == BANNED_IP)
			return (Ips.find(arg1) != Ips.end());					
		else return false;
	}

	bool ThreadAcctions::removeOverIP(const std::string& arg1, acction_e Acction)
	{
		if (Ips.empty()) return false;
		if (Acction == UNBANNED_IP)
		{
			std::set<std::string>::iterator it = Ips.find(arg1);
			if ( it != Ips.end())		
			{
				Ips.erase(it);			
				return true;
			} else return false;
		} else return false;

	}
}

