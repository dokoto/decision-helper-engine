package net.whatswith.structs;

public class Questions_by_Phase
{
	public int phase_id;
	public String phase;
	public int new_ones;
	public int tot_ones;
	
	
	public Questions_by_Phase(int Phase_id, String Phase)
	{
		phase_id = Phase_id;
		phase = Phase;
		new_ones = 0;
		tot_ones = 0;
	}
		
}
