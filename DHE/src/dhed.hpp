#ifndef _DHED_HPP
#define _DHED_HPP

#include "sslConn.hpp"
#include <map>
#include <utility>
#include <string>

namespace dhe
{

enum ARGS_T {CONFPATH, CERTFILE, KEYFILE, MAX_PASSWORD_FAILS, PORT, PROXY_USER, PROXY_PASSWORD, MAX_POOL_SIZE, DB_HOST,
			DB_NAME, DB_USER, DB_PASSWORD, ENCRYPT_METHOD, TIMEOUT_IN_SECS, LOG_TO_FILE, LOG_PATH, LOG_MAX_SIZE};
enum MAX_T {MAX_NUM_OF_ARGS = 2, MAX_LENGHT_PATH = 1000, MAX_LENGTH_PORT = 10};
extern const char* argsTX[];

class dhed
{
    public:
        const int NumOfArgs;
        const char** ArrayOfArgs;
		SSL_CTX* ctx;
		int server_sock_fd, client_sock_fd;	
		int Thread_RC;
		pthread_attr_t Thread_Attr;
		unsigned max_pool_size;
        long timeout_in_secs;
		int max_password_fails;		
		char port[MAX_LENGTH_PORT];
		char console_port[MAX_LENGTH_PORT];		
        std::map<int, std::string> passArgs, confArgs;        
				
    private:
        dhed(void);
        dhed(const dhed& d);
        dhed& operator=(const dhed& d);
		bool parse_arg(const int arg, const std::string& line, std::map<int, std::string>& confArgs);
		bool dhe_service_th_(void);
    
	public:
        dhed(const int argc, const char** argv);
		~dhed(void);
    
		bool run(void);
        bool configure_serlet(void);
        bool parse_args(void);
        bool parse_conf(void);
};
    

}

#endif //  _DHED_HPP


