package servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ddbb.PostgreSQL;
import ddbb.Query;
import requests.ProcRequest;
import auth.Sessions;
import auth.Sessions.SessionReturn;

public class Service
{
	private Cmds sqlTemplate;
	private Sessions sessions;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private Query query;
	private PostgreSQL dbHandler;

	public Service(HttpServletRequest request, HttpServletResponse response, Cmds sqlTemplate, PostgreSQL dbHandler) throws Exception
	{
		this.sessions = new Sessions();
		this.sqlTemplate = sqlTemplate;
		this.request = request;
		this.response = response;
		this.query = ProcRequest.proccess(request, response, sqlTemplate);
		this.dbHandler = dbHandler;
	}

	private void ValidateSession() throws Exception
	{
		if (sessions.getTicket(request) == SessionReturn.SESSION_HAS_EXPIRED)
		{
			if (query.action().compareTo("USER_PASSWORD_EXIST") != 0)
				throw new Exception(SessionReturn.SESSION_HAS_EXPIRED.toString());
			else
				sessions.getNewTicket(request, response, query.GetRequestArgValue("USER_ID"));
			
		} else if (sessions.getTicket(request) == SessionReturn.SESSION_VALID_USER_NO_FOUND)
			throw new Exception(SessionReturn.SESSION_VALID_USER_NO_FOUND.toString());
		
		else if (sessions.getTicket(request) == SessionReturn.SESSION_VALID) {}			

	}

	public void Run() throws Exception
	{
		ValidateSession();
		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(query.GenerateResponse(dbHandler.ExecSQL(query.GenerateRequest())));
	}
}
