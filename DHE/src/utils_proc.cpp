#include "utils_proc.hpp"
#include <cstdlib>
#include <ctime>

namespace dhe
{

	int calc_better_proposal::calc_distancia(const std::vector< std::vector<int> >& matrix, const std::vector<int>& repRows)
	{
		if (repRows.empty()) return -1;
		std::map<int, int> dist;
		for( int row : repRows)
		{
			int sum = 0;
			for (size_t col = 0; col != matrix[row].size(); ++col)
			{
				for (size_t colCmp = 0; colCmp != matrix[row].size(); ++colCmp)
				{
					if (col != colCmp)
					{
						sum += (matrix[row][col] > matrix[row][colCmp]) ? matrix[row][col] - matrix[row][colCmp]
						: matrix[row][colCmp] - matrix[row][col];
					}
				}
			}
			//std::cout << "DISTANCIA : " << sum << " ROW : " << row << std::endl;
			dist[sum] = row;
		}
		if (dist.empty())
			return -1;
		else
		{
			srand ((unsigned)time(NULL));
			int rn = 0, ind = 0;
			rn = rand() % dist.size();
			ind = dist[rn];
			return map_proposal_id[ind];
		}
	}

	calc_better_proposal::~calc_better_proposal()
	{
	}

	calc_better_proposal::calc_better_proposal(int num_of_proposals, int num_of_users)
	{
		std::vector<int> vrow;
		srand ((unsigned)time(NULL));
		for (int col = 0; col < num_of_users; col++)
		{
			std::map<int, int> rep;
			for (int row = 0; row < num_of_proposals; row++)
			{
				bool loop_again = true;
				while (loop_again)
				{
					int randomInt = rand() % num_of_proposals + 1; 
					if (rep.find(randomInt) == rep.end())
					{						
						vrow.push_back(randomInt);
						rep[randomInt] = 0;
						loop_again = false;
					}
				}
			}
			matrix.push_back(vrow);
			vrow.clear();
		}
	}

	calc_better_proposal::calc_better_proposal(const std::vector<calc_better_proposal_t>& resulset)
	{
		unsigned prev_proposal_id = 0, row = 0, col = 0;
		bool first = true;
		std::vector<int> vrow;

		for(auto it = resulset.begin(); it != resulset.end(); it++)
		{
			if(!first && prev_proposal_id != it->proposal_id) 
			{
				++row;
				col = 0;
				matrix.push_back(vrow);
				vrow.clear();
			}

			vrow.push_back( it->priority );
			//map_user_id.insert(std::make_pair(col, std::string(it->user_id.Value())));
			map_proposal_id[row] = it->proposal_id;

			++col;
			prev_proposal_id = it->proposal_id;
			if(first) first = false;	
		}
		matrix.push_back(vrow);
	}

	int calc_better_proposal::get(void)
	{
		std::vector<int> sum(matrix.size());
		int max_value = 0, proposal = 0;
		for (size_t row = 0; row < matrix.size(); row++)
		{
			for (size_t col = 0; col < matrix[row].size(); col++)
				sum[row] += matrix[row][col];

			if (max_value < sum[row])
			{
				proposal = row;
				max_value = sum[row];
			}
		}

		//std::cout << "Max Proposal : " << proposal << " Value : " << max_value << std::endl;
		std::vector<int> repRows;
		for (size_t i = 0; i < matrix.size(); i++)
		{
			if (sum[i] == max_value)
				repRows.push_back(i);
		}

		if (repRows.size() > 1)
		{
			//for (int row : repRows)
			//	std::cout << "Repet Proposal Value at : " << row << std::endl;
			return calc_distancia(matrix, repRows);
		}
		else		
			return (map_proposal_id.empty()) ? -1 : map_proposal_id[proposal];		
	}

}


