package net.whatswith.screens.login;

import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.screens.menus.Debug_Settings;
import net.whatswith.screens.questions.Boxes;
import net.whatswith.structs.Return;
import net.whatswith.tools.ActivityLuncher;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class Check_phone_number extends Activity
{

	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_check_phone_number);

		Bundle b = getIntent().getExtras();
		final int code = b.getInt("Code");
		final int min_password_len = 10;
		final String Phone = b.getString("Phone");

		pd = new ProgressDialog(Check_phone_number.this);
		pd.setTitle("Checking user status...");
		pd.setMessage("Please wait.");
		pd.setCancelable(false);
		pd.setIndeterminate(true);

		findViewById(R.id.layo_check_phone_number_bt_continue).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				try
				{
					pd.show();
					final EditText et_code = (EditText) findViewById(R.id.layo_check_phone_number_et_code);
					String src_code = et_code.getText().toString();
					String sent_code = String.valueOf(code);
					if (src_code.compareTo(sent_code) != 0)
					{
						et_code.setError("Fail, Wrong Code ! re-type please.");
						pd.dismiss();
						return;
					}

					final EditText et_password = (EditText) findViewById(R.id.layo_check_phone_number_et_password);
					if (!Main.Nexus.Storage.crazy_mode && et_password.getText().toString().length() < min_password_len)
					{
						et_password.setError("Fail, password must has at least a minimun of 10 characteres.");
						pd.dismiss();
						return;
					}

					final EditText et_password_re = (EditText) findViewById(R.id.layo_check_phone_number_et_password_conf);
					if (et_password_re.getText().toString().compareTo(et_password.getText().toString()) != 0)
					{
						et_password_re.setError("Fail, both passwords must be equal.");
						pd.dismiss();
						return;
					}

					Main.Nexus.Storage.phoneNumber(Phone, Main.Nexus.SimCountryIso);
					Main.Nexus.Storage.password = et_password.getText().toString();
					Main.Nexus.Serial.Save(Main.Nexus.Storage);
					Async_UserExist(Phone, et_password.getText().toString());

				} catch (Exception e)
				{
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
					Check_phone_number.this.finish();
				}
			}
		});

	}

	private void Async_Create_new_user(final String phoneNumber, final String password)
	{
		try
		{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
			{
				private Return ret;

				@Override
				protected void onPreExecute()
				{
				}

				@Override
				protected Void doInBackground(Void... arg0)
				{
					ret = Commands.FirstSession(Main.Nexus.Storage.host, Main.Nexus.Storage.port, phoneNumber, password);
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					pd.dismiss();
					if (ret.status == false)
					{
						if (ret.type == Return.types.HOST_DOWN || ret.type == Return.types.INTERNET_DOWN)
							new ActivityLuncher(Check_phone_number.this, Debug_Settings.class).Launch();
						else if (ret.type == Return.types.BANNED_IP)
							Check_phone_number.this.finish();

						Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
					} else
					{
						new ActivityLuncher(Check_phone_number.this, Boxes.class).Launch();
						Check_phone_number.this.finish();
					}
				}
			};
			task.execute((Void[]) null);

		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}

	private void Async_UserExist(final String user_id, final String password)
	{
		try
		{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
			{
				private Return ret;

				@Override
				protected void onPreExecute()
				{
				}

				@Override
				protected Void doInBackground(Void... arg0)
				{
					ret = Commands.UserExist(Main.Nexus.Storage.host, Main.Nexus.Storage.port, user_id);
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					if (ret.status == true)
					{
						pd.dismiss();
						Intent intent = new Intent(Check_phone_number.this, Getpassword.class);
						Bundle b = new Bundle();
						b.putString("Password", password);
						intent.putExtras(b);
						startActivity(intent);
						Check_phone_number.this.finish();
					} else
						Async_Create_new_user(Main.Nexus.Storage.phoneNumber(), password);

				}
			};
			task.execute((Void[]) null);

		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_debug_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Main.Nexus.MenuSelector(this, item);
		return true;
	}

}
