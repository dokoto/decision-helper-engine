#ifndef _THREADACCTIONS_HPP
#define _THREADACCTIONS_HPP

#include <map>
#include <vector>
#include <set>
#include <string>

namespace dhe
{

const char* const acctions_x[] = {"TERMINATE_THREAD", "BANNED_IP", "UNBANNED_IP"};
enum acction_e {TERMINATE_THREAD, BANNED_IP, UNBANNED_IP};

class ThreadAcctions
{
	private:
		
		std::multimap<int, acction_e> acctions;
		std::set<std::string> Ips;

		ThreadAcctions(const ThreadAcctions& ) {}
        ThreadAcctions& operator=(const ThreadAcctions&) { return *this; }

	public:

		ThreadAcctions(void) {}
		~ThreadAcctions(void) {}

		void addOverThread(const int SessionID, acction_e Acction);		
		void getOverThread(const int SessionID, std::vector<acction_e>& Acctions);
		void removeOverThread(const int SessionID, acction_e Acction);
		bool checkOverThread(const int SessionID, acction_e Acction);

		void addOverIP(const std::string& arg1, acction_e Acction);		
		bool checkOverIP(const std::string& arg1, acction_e Acction);
		bool removeOverIP(const std::string& arg1, acction_e Acction);
};

}

#endif // _THREADACCTIONS_HPP

