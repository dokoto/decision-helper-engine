package net.whatswith.screens.login;

import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.screens.menus.Debug_Settings;
import net.whatswith.screens.questions.Boxes;
import net.whatswith.structs.Return;
import net.whatswith.tools.ActivityLuncher;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Getpassword extends Activity
{
	private Return ret = new Return();
	final int min_password_len = 10;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.layo_row_getpassword);
		String Password = null;
		try
		{
			Bundle b = getIntent().getExtras();
			Password = b.getString("Password");
		}
		catch (Exception e)
		{
			Password = new String();	
		}
		
		final RelativeLayout container = (RelativeLayout) findViewById(R.id.layo_getpassword);
		container.setVisibility(View.GONE);
		Async_UserExist(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), Password);
		
		findViewById(R.id.layo_getpassword_bt_go).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				try
				{
					final EditText et_password = (EditText) findViewById(R.id.layo_getpassword_et_password);
					if (!Main.Nexus.Storage.crazy_mode && et_password.getText().toString().length() < min_password_len)
					{
						et_password.setError("Fail, password has at least a minimun of 10 characteres.");
						return;
					}
					Auth(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), et_password.getText().toString());

				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});

	}
	
	
	private void Auth(final String host, final int port, final String PhoneNumber, final String Password)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private EditText password;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(Getpassword.this);
				pd.setTitle("Authenticating User...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();

				password = (EditText) findViewById(R.id.layo_getpassword_et_password);
				password.setEnabled(false);
				final Button go = (Button) findViewById(R.id.layo_getpassword_bt_go);
				go.setEnabled(false);

			}

			@Override
			protected Void doInBackground(Void... arg0)
			{				
				ret = Commands.ConnectAndAuth(host, port, PhoneNumber, Password);				
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				CheckProcess(ret);
			}

		};
		task.execute((Void[]) null);		
	}
	
	private void Async_UserExist(final String host, final int port, final String user_id, final String password)
	{
		try
		{
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
			{
				private Return ret;
				private ProgressDialog pd;

				@Override
				protected void onPreExecute()
				{
					pd = new ProgressDialog(Getpassword.this);
					pd.setTitle("Checking User existence...");
					pd.setMessage("Please wait.");
					pd.setCancelable(false);
					pd.setIndeterminate(true);
					pd.show();
				}

				@Override
				protected Void doInBackground(Void... arg0)
				{
					ret = Commands.UserExist(Main.Nexus.Storage.host, Main.Nexus.Storage.port, user_id);
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					pd.dismiss();
					if (ret.status == true)
					{																		
						if (password.isEmpty() == false && Main.Nexus.Storage.password.isEmpty() == true)
							Auth(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), password);
						else if (password.isEmpty() == true && Main.Nexus.Storage.password.isEmpty() == false)
							Auth(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), Main.Nexus.Storage.password);
						else if (password.isEmpty() == true && Main.Nexus.Storage.password.isEmpty() == true)
							Auth(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), Main.Nexus.Storage.password);
						else if (password.isEmpty() == false && Main.Nexus.Storage.password.isEmpty() == false)
							Auth(Main.Nexus.Storage.host, Main.Nexus.Storage.port, Main.Nexus.Storage.phoneNumber(), password);
					}
					else				
					{
						new ActivityLuncher(Getpassword.this, Get_phone_number.class).Launch();
						Getpassword.this.finish();
					}
					
				}
			};
			task.execute((Void[]) null);

		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
		}
	}	
		
	private void CheckProcess(Return ret)
	{
		if (ret.status)
		{			
			Main.Nexus.Storage.password = ret.arg1;			
			Main.Nexus.Serial.Save(Main.Nexus.Storage);
			new ActivityLuncher(this, Boxes.class).Launch();
			//Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();				
			Getpassword.this.finish();			
		} else
		{
			Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_LONG).show();
			if (ret.type == Return.types.HOST_DOWN || ret.type == Return.types.INTERNET_DOWN )			
				new ActivityLuncher(this, Debug_Settings.class).Launch();
			if (ret.type == Return.types.BANNED_IP)
				Getpassword.this.finish();
									
			final RelativeLayout container = (RelativeLayout) findViewById(R.id.layo_getpassword);
			container.setVisibility(View.VISIBLE);
			final EditText password = (EditText) findViewById(R.id.layo_getpassword_et_password);
			password.setEnabled(true);
			password.setText("");
			final Button go = (Button) findViewById(R.id.layo_getpassword_bt_go);
			go.setEnabled(true);			

		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (true == Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_debug_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		
		Main.Nexus.MenuSelector(this, item);
		return true;
	}

}
