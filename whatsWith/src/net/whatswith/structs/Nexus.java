package net.whatswith.structs;

import java.net.InetAddress;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.contacts.Contacts;
import net.whatswith.screens.login.Get_phone_number;
import net.whatswith.screens.login.Getpassword;
import net.whatswith.screens.menus.Debug_Settings;
import net.whatswith.screens.questions.Add_question;
import net.whatswith.service.WWNotifService;
import net.whatswith.tools.ActivityLuncher;
import net.whatswith.tools.Serial;

public class Nexus
{
	public Container Storage;
	public boolean Close = false;
	public net.whatswith.tools.Serial<Container> Serial = null;
	private final String key = "whatswithKey";
	public Activity MainActivity;
	public TelephonyManager tm;
	public String SimCountryIso;
	private long TimeOutInSec = 10;
	private TextView status;
	private ProgressBar mProgress;

	public Nexus(Activity act)
	{
		MainActivity = act;
		this.tm = (TelephonyManager) act.getSystemService(Context.TELEPHONY_SERVICE);
		SimCountryIso = tm.getSimCountryIso().toUpperCase(Locale.ENGLISH);
		Serial = new Serial<Container>(act, key);
		if (null == (Storage = Serial.Load()))
			Storage = new Container();
	}

	public void InitApp()
	{
		status = (TextView) MainActivity.findViewById(R.id.layo_main_tv_status);
		status.setText("Checking Connection...");
		CheckConnections(Storage.host);
	}

	private void CheckConnections(final String host)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private Return ret;

			@Override
			protected void onPreExecute()
			{
				mProgress = (ProgressBar) MainActivity.findViewById(R.id.layo_main_pb_status);
				mProgress.setVisibility(View.VISIBLE);
				status.setText("Checking Server is Up...");
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = new Return(true, "OK", "Checking net");
				if (Storage.desactv_conn == false)
				{
					if (CheckInternetUP() == false)
						ret = new Return(false, "Internet is Down", "Check Connections");
					if (CheckHostIsUP(Storage.host, TimeUnit.SECONDS.toMillis(TimeOutInSec)) == false)
						ret = new Return(false, "The Server is Down, please try later", "Check Host is Up");
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{				
				mProgress.setVisibility(View.GONE);
				if (Storage.desactv_conn == true)
					Toast.makeText(MainActivity.getApplicationContext(), "Check Connection is des-activated, MASTER !!", Toast.LENGTH_LONG).show();
				if (ret.status == false)
				{
					Toast.makeText(MainActivity.getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
					status.setText(ret.message);
				} else
				{
					if (Storage.phoneNumber().isEmpty())
						new ActivityLuncher(MainActivity, Get_phone_number.class).Launch();
					else
						new ActivityLuncher(MainActivity, Getpassword.class).Launch();
				}
			}

		};
		task.execute((Void[]) null);
	}

	public void DownApp()
	{
		Commands.Async_Commit_UnPlug(Storage.login(), null);
	}

	public void DebugMenu(Activity act, MenuItem item)
	{
		switch (item.getItemId())
		{

		case R.id.action_debug_settings:
			Intent intent = new Intent(act, Debug_Settings.class);
			act.startActivity(intent);
			break;
		}
	}

	public Login login()
	{
		return Storage.login();
	}

	public BroadcastReceiver receiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(context, WWNotifService.class);
			PendingIntent pi = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
			am.cancel(pi);
			if (Storage.intervalInMin > 0)
			{
				//ELAPSED_REALTIME_WAKEUP
				am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME , SystemClock.elapsedRealtime() + Storage.intervalInMin * 60 * 1000,
						Storage.intervalInMin * 60 * 1000, pi);
			}
		}
	};

	public void setWakeService()
	{
		AlarmManager am = (AlarmManager) MainActivity.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(MainActivity, WWNotifService.class);
		PendingIntent pi = PendingIntent.getService(MainActivity, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
		am.cancel(pi);
		if (Storage.intervalInMin > 0)
		{
			am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + Storage.intervalInMin * 60 * 1000,
					Storage.intervalInMin * 60 * 1000, pi);
		}
	}

	public void MenuSelector(Activity act, MenuItem item)
	{
		Intent intent = null;
		switch (item.getItemId())
		{

		case R.id.menu_boxes_add:
			intent = new Intent(act, Add_question.class);
			act.startActivity(intent);
			break;

		case R.id.menu_add_question_add_contacts:
			Commands.Async_Commit_Update_Contacts(Storage.login(), act,
					new ActivityLuncher(act, Contacts.class, "Filter", Contacts_db.FilterType.BY_NAME.ordinal()));
			break;

		case R.id.action_debug_settings:
			intent = new Intent(act, Debug_Settings.class);
			act.startActivity(intent);
			break;
		}
	}

	public boolean CheckInternetUP()
	{

		try
		{
			ConnectivityManager conMgr = (ConnectivityManager) MainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (conMgr != null)
			{
				NetworkInfo i = conMgr.getActiveNetworkInfo();
				if (i != null)
				{
					if (!i.isConnected())
						return false;
					if (!i.isAvailable())
						return false;
					return true;
				} else
					return false;

			} else
				return false;
		} catch (Exception e)
		{
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return false;
		}
	}

	public boolean CheckHostIsUP(final String Host, final long TimeOutInMillis)
	{

		if (Host.isEmpty())
			return false;
		InetAddress CheckIP = null;
		try
		{
			CheckIP = InetAddress.getByName(Host);
			return CheckIP.isReachable((int) TimeOutInMillis);
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}

	}
}
