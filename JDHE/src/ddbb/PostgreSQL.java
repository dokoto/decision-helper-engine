package ddbb;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class PostgreSQL
{
	Connection dbConnection = null;
	private Properties prop;
	private ClassLoader loader;
	private InputStream stream ;
	private Statement dbStatement = null;
	private final String url = "jdbc:postgresql://";
	
	public PostgreSQL() throws IOException
	{
		prop = new Properties();
		loader = Thread.currentThread().getContextClassLoader();
		stream = loader.getResourceAsStream("config.properties");
		prop.load(stream);
	}
	
	public void Connect() throws SQLException
	{
		dbConnection = DriverManager.getConnection(url + prop.getProperty("db_host") + "/" + prop.getProperty("db_name"),
				prop.getProperty("db_user"), prop.getProperty("db_password"));
	}
	
	public void DesConnect() throws SQLException
	{
		if (dbStatement != null)
			dbStatement.close();
		if (dbConnection != null)
			dbConnection.close();
	}
	
	public ResultSet ExecSQL(String sql) throws SQLException
	{
		dbStatement = dbConnection.createStatement();
        return dbStatement.executeQuery(sql);
	}
	
	
	
}
