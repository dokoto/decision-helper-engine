package net.atdhe;

import net.atdhe.structs.Nexus;
import net.atdhe.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;

public class Main extends Activity
{

	public static Nexus Nexus = null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_mainact);
		Init();
	}

	@Override
	protected void onDestroy()
	{
		Nexus.DownApp();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (true == Main.Nexus.Storage.debug_menu_on)
			getMenuInflater().inflate(R.menu.menu_debug_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		Main.Nexus.MenuSelector(this, item);
		return true;
	}

	private void Init()
	{
		Nexus = new net.atdhe.structs.Nexus(this);
		Nexus.InitApp();
	}

}
