package net.whatswith.service;

import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Container;
import net.whatswith.structs.Phases_db;
import net.whatswith.structs.Question_db;
import net.whatswith.structs.Questions_by_Phase;
import net.whatswith.structs.Return;
import net.whatswith.tools.Serial;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.SparseArray;
import android.app.Service;
import android.content.Intent;

public class WWNotifService extends Service
{	
	private WakeLock mWakeLock;
	public final int id = 1;

	/**
	 * Simply return null, since our Service will not be communicating with any
	 * other components. It just does its work silently.
	 */

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	/**
	 * This is where we initialize. We call this when onStart/onStartCommand is
	 * called by the system. We won't do anything with the intent here, and you
	 * probably won't, either.
	 */
	private void handleIntent(Intent intent)
	{		
		// obtain the wake lock
		PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WWNOTIFSERVICE");
		mWakeLock.acquire();
		// check the global background data setting
		ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		if (null == cm.getActiveNetworkInfo())
		{
			stopSelf();
			return;
		}
		// do the actual work, in a separate thread
		new PollTask().execute();
		
		
	}

	private class PollTask extends AsyncTask<Void, Void, Void>
	{
		/**
		 * This is where YOU do YOUR work. There's nothing for me to write here
		 * you have to fill this in. Make your HTTP request(s) or whatever it is
		 * you have to do to get your updates in here, because this is run in a
		 * separate thread
		 */
		private Return ret = new Return(true);
		private Return ret_q = new Return(true);
		private Return ret_p = new Return(true);
		private Container Storage = null;
		private net.whatswith.tools.Serial<Container> Serial = null;
		private final String key = "whatswithKey";
		private SparseArray<Question_db> questions;
		private SparseArray<Phases_db> phases;
		
		@Override
		protected Void doInBackground(Void... params)
		{
			Serial = new Serial<Container>(WWNotifService.this, key);
			if (null == (Storage = Serial.Load()))
				Storage = new Container();
			if (Storage.host.isEmpty() || Storage.phoneNumber().isEmpty() || Storage.password.isEmpty())
				ret = new Return(false, "No Connection", "Whats With Sevice: Connecting..");
			else
			{
				questions = Commands.QuestionsSync_ISOLATE(Storage.login(), ret_q);				
				phases = Commands.PhasesUpdate_ISOLATE(Storage.login(), ret_p);
			}
			if (ret.status == false || ret_q.status == false || ret_p.status == false)
			{
				System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
			}
			else
			{
				ArrayList<Questions_by_Phase> questions_by_phase = Commands.GetQuestionsByPhaseForBoxes_ISOLATE(questions, phases);					
				WWNotifManager notif = new WWNotifManager(id, WWNotifService.this);
				StringBuilder sb = new StringBuilder();
				for (Questions_by_Phase row : questions_by_phase)
				{
					if (row.new_ones > 0)					
						sb.append("You have " +  row.new_ones + " " + row.phase + " questions awaiting \n");					
				}
				if (!sb.toString().isEmpty())
				{	
					notif.setLayo(R.drawable.ic_launcher_app, "Questions waiting", sb.toString());
					notif.setNotify(Main.class);					  
				}
			}
			
			return null;
		}

		/**
		 * In here you should interpret whatever you fetched in doInBackground
		 * and push any notifications you need to the status bar, using the
		 * NotificationManager. I will not cover this here, go check the docs on
		 * NotificationManager. What you HAVE to do is call stopSelf() after
		 * you've pushed your notification(s). This will: 1) Kill the service so
		 * it doesn't waste precious resources 2) Call onDestroy() which will
		 * release the wake lock, so the device can go to sleep again and save
		 * precious battery.
		 */
		@Override
		protected void onPostExecute(Void result)
		{
			
			
			stopSelf();
		}
	}

	/**
	 * This is deprecated, but you have to implement it if you're planning on
	 * supporting devices with an API level lower than 5 (Android 2.0).
	 */
	@Override
	public void onStart(Intent intent, int startId)
	{
		handleIntent(intent);
	}

	/**
	 * This is called on 2.0+ (API level 5 or higher). Returning
	 * START_NOT_STICKY tells the system to not restart the service if it is
	 * killed because of poor resource (memory/cpu) conditions.
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		handleIntent(intent);
		return START_NOT_STICKY;
	}

	/**
	 * In onDestroy() we release our wake lock. This ensures that whenever the
	 * Service stops (killed for resources, stopSelf() called, etc.), the wake
	 * lock will be released.
	 */
	public void onDestroy()
	{
		super.onDestroy();
		mWakeLock.release();
	}

}
