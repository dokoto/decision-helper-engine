package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ddbb.PostgreSQL;
import auth.Sessions;

/**
 * Servlet implementation class Jdhe
 */
public class Jdhe extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private Cmds SqlTemplate;
	private PostgreSQL dbHandler;

	public Jdhe()
	{
		super();
	}

	public void init(ServletConfig config) throws ServletException
	{
		try
		{
			SqlTemplate = new Cmds();
			dbHandler = new PostgreSQL();
			dbHandler.Connect();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void destroy()
	{
		try
		{
			dbHandler.DesConnect();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			Service serv = new Service(request, response, SqlTemplate, dbHandler);
			serv.Run();
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
