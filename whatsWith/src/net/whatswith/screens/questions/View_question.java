package net.whatswith.screens.questions;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import net.whatswith.R;
import net.whatswith.operations.Commands;
import net.whatswith.screens.contacts.Contacts;
import net.whatswith.screens.main.Main;
import net.whatswith.structs.Contacts_db;
import net.whatswith.structs.Question_db;
import net.whatswith.structs.Return;
import net.whatswith.tools.ActivityLuncher;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class View_question extends FragmentActivity
{
	private int QuestionId;
	private int PhaseId;
	private ArrayList<Adapter_row> ProposalsAdapter = null;
	private Adapter_ViewQuestion Adapter;
	private ListView lv_proposals;
	private boolean view_mode = false;	
	
	private class Adapter_row
	{

		public Adapter_row(ImageView ico, String Proposal, String User_id)
		{
			this.User_id = User_id;
			this.Proposal = Proposal;
			this.Ico = ico;
		}

		public Adapter_row(String Proposal, String User_id)
		{
			this.User_id = User_id;
			this.Proposal = Proposal;
			this.Ico = null;
		}

		public ImageView Ico;
		public String Proposal;
		public String User_id;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_view_question);
		try
		{
			Bundle b = getIntent().getExtras();
			QuestionId = b.getInt("QuestionId");
			PhaseId = b.getInt("PhaseId");

			Main.Nexus.Storage.contacts.clear();
			lv_proposals = (ListView) findViewById(R.id.layo_view_question_lv_proposal);
			String QuestionOwner = Main.Nexus.Storage.questions.get(QuestionId).user_id;
			boolean is_phase_accepted = Main.Nexus.Storage.questions.get(QuestionId).is_phase_accepted;
			if (Main.Nexus.Storage.phoneNumber().compareTo(QuestionOwner) == 0 || is_phase_accepted == true)
			{
				ImageView bt_add = (ImageView) findViewById(R.id.layo_view_question_bt_proposal);
				EditText et_proposal = (EditText) findViewById(R.id.layo_view_question_et_proposal);
				bt_add.setVisibility(View.GONE);
				et_proposal.setVisibility(View.GONE);
				view_mode = true;
			} else
			{
				findViewById(R.id.layo_view_question_bt_proposal).setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						EditText et_proposal = (EditText) findViewById(R.id.layo_view_question_et_proposal);
						ImageView iv_del_proposal = new ImageView(View_question.this);
						iv_del_proposal.setImageResource(R.drawable.ic_menu_delete);
						Adapter.Rows.add(0, new Adapter_row(iv_del_proposal, et_proposal.getText().toString(),
								Main.Nexus.login().user));
						et_proposal.setText("");
						et_proposal.setHint("Proposal");
						et_proposal.requestFocus();
						Adapter.notifyDataSetChanged();
					}
				});
			}

			InitViewQuestions();
		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if (view_mode == false)
			getMenuInflater().inflate(R.menu.menu_view_question, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.menu_view_question_save)
			save();
		else if (item.getItemId() ==  R.id.menu_view_question_add_contacts)
			Commands.Async_Commit_Update_Contacts( Main.Nexus.login(), QuestionId, this,  
				new ActivityLuncher(this, Contacts.class, "Filter", Contacts_db.FilterType.BY_LOCK.ordinal()) );		
		else
			Main.Nexus.MenuSelector(this, item);

		return true;
	}

	private void InitViewQuestions()
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return ret;
			private Format formatter = new SimpleDateFormat("yyyy-MM-dd");

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(View_question.this);
				pd.setTitle("Upgrading Question...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				ret = Commands.ProposalsSyncByQuestion(Main.Nexus.login(), QuestionId);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{				
				if (ret.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + ret.message);
					Toast.makeText(getApplicationContext(), ret.message, Toast.LENGTH_SHORT).show();
				} else
				{
					ProposalsAdapter = new ArrayList<Adapter_row>();
					for (int i = 0; i < Main.Nexus.Storage.proposals.size(); ++i)
					{						
						int key = Main.Nexus.Storage.proposals.keyAt(i);
						ProposalsAdapter.add(new Adapter_row(Main.Nexus.Storage.proposals.get(key).proposal, 
								Main.Nexus.Storage.proposals.get(key).user_id));
					}
					TextView et_question = (TextView) findViewById(R.id.layo_view_question_et_question);
					TextView et_exp_date = (TextView) findViewById(R.id.layo_view_question_et_exp_date);
					Config_HeadUI(et_question, et_exp_date);
					Question_db q = Main.Nexus.Storage.questions.get(QuestionId);
					if (null != q)
					{
						et_question.setText(q.question);
						et_exp_date.setText(formatter.format(q.exp_date));
					}

					if (null == Adapter)
					{						
						Adapter = new Adapter_ViewQuestion(View_question.this, ProposalsAdapter);
						lv_proposals.setAdapter(Adapter);
					} else
					{
						Adapter.clear();
						if (android.os.Build.VERSION.SDK_INT > 10)
							Adapter.addAll(ProposalsAdapter);
						else
						{
						    for (int i = 0; i < ProposalsAdapter.size(); i++) {
						    	Adapter.add(ProposalsAdapter.get(i));
						    }
						}
						Adapter.setNotifyOnChange(true);
					}

				}
				pd.dismiss();
			}
		};
		task.execute((Void[]) null);
	}

	private void Config_HeadUI(TextView et_question, TextView et_exp_date)
	{
		String scolor = Main.Nexus.Storage.phases.get(PhaseId).color;
		try
		{
			if (scolor.substring(0, 1).compareTo("#") != 0)
				scolor = "#" + scolor;
			int icolor = Color.parseColor(scolor);
			et_question.setTextColor(icolor);
			et_question.setTypeface(null, Typeface.BOLD);
		} catch (Exception e)
		{
			et_question.setTextColor(Color.parseColor("black"));
		}
	}

	private void save()
	{
		try
		{
			boolean error = false;
			EditText et_proposal = (EditText) findViewById(R.id.layo_view_question_et_proposal);

			ArrayList<String> proposals = new ArrayList<String>();
			if (Adapter.Rows.size() > 0)
			{
				for (Adapter_row row : Adapter.Rows)
				{
					if (row.User_id.compareTo(Main.Nexus.login().user) == 0)
						proposals.add(Commands.JsonSpecialCharFixer(row.Proposal));
				}
				et_proposal.setError(null);
			} else if (et_proposal.getText().toString().isEmpty())
			{
				et_proposal.setError("The proposals are mandatory, at least write one.");
				error = true;
			}

			if (!et_proposal.getText().toString().isEmpty())
			{
				proposals.add(Commands.JsonSpecialCharFixer(et_proposal.getText().toString()));
				et_proposal.setError(null);
			}
			
			String[] users_ids = Commands.ContactsToArrayOnlySelected(Main.Nexus.Storage.contacts, false);

			if (error == false)
			{
				if (users_ids.length > 0)
					add_proposals_and_users(proposals.toArray(new String[proposals.size()]), users_ids);
				else
					add_proposals_and_users(proposals.toArray(new String[proposals.size()]));
			}

		} catch (Exception e)
		{
			Toast.makeText(getApplicationContext(), "ERROR : " + e.toString(), Toast.LENGTH_LONG).show();
		}
	}

	private void add_proposals_and_users(final String[] proposals)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return retA;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(View_question.this);
				pd.setTitle("Adding new Proposals...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				retA = Commands.AddProposals(Main.Nexus.login(), QuestionId, proposals);
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				if (retA.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + retA.message);
					Toast.makeText(getApplicationContext(), retA.message, Toast.LENGTH_SHORT).show();
				}
				if (retA.status)
				{
					Commands.Async_Question_Accept(Main.Nexus.login(), View_question.this, null, true, QuestionId);
				}
			}
		};
		task.execute((Void[]) null);
	}
		
	private void add_proposals_and_users(final String[] proposals, final String[] users_ids)
	{
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>()
		{
			private ProgressDialog pd;
			private Return retA, retB;

			@Override
			protected void onPreExecute()
			{
				pd = new ProgressDialog(View_question.this);
				pd.setTitle("Adding new Proposals...");
				pd.setMessage("Please wait.");
				pd.setCancelable(false);
				pd.setIndeterminate(true);
				pd.show();
			}

			@Override
			protected Void doInBackground(Void... arg0)
			{
				retA = Commands.AddProposals(Main.Nexus.login(), QuestionId, proposals);
				if (users_ids.length > 0)
					retB = Commands.Question_add_users(Main.Nexus.login(), QuestionId, users_ids);
				else
					retB = new Return(true, "Success", "Add new User to Question");
				return null;
			}

			@Override
			protected void onPostExecute(Void result)
			{
				pd.dismiss();
				if (retA.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + retA.message);
					Toast.makeText(getApplicationContext(), retA.message, Toast.LENGTH_SHORT).show();
				}
				if (retB.status == false)
				{
					System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + retB.message);
					Toast.makeText(getApplicationContext(), retB.message, Toast.LENGTH_SHORT).show();
				}
				if (retA.status && retB.status)
				{
					Commands.Async_Question_Accept(Main.Nexus.login(), View_question.this, null, true, QuestionId);
				}
			}
		};
		task.execute((Void[]) null);
	}
	
	private static class ViewHolder
	{
		public ImageView iv_ico;
		public TextView tv_proposal;
		public TextView tv_user_id;
	}


	public class Adapter_ViewQuestion extends ArrayAdapter<Adapter_row>
	{
		public final Context ContextAdapter;
		public ArrayList<Adapter_row> Rows;
		ViewHolder holder;

		public Adapter_ViewQuestion(Context context, ArrayList<Adapter_row> rows)
		{
			super(context, R.layout.layo_row_view_question, rows);
			this.ContextAdapter = context;
			this.Rows = rows;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View rowView = convertView;
			if (rowView == null)
			{
				LayoutInflater inflater = (LayoutInflater) ContextAdapter.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.layo_row_view_question, parent, false);
				holder = new ViewHolder();
				holder.iv_ico = (ImageView) rowView.findViewById(R.id.layo_row_view_question_bt_del);
				holder.tv_proposal = (TextView) rowView.findViewById(R.id.layo_row_view_question_et_proposal);
				holder.tv_user_id = (TextView) rowView.findViewById(R.id.layo_row_view_question_user_id);
				rowView.setTag(holder);
			}

			holder = (ViewHolder) rowView.getTag();

			if (Rows.size() == 0)
			{
				holder.tv_proposal.setVisibility(View.GONE);
				holder.iv_ico.setVisibility(View.GONE);
			} else
			{
				holder.tv_user_id.setText(Commands.GetContactNameFromPhone(Rows.get(position).User_id, View_question.this));
				holder.tv_proposal.setText(Rows.get(position).Proposal);
				if (Rows.get(position).Ico != null)
				{
					holder.iv_ico.setImageResource(R.drawable.ic_menu_delete);
					holder.iv_ico.setVisibility(View.VISIBLE);
					holder.iv_ico.setTag(Integer.valueOf(position));
					holder.iv_ico.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(final View view)
						{
							int position = (Integer) view.getTag();
							Rows.remove(position);
							notifyDataSetChanged();
						}
					});
				} else
				{
					holder.iv_ico.setVisibility(View.GONE);
				}
			}
			notifyDataSetChanged();
			return rowView;
		}

	}

}
