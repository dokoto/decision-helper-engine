package net.atdhe.tools;

import net.atdhe.structs.Container;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

public class Serial<T>
{
	private SharedPreferences Prefs;
	private SharedPreferences.Editor Editor;
	private String Key;
	private Activity App = null;
	private Service Serv = null;
	private Gson gson;

	public Serial(final Activity app, final String key)
	{
		this.Key = key;
		this.App = app;
		this.Prefs = App.getSharedPreferences(App.getApplicationInfo().name, Context.MODE_PRIVATE);
		this.Editor = Prefs.edit();
		this.Editor.commit();
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}

	public Serial(final Service serv, final String key)
	{
		this.Key = key;
		this.Serv = serv;
		this.Prefs = Serv.getSharedPreferences(Serv.getApplicationInfo().name, Context.MODE_PRIVATE);
		this.Editor = Prefs.edit();
		this.Editor.commit();
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}

	public void Delete()
	{
		Editor.clear().commit();
	}

	public void Delete(String key)
	{
		Editor.remove(Key).commit();
	}

	public Container Load()
	{
		try
		{
			JsonParser parser = new JsonParser();
			String gsonString = Prefs.getString(Key, null);
			if (gsonString == null)
				return null;
			if (gsonString.isEmpty())
				return null;
			parser.parse(gsonString);
			return gson.fromJson(gsonString, Container.class);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.err.println(Thread.currentThread().getStackTrace()[2].getMethodName() + " : " + e.toString());
			return new Container();
		}
	}

	public void Save(Container Obj)
	{
		String gsonString = gson.toJson(Obj);
		Editor.putString(Key, gsonString);
		Editor.commit();
	}
}
