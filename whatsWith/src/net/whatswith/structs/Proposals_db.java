package net.whatswith.structs;

import org.json.JSONObject;

public class Proposals_db
{
	public int proposal_id;
	public int question_id;
	public String user_id;
	public String proposal;
	
	public Proposals_db()
	{		
	}
	
	public Proposals_db(Proposals_db p)
	{		
		this.proposal_id = p.proposal_id;
		this.question_id = p.question_id;
		this.user_id = p.user_id;
		this.proposal = p.proposal;
	}
	
	public Proposals_db(int proposal_id, int question_id, String user_id, String proposal)
	{		
		this.proposal_id = proposal_id;
		this.question_id = question_id;
		this.user_id = user_id;
		this.proposal = proposal;
	}
	
	
	public Proposals_db(JSONObject ojb)
	{
		try
		{
			proposal_id = ojb.getInt("PROPOSAL_ID");
			question_id = ojb.getInt("QUESTION_ID");
			user_id = ojb.getString("USER_ID");
			proposal = ojb.getString("PROPOSAL");
		} catch (Exception e)
		{ 
			e.printStackTrace();
		}
	}
	
	
}
