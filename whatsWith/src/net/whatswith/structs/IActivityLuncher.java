package net.whatswith.structs;

public interface IActivityLuncher
{
	void Launch();
	void LaunchWithKey();
}
