/*
* This include header must be a top of include header caller
*/

#ifndef _SSL_CONN_HPP_
#define _SSL_CONN_HPP_

#ifdef _MSC_VER
#include <winsock2.h>
#include <ws2tcpip.h>
//#include <strsafe.h>
#pragma comment(lib, "Ws2_32.lib")
#pragma warning(disable : 4996) // hide sprintf warning
#else
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <resolv.h>
#endif

#include <cerrno>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sys/types.h>

extern "C"
{
#include "openssl/ssl.h"
#include "openssl/err.h"
}
extern "C"
{
#include "pthread.h"
}

#include "dbConn.hpp"

namespace dhe
{
	typedef struct
	{
        SSL_CTX* ctx;
        int client_sock_fd;	
		int max_password_fails;
	}thread_args_t;


	enum encrypt_method_t {TLSv1, TLSv1_1, TLSv1_2, SSLv2, SSLv3};
	extern const char* encryptTX[];

	void SSL_Constructor(void);
	void SSL_Destructor(void);

	SSL_CTX* InitServerCTX(encrypt_method_t encrypt_method);
	bool init_map_of_pointer_funcs(void);
	bool LoadCertificates(SSL_CTX* ctx, const char* CertFile, const char* KeyFile);
	bool SetSocketToListen(const int port, const int poolSize, int& sockFD);
	void ShowCerts(SSL* ssl);
	time_t ASN1_GetTimeT(ASN1_TIME* time);
	bool PrintlnSubjectInfo(const char* const pathToServCertFile);
	bool PrintlnIssuerInfo(const char* const pathToServCertFile);
	bool GetSubjectInfo(const char* const pathToServCertFile, char* buffer, int sizeofBuffer);
	bool GetIssuerInfo(const char* const pathToServCertFile, char* buffer, int sizeofBuffer);
	bool CloseSocket(int sockFD);
	bool CloseCoscketSession(int sockFD);
	bool InitSocketStream(int& sockFD);
	bool PrintlnFieldOfSubject(const char* const pathToServCertFile, const char* const fieldName);
	bool PrintlnFieldOfIssuer(const char* const pathToServCertFile, const char* const fieldName);
	bool GetFieldOfSubject(const char* const pathToServCertFile, const char* const fieldName, char* buffer, int sizeofBuffer);
	bool GetFieldOfIssuer(const char* const pathToServCertFile, const char* const fieldName, char* buffer, int sizeofBuffer);
	X509* GetCertFromFile(const char* const pathToServCertFile);
    void SetTimeOut(const int sockFD, const long TimeOutInSecs);
    
	void Servlet(thread_args_t& ThreadArgs); 
	void* ServetThread(void* thread_args); // threadable 
}
#endif // _SSL_CONN_HPP_



